#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) lxc-export-optimized-containers: export all LXC containers
#@(#)--------------------------------------------------------------
#@(#) Backup of all the containers, optimized way
#@(#) The containers all shutdowned one by one before backup und restart
#@(#) Version : 1.1
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#@(#) - 1.1 : print stats
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" | sed 's/\\n/$/g' | tr '$' '\n' >&2
}


is_integer () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
# true
echo 1
else
# false
echo 0
fi
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -d directory ] [ -n integer ] [ -c ] [ -o ] [ -r ] [ -h ]"
echo "-d (directory) : directory where the export must be stored (required)."
echo "-c (create) : create the backup directory if does not exist (optional)."
echo "-o (overwrite) : overwrite the backup if already exist (optional)."
echo "-n (number of backups) : keep the n last backups (optional)."
echo "-r (restart) : restart the container after export if running (optional). If this option is not used, the running container is stopped."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$( id -u )
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

CURRENT_DATE="$(date -u +'%Y%m%d')"
#------------------------------------------------------------------
# Parameters
#------------------------------------------------------------------
# Gestion des options
while getopts ":d:n:corh" option 
do 
case ${option} in 

d)
# Directory
HAS_DIRECTORY="true"
BACKUP_DIR="${OPTARG}"
;;

c)
# Create directory if does not exist
FORCE_DIR_CREATION="true"
;;

o)
# Overwrite export
IS_OVERWRITE="true"
;;

n)
# Keep n backups max
HAS_NB_BACKUP="true"
BACKUP_NB="${OPTARG}"
IS_INTEGER=$(is_integer "${BACKUP_NB}")
[ ${IS_INTEGER:-0} -ne 1 ] && print_error "${OPTARG} is not an integer" && usage && exit 1
;;

r)
# Restart after export
IS_RESTART="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
print_error "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Controls
[ "${HAS_DIRECTORY:-false}" = "false" ] && print_error "Missing directory.\nOption -d required." && usage && exit 1
[ "${HAS_DIRECTORY:-false}" = "true" -a ! -d "${BACKUP_DIR}" -a "${FORCE_DIR_CREATION:-false}" = "true" ] && mkdir -p "${BACKUP_DIR}"
[ "${HAS_DIRECTORY:-false}" = "true" -a ! -d "${BACKUP_DIR}" -a "${FORCE_DIR_CREATION:-false}" != "true" ] && print_error "Directory not found." && exit 1
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Checks running containers before export
CONTAINERS_RUNNING_BEFORE="$(lxc list -c n,s --format csv | awk -F',' '($2 == "RUNNING") {print $1}' | tr '\n' ',')"
#------------------------------------------------------------------
# Dumps LXD server config
print_error "Dumping the LXD configuration…"
lxd init --dump > "${BACKUP_DIR}/lxd.config.${CURRENT_DATE}"
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "An error occurred while dumping the config." && exit 1
print_error "LXD config dumped successfully."

# Dumps all instances list ##
lxc list > "${BACKUP_DIR}/lxd.instances.lst.${CURRENT_DATE}"
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "An error occurred while dumping the instances list." && exit 1
print_error "LXD instances list dumped successfully."
 
# Makes sure we know LXD version too ##
snap list lxd > "${BACKUP_DIR}/lxd-version.${CURRENT_DATE}"
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "An error occurred while dumping the LXD version." && exit 1
print_error "LXD version dumped successfully."
#------------------------------------------------------------------
# Backups the containers one by one

# List of containers with name (n) and status (s)
lxc list -c n,s --format csv | while read record
do

CONTAINER=$(echo ${record} | cut -d',' -f1)
STATUS=$(echo ${record} | cut -d',' -f2)

BACKUP_FILE="${BACKUP_DIR}/${CONTAINER}_bck_${CURRENT_DATE}.tar.xz"

# Checks if export already exists
[ -f "${BACKUP_FILE}" ] && print_error "Backup file already exists for container ${CONTAINER}."
[ -f "${BACKUP_FILE}" -a "${IS_OVERWRITE:-false}" = "false" ] && print_error "No overwrite option --> do nothing." && continue
[ -f "${BACKUP_FILE}" -a "${IS_OVERWRITE:-false}" = "true" ] && mv ${BACKUP_FILE} ${BACKUP_FILE}.old

# Stops container if running
[ "${STATUS:-none}" = "RUNNING" ] && {
print_error "\nStopping the container ${CONTAINER}…"
lxc stop ${CONTAINER}
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "An error occurred while stopping the container ${CONTAINER}." && exit 1
print_error "Container ${CONTAINER} successfully stopped."
}

# Exports the container
print_error "Exporting the container ${CONTAINER}…"
lxc export ${CONTAINER} ${BACKUP_FILE}
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "Export failed." && exit 1

# Print the stats for this backup
BACKUP_SIZE="$(ls -lhA ${BACKUP_FILE} | awk '{print $5}')"
print_error "Container ${CONTAINER} successfully exported. Size: ${BACKUP_SIZE}"

# Starts container if reboot activated
[ "${STATUS:-none}" = "RUNNING" -a "${IS_RESTART:-false}" = "true" ] && {
print_error "Restarting the container ${CONTAINER}…"
lxc start ${CONTAINER}
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "Restart failed." && exit 1
print_error "Container ${CONTAINER} successfully restarted."
}

# Clean
rm -f ${BACKUP_DIR}/${CONTAINER}_bck_*.old

done

#------------------------------------------------------------------
# Removes old backups
[ "${HAS_NB_BACKUP:-false}" = "true" ] && {
print_error "Removing old backups."

NB_HISTO=$(ls -1 ${BACKUP_DIR}/lxd.config.* | wc -l)
[ ${NB_HISTO:-0} -gt ${BACKUP_NB:-0} ] && {
NB_DELETE=$(expr ${NB_HISTO} - ${BACKUP_NB:-0})
ls -1A ${BACKUP_DIR}/lxd.config.* | head -${NB_DELETE} | xargs -n 1 basename | cut -d'.' -f3 | while read BACKUP_DATE
do
print_error "Removing backups of ${BACKUP_DATE}."
rm -f ${BACKUP_DIR}/*_bck_${BACKUP_DATE}.tar.xz
rm -f ${BACKUP_DIR}/*.${BACKUP_DATE}
print_error "Done!"
done
}

}
#------------------------------------------------------------------
# Print the stats
NB_BACKUPS=$(ls -1A ${BACKUP_DIR}/lxd-version.* | wc -l)
SIZE_BACKUPS="$(du -sh ${BACKUP_DIR} | awk '{print $1}')"
print_error "\n${NB_BACKUPS} backups, for a total of ${SIZE_BACKUPS}.\n"
#------------------------------------------------------------------
# Checks the statuses
CONTAINERS_RUNNING_AFTER="$(lxc list -c n,s --format csv | awk -F',' '($2 == "RUNNING") {print $1}' | tr '\n' ',')"
[ "${IS_RESTART:-false}" = "true" -a "${CONTAINERS_RUNNING_AFTER:-none}" != "${CONTAINERS_RUNNING_BEFORE:-enon}" ] && print_error "Statuses are not correct.\nCheck if the your containers run as expected." && exit 1 
#------------------------------------------------------------------
exit 0