#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) lxc-export-all-containers: export all LXC containers
#@(#)--------------------------------------------------------------
#@(#) Backup of all the containers
#@(#) Version : 1.0
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" | sed 's/\\n/$/g' | tr '$' '\n' >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -d directory ] [ -h ]"
echo "-d (directory) : directory where the export must be stored (required)."
echo "-s (strict mode) : do not perform any export if some containers are still running (optional)."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$( id -u )
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

#------------------------------------------------------------------
# Parameters
#------------------------------------------------------------------
# Gestion des options
while getopts ":d:sh" option 
do 
case ${option} in 

d)
# Directory
HAS_DIRECTORY="true"
BACKUP_DIR="${OPTARG}"
;;

s)
# Strict mode
IS_STRICT="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Controls
[ "${HAS_DIRECTORY:-false}" = "false" ] && print_error "Missing directory.\nOption -d required." && usage && exit 1
[ "${HAS_DIRECTORY:-false}" = "true" -a ! -d "${BACKUP_DIR}" ] && print_error "Directory not found." && exit 1
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Checks if containers are running
IS_RUNNING=$(lxc list -c n,s --format csv | awk -F',' '($2 == "RUNNING") {print $1}' | wc -l)
[ ${IS_RUNNING:-0} -gt 0 -a "${IS_STRICT:-false}" = "true" ] && print_error "Some containers are still running. Stops the export." && exit 1
[ ${IS_RUNNING:-0} -gt 0 -a "${IS_STRICT:-false}" = "false" ] && print_error "Some containers are still running. These will not be exported."

# List of containers with name (n) and status (s)
lxc list -c n,s --format csv | awk -F',' '($2 == "STOPPED") {print $1}' | while read CONTAINER
do

BACKUP_FILE="${BACKUP_DIR}/${CONTAINER}_bck_$(date +'%Y%m%d').tar.xz"

# Checks if export already exists
[ -f "${BACKUP_FILE}" ] && print_error "Backup file already exists for container ${CONTAINER}." && exit 1

# Exports the container
print_error "Exporting the container ${CONTAINER}…"
lxc export ${CONTAINER} ${BACKUP_FILE}--optimized-storage
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "Export failed." && exit 1
[ ${RETURN:-1} -eq 0 ] && print_error "Container ${CONTAINER} successfully exported."
done
#------------------------------------------------------------------
exit 0
