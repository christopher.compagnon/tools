#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) lxc-stop-all-containers: stop all LXC containers
#@(#)--------------------------------------------------------------
#@(#) Stops all the containers
#@(#) Version : 1.0
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" | sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$( id -u )
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# List of containers with name (n) and status (s)
lxc list -c n,s --format csv | awk -F',' '($2 == "RUNNING") {print $1}' | while read CONTAINER
do

# Stops the container
print_error "Stopping the container ${CONTAINER}…"
lxc stop ${CONTAINER}
RETURN=${?}
[ ${RETURN:-1} -ne 0 ] && print_error "An error occurred while stopping the container ${CONTAINER}." && exit 1

# Rechecks the status
STATUS=$(lxc info ${CONTAINER} | grep "Status:" | cut -d':' -f2- | xargs)
[ "${STATUS:-none}" = "RUNNING" ] && print_error "Container ${CONTAINER} cannot be stopped." && exit 1

print_error "Container ${CONTAINER} successfully stopped."

done
#------------------------------------------------------------------
exit 0
