#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) clear swap : clear the swap on Linux systems
#@(#)--------------------------------------------------------------
#@(#) Clear the swap on Linux systems
#@(#) Dependencies : none
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.2
#------------------------------------------------------------------
#@(#) Version : 1.0 - first version
#@(#) Version : 1.1 - check is more free RAM than Swap to perform the operation
#@(#) Version : 1.2 - add option for silent execution, usefull for automatic processing
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

usage () {
echo "Usage: ${SCRIPT_NAME} [ -s ] [ -h ]"
echo "-s (silent) : exit with no issue if something goes wrong. This option is usefull for automatic cleansing."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

EXIT_CODE=1
#------------------------------------------------------------------
# Parameters
#------------------------------------------------------------------
# Gestion des options
while getopts ":sh" option 
do 
case ${option} in 

s)
# Silent
IS_SILENT="true"
;; 

h)
# usage
usage
exit 0
;;

\?) 
print_error "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Controls
[ "${IS_SILENT:-false}" = "true" ] && EXIT_CODE=0

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Before clearing the swap, check that you have enough free RAM available to take all the pages from swap. 
# In other words, you should have more free RAM available than the current swap in use.
SWAP_FREE=$(free -h | awk '(NR == 3) {print $4}')
SWAP_FREE_UNIT="$(echo ${SWAP_FREE} | sed 's/[[:punct:]]//g' | sed 's/[0-9]//g')"
SWAP_FREE_VALUE=$(echo ${SWAP_FREE} | sed 's/[[:alpha:]]//g')
# Unit can be Mi/Gi

# If no Swap, stop here
[ ${SWAP_FREE_VALUE:-0} -eq 0 ] && print_error "No Swap to clear." && exit 0

RAM_FREE=$(free -h | awk '(NR == 2) {print $7}')
RAM_FREE_UNIT="$(echo ${RAM_FREE} | sed 's/[[:punct:]]//g' | sed 's/[0-9]//g')"
RAM_FREE_VALUE=$(echo ${RAM_FREE} | sed 's/[[:alpha:]]//g')
# Unit can be Mi/Gi

# Conversion in Mi for all values
[ "${RAM_FREE_UNIT:-Mi}" = "Gi" ] && RAM_FREE_VALUE=$( expr ${RAM_FREE_VALUE:-0} \* 1000 )
[ "${SWAP_FREE_UNIT:-Mi}" = "Gi" ] && SWAP_FREE_VALUE=$( expr ${SWAP_FREE_VALUE:-0} \* 1000 )

# Comparison
[ ${RAM_FREE_VALUE:-0} -le ${SWAP_FREE_VALUE} ] && print_error "You do not have enough RAM to clear the Swap safely." && exit ${EXIT_CODE}

# Store the number of swap zones
SWAP_NB_BEFORE=$(swapon --show | wc -l)

# Deactivate all the Swap on the system
swapoff -a

# Activate all the Swap on the system
swapon -a

# Store the number of swap zones
SWAP_NB_AFTER=$(swapon --show | wc -l)
#------------------------------------------------------------------
# Check if all is OK
[ ${SWAP_NB_BEFORE:-0} -gt ${SWAP_NB_AFTER:-0} ] && print_error "Something went wrong during the process.\nPlease check the swap manually." && exit 1
#------------------------------------------------------------------
exit 0