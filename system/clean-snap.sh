#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) clean-snap: Removes old revisions of snaps
#@(#)--------------------------------------------------------------
#@(#) Removes old revisions of snaps
#@(#) Version : 1.0
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -c ] [ -s ] [ -h ]"
echo "-c (clean) : perform the cleansing of the old snaps."
echo "-y (yes) : apply the [Y]es answer to all questions."
echo "-s (size) : display the size of the snap directory."
echo "-h (help) : display help."
echo
echo "Note: if no options, then perform cleansing by default."
}

snap_size () {
SNAPS_SIZE="$( du -sh /var/lib/snapd | awk '{ print $1 }' | xargs )"
print_error "${SNAPS_SIZE} of snaps."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$( id -u )
[ ${UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

HAS_OLD_SNAP=$( snap list --all | awk '/disabled/{print $1, $3}' | wc -l )
#------------------------------------------------------------------
# Parameters
#------------------------------------------------------------------
# Gestion des options
while getopts ":csyh" option 
do 
case ${option} in 

c)
# Clean
HAS_CLEAN="true"
;;

s)
# Size
HAS_SIZE="true"
;;

y)
# Yes
HAS_YES="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Controls
[ "${HAS_YES:-false}" = "true" ] && RESQUEST_ANSWER="Y"
[ "${HAS_SIZE:-none}" = "none" -a "${HAS_CLEAN:-none}" = "none" ] && HAS_CLEAN="true"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Display size if asked
[ "${HAS_SIZE:-false}" = "true" ] && snap_size

#------------------------------------------------------------------
# Clean the old snaps
[ "${HAS_CLEAN:-false}" = "true" -a ${HAS_OLD_SNAP:-0} -eq 0 ] && print_error "No old snap to remove."
[ "${HAS_CLEAN:-false}" = "true" -a ${HAS_OLD_SNAP:-0} -gt 0 ] && {

snap_size

print_error "Removes old revisions of snaps"
print_error "CLOSE ALL SNAPS BEFORE RUNNING THIS!!!"
echo "Confirm action ? [Y]es/[N]o"
[ "${HAS_YES:-false}" = "true" ] && print_error "--> Answer is [Y]es." 
[ "${HAS_YES:-false}" = "false" ] && read RESQUEST_ANSWER

[ "${RESQUEST_ANSWER:-N}" != "Y" ] && print_error "Exiting the process."  && exit 0

print_error "Removing old snaps…"
# -e : Exit immediately if a pipeline, which may consist of a single simple command, a list, or a compound command returns a non-zero status.
# -u : reat unset variables and parameters other than the special parameters ‘@’ or ‘*’ as an error when performing parameter expansion. An error message will be written to the standard error, and a non-interactive shell will exit. 
set -eu
snap list --all | awk '/disabled/{print $1, $3}' | while read snapname revision
do
print_error "Remove snap ${snapname} ( revision:${revision} )"
snap remove "${snapname}" --revision="${revision}"
done
print_error "Done."
snap_size

}

#------------------------------------------------------------------
exit 0
