# System tools

## mount file (mount-file.sh)

Script for mounting a file into the filesystem.
Useful to mount some types of file such as *.iso* which is usually mounted as read-only by default.
You can use it to mount a filesystem stored in a file and access it with read + write rights.

Note : You must be root to execute this script.

You have to create first a file + filesystem with r+w for the user. You can use *mount-file.sh* to create the file. For example :

    sudo ./mount-file.sh -c 500 -f "path/to/my-file" -l "myPersonalFS"

creates a file of 500MB named *my-file* and located at *path/to/*.

Without any further option of mounting, the script automatically mounts the file on a default directory in */media*.

The example above is the same as :

    sudo ./mount-file.sh -c 500 -f "path/to/my-file" -l "myPersonalFS" -M

If you want to mount the file on a specific directory, you have to use option **-m** as :

    sudo ./mount-file.sh -c 500 -f "path/to/my-file" -m "path/of/mounting"

or, if the file already exists :

    sudo ./mount-file.sh -f "path/to/my-file" -m "path/of/mounting"



## Ramdisk

Script to manage ramdisks.

Note : You must be root to execute this script.

For creating a new ramdisk of 100 Mo with a label as *MyRamDisk*:

    sudo ./ramdisk.sh -c 100 -l "MyRamDisk"

For creating a new ramdisk of 100 Mo with a label as *MyRamDisk*: on a specific mount point (if no mount point then */media/ramdisks/*):

    sudo ./ramdisk.sh -c 100 -l "MyRamDisk" -m "path/to/mount/point"

You can also create a ramdisk into /media for accessing it through the desktop environment as a mounted disk:

    sudo ./ramdisk.sh -c 100 -l "MyRamDisk" -M


## Swap alert (uses Zenity)

Script to alert when RAM or Swap reached a threshold.

For checking the RAM at 80%:

    /path/to/swap-alert.sh -r 80

if the RAM has reached the 80%, then gets a notification.

For checking the RAM at 10%:

    /path/to/swap-alert.sh -s 10

if the Swap has reached the 10% (meaning the RAM is probably full), then gets a notification.

The script should be launched regularly through Crontab.

The script can also launche an extra command with -c option:

    /path/to/swap-alert.sh -s 10 -c "my extra command"

executes "my extra command" once Swap has reached 10%.

## Firefox session backup

Script to save the current session of Firefox.

Launch the backup at start with:

    crontab -e

for editing the crontab and add:

    @reboot /path/to/ffx-session-backup.sh &


## Normalize rename

Rename a batch of file in a directory with normalized names as %0xd, with x as parameter.

    normalize-rename.sh -f path/to/my_file.ext -s 4

renames all the files of the same directory to a normalized name 0001.ext, 0002.ext, …

    normalize-rename.sh -d path/to/directory -s 4

takes the first file in the directory and renames all the files with the same extension in that directory to a normalized name 0001.ext, 0002.ext, …


## Get my public IP

Get and display the public IP(v4) of the machine where the script is executed.

    get-my-public-ip.sh

This script uses *ipecho.net*.

## Maya

    ./maya.sh

gets the Maya date of the current day. Requires *maya.bc*.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
