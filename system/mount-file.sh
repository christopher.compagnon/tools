#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Mount File : Mount a file in a filesystem
#@(#)--------------------------------------------------------------
#@(#) Mount a file in a filesystem
#@(#) Version : 1.2
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#@(#) - 1.1 : add creation of file + better management of mounting
#@(#) - 1.2 : improvements of the mounting + unmounting
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_integer () {
# teste si le paramètre est un entier
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -f file ] [ -c integer ] [ -l string ] [ -m directory ] [ -M ] [ -u ] [ -h ]"
echo "-f (file) : filename to mount. Required."
echo "-c (create) : create a data file of n MB. Optional."
echo "-l (label) : label of the disk (if option -c)."
echo "-m (mount point) : mount the file on a specific directory. Optional."
echo "-M (mount) : mount the file on a default directory. Optional. By default if no action defined."
echo "-u (unmount) : unmount the mounted file."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
[ ${UID:--1} -ne 0 ] && echo "You must be root to execute this script." && exit 1

#USER_NAME=$(id -nu)
USER_NAME="${SUDO_USER}"
# The user behind the SUDO can be found also with logname
#USER_NAME=$(logname)
DEFAULT_MOUNT_POINT="/media/${USER_NAME}"
# Note : losetup -j "${DATA_FILE}" gives the status attached to this file
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":c:l:f:m:uMdh" option 
do 
case ${option} in 

c)
# Create file of n MB
NB_BLOCK=${OPTARG}
IS_INTEGER=$(is_integer ${NB_BLOCK})
[ ${IS_INTEGER:-1} -ne 0 ] && print_error "Sorry, ${NB_BLOCK} is not an integer." && exit 1
[ ${NB_BLOCK:-0} -eq 0 ] && print_error "You need to define a size." && usage && exit 1
HAS_SIZE="true"
ACTION="create"
;;


l)
# label of RAMDISK
P_DISK_LABEL="${OPTARG}"
DISK_LABEL="$( echo "${P_DISK_LABEL}" | xargs | sed 's/ /_/g')"
HAS_LABEL="true"
;;


f)
# File to mount
DATA_FILE="${OPTARG}"
#[ ! -f "${DATA_FILE}" ] && echo "File ${DATA_FILE} does not exist." && exit 1 
DIRECTORY="$(dirname ${DATA_FILE})"
[ "x${DIRECTORY}x" = "xx" ] && DIRECTORY="/home/${USER_NAME}"
FILE_NAME="$(basename ${DATA_FILE})"
[ "x${FILE_NAME}x" = "xx" ] && echo "File must be defined." && usage && exit 1
DATA_FILE="${DIRECTORY}/${FILE_NAME}"
# Mount point --> create an UUID from the file to be mounted
MOUNT_UUID="$(echo "${DATA_FILE}" | openssl dgst -sha256 | cut -d "=" -f2- | xargs)"
HAS_FILE="true"
;;


m)
# Mount point of file
MOUNT_POINT="${OPTARG}"
HAS_MOUNT_POINT="true"
ACTION="mount"
;;


M)
# Auto mount of file
HAS_MEDIA_MOUNT="true"
ACTION="mount"
;;


u)
# Unmount file
ACTION="unmount"
;;


h)
# Help
usage
exit 0
;;


\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;


esac
done
#------------------------------------------------------------------
# Contrôles

[ "${HAS_FILE:-false}" = "false" ] && print_error "You have to define a file to mount with option -f…" && usage && exit 1

# If no actions defined, then auto mount
[ "${ACTION:-none}" = "none" ] && ACTION="mount"
[ "${HAS_MEDIA_MOUNT:-false}" = "true" ] && MOUNT_POINT="${DEFAULT_MOUNT_POINT}/${MOUNT_UUID}"
[ "${HAS_MEDIA_MOUNT:-false}" = "false" ] && HAS_MOUNT_POINT="true"

[ "${ACTION:-none}" = "mount" -a "${HAS_SIZE:-false}" = "true" ] && ACTION="create"

[ "${ACTION:-none}" = "create" -a  "${HAS_FILE:-false}" = "false" ] && print_error "You have to define a file with option -f to use option -c…" && usage && exit 1


# If the file to be created already exists
[ "${ACTION:-none}" = "create" -a  "${HAS_FILE:-false}" = "true" -a -f "${DATA_FILE}" ] && print_error "The file ${DATA_FILE} already exists." && exit 1
#------------------------------------------------------------------
# Create file
[ "${ACTION:-none}" = "create" ] && {

echo "Creating ${DATA_FILE}…"

LO_DEVICE="$(losetup -f)"
DEVICE_NAME="$(basename ${LO_DEVICE})"

[ "${HAS_LABEL:-false}" = "false" ] && DISK_LABEL="${DEVICE_NAME}"

fallocate -l ${NB_BLOCK}MB "${DATA_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to create file ${DATA_FILE}." && exit 1

chown ${USER_NAME:-root} "${DATA_FILE}"
chmod 700 "${DATA_FILE}"

losetup "${LO_DEVICE}" "${DATA_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to mount ${DATA_FILE} on ${LO_DEVICE}." && exit 1

mkfs.ext4 "${LO_DEVICE}" -L "${DISK_LABEL}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to format ${DATA_FILE} on ${LO_DEVICE}." && exit 1

losetup -d "${LO_DEVICE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to unmount ${LO_DEVICE}." && exit 1

echo "${DATA_FILE} successfully created and ready to be used."
}

[ "${ACTION:-none}" = "create" -a "${HAS_MOUNT_POINT:-false}" = "true" ] && ACTION="mount"
#------------------------------------------------------------------
# Mount file
[ "${ACTION:-none}" = "mount" ] && {

# Check if file is not mounted already
IS_MOUNTED=$(mount -l | grep "${DATA_FILE}" | wc -l)
[ ${IS_MOUNTED:-0} -ne 0 ] && print_error "${DATA_FILE} already mounted." && exit 0
	
echo "Mounting ${DATA_FILE}…"

mkdir -p "${MOUNT_POINT}"
[ "${HAS_MEDIA_MOUNT:-false}" = "true" ] && chown root "${MOUNT_POINT}"

mount "${DATA_FILE}" "${MOUNT_POINT}" -o loop
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to mount ${DATA_FILE}." && exit 1

IS_MOUNTED=$(mount -l | grep "${DATA_FILE}" | wc -l)
[ ${IS_MOUNTED:-0} -ne 1 ] && print_error "Unable to mount ${DATA_FILE} on ${MOUNT_POINT}." && exit 1

# Change the rights to access R + W
chown -R ${USER_NAME:-root} "${MOUNT_POINT}"
chmod 700 "${MOUNT_POINT}"

echo "${DATA_FILE} successfully mounted on ${MOUNT_POINT}."

}
#------------------------------------------------------------------
# Unmount file

[ "${ACTION:-none}" = "unmount" ] && {

echo "Unmounting ${DATA_FILE}…"

IS_MOUNTED=$(mount -l | grep "${DATA_FILE}" | wc -l)
[ ${IS_MOUNTED:-0} -eq 0 ] && print_error "${DATA_FILE} not mounted." && exit 1
[ ${IS_MOUNTED:-0} -eq 1 ] && {
echo "unmount ${DATA_FILE}…"
umount "${DATA_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to unmount ${DATA_FILE}." && exit 1
}

IS_MOUNTED=$(mount -l | grep "${DATA_FILE}" | wc -l)
[ ${IS_MOUNTED:-0} -eq 0 ] && echo "${DATA_FILE} successfully unmounted."
[ ${IS_MOUNTED:-0} -ne 0 ] && print_error "Unable to unmount ${DATA_FILE}." && exit 1

# Supprimer le répertoire de montage si dans /media
[ "${HAS_MEDIA_MOUNT:-false}" = "true" -a -d "${MOUNT_POINT}" ] && rm -R "${MOUNT_POINT}"

}
#------------------------------------------------------------------
# Nettoyage des répertoires de montage

#------------------------------------------------------------------
exit 0
