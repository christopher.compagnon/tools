#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) swap-alert : alerts when RAM/Swap has reached a threshold
#@(#)--------------------------------------------------------------
#@(#) Notifies when RAM/Swap has reached a threshold and executes an extra command.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_number () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -r numeric ] [ -s numeric ] [ -c command ] [ -q ] [ -h ]"
echo "-r (RAM) : tests if the usage of RAM has reached the parameter's value (between 0 and 100) and triggers a notification if so."
echo "-s (SWAP) : tests if the usage of Swap has reached the parameter's value (between 0 and 100) and triggers a notification if so."
echo "-c (Command) : executes the command if the treshold has been reached."
echo "-q (Quiet) : no notifications. Only extra command."
echo "-h (Help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Récupération des options
while getopts ":c:r:s:qh" OPTION
do

case "${OPTION}" in

"r")
# RAM
IS_RAM="true"
RAM_THRESHOLD=${OPTARG}
IS_NUMBER=$(is_number ${RAM_THRESHOLD})
[ ${IS_NUMBER:-1} -ne 0 ] && print_error "Sorry, ${RAM_THRESHOLD} is not an integer." && exit 1
[ ${RAM_THRESHOLD:-1} -gt 100 -o ${RAM_THRESHOLD:-1} -lt 0 ] && print_error "Wrong value of threshold for -${OPTION}, must be set between 0 and 100." && exit 1 
RAM_TOTAL=$(vmstat -s | head -1 | tail -1 | awk '{print $1}')
RAM_USED=$(vmstat -s | head -2 | tail -1 | awk '{print $1}')
RAM_USAGE=$( expr ${RAM_USED} \* 100 \/ ${RAM_TOTAL} )
;;

"s")
# SWAP
IS_SWAP="true"
SWAP_THRESHOLD=${OPTARG}
IS_NUMBER=$(is_number ${SWAP_THRESHOLD})
[ ${IS_NUMBER:-1} -ne 0 ] && print_error "Sorry, ${SWAP_THRESHOLD} is not an integer." && exit 1
[ ${SWAP_THRESHOLD:-1} -gt 100 -o ${SWAP_THRESHOLD:-1} -lt 0 ] && print_error "Wrong value of threshold for -${OPTION}, must be set between 0 and 100." && exit 1 
SWAP_TOTAL=$(vmstat -s | head -8 | tail -1 | awk '{print $1}')
SWAP_USED=$(vmstat -s | head -9 | tail -1 | awk '{print $1}')
SWAP_USAGE=$( expr ${SWAP_USED} \* 100 \/ ${SWAP_TOTAL} )
;;

"c")
# Command
HAS_COMMAND="true"
CMD_EXEC="${OPTARG}"
;;

"q")
# Quiet
IS_QUIET="true"
;;

"h")
# Help
usage
exit 0
;;

:)
print_error "Missing argument for -${OPTARG}."
echo
usage
exit 1
;;

?)
print_error "Bad option -${OPTARG}."
echo
usage
exit 2
;;

esac

done
#------------------------------------------------------------------
# Contrôles
[ "${IS_RAM:-false}" = "false" -a "${IS_SWAP:-false}" = "false" ] && print_error "No type defined" && usage && exit 1

#------------------------------------------------------------------
# Lance une alerte (notification)
[ "${IS_SWAP:-false}" = "true" -a ${SWAP_USAGE:-0} -gt ${SWAP_THRESHOLD:-0} ] && {
[ "${IS_QUIET:-false}" = "false" ] && zenity --notification --window-icon="info" --text="Swap is used at ${SWAP_USAGE:-0}%.\nPlease make space in RAM!"

[ "${HAS_COMMAND:-false}" = "true" -a "${IS_QUIET:-false}" = "false" ] && zenity --notification --window-icon="info" --text="Threshold has been reached: launches an extra command"
[ "${HAS_COMMAND:-false}" = "true" ] && ${CMD_EXEC}

}

[ "${IS_RAM:-false}" = "true" -a ${RAM_USAGE:-0} -gt ${RAM_THRESHOLD:-0} ] && {
[ "${IS_QUIET:-false}" = "false" ] && zenity --notification --window-icon="info" --text="RAM is used at ${RAM_USAGE:-0}%.\nPlease make space in RAM!"

[ "${HAS_COMMAND:-false}" = "true" -a "${IS_QUIET:-false}" = "false" ] && zenity --notification --window-icon="info" --text="Threshold has been reached: launches an extra command"
[ "${HAS_COMMAND:-false}" = "true" ] && ${CMD_EXEC}

}

#------------------------------------------------------------------
exit 0