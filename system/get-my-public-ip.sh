#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) get-my-publec-ip : Get the public IP of the machine.
#@(#)--------------------------------------------------------------
#@(#) Get and display the public IP of the machine where the script is executed
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# ipecho.net offers an unformatted page displaying the IP address :
wget -q -O - http://ipecho.net/plain && echo

#------------------------------------------------------------------
exit 0