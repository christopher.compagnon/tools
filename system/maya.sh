#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) maya : Maya date of the day
#@(#)--------------------------------------------------------------
#@(#) Maya date of the day
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Date of the day
annee=$(date +'%Y')
mois=$(date +'%m')
jour=$(date +'%d')

# Maya calendar
baktum=$( echo "baktum(${annee},${mois},${jour})" | bc ${SCRIPT_DIR}/maya.bc)
katum=$( echo "katum(${annee},${mois},${jour})" | bc ${SCRIPT_DIR}/maya.bc)
tun=$( echo "tun(${annee},${mois},${jour})" | bc ${SCRIPT_DIR}/maya.bc)
uinal=$( echo "uinal(${annee},${mois},${jour})" | bc ${SCRIPT_DIR}/maya.bc)
kin=$( echo "kin(${annee},${mois},${jour})" | bc ${SCRIPT_DIR}/maya.bc)

# Print result
echo "${baktum}.${katum}.${tun}.${uinal}.${kin}"
#------------------------------------------------------------------
exit 0