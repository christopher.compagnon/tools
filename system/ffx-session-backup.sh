#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) ffx-session-backup : Save Firefox session of current user
#@(#)--------------------------------------------------------------
#@(#) Save Firefox session of current user
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
BCK_DIR="${HOME}/.ffx-session-backup"
HORODATE="$(date +'%Y%m%d%H%M%S')"
FFX_PROFILES_DIR="${HOME}/.mozilla/firefox"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
mkdir -p "${BCK_DIR}"
cd "${BCK_DIR}"

# Récupération des options
while getopts ":br:n:" OPTION
do

case "${OPTION}" in

"b")
# Backup
ACTION="BACKUP"
;;

"r")
# Restore
ACTION="RESTORE"
ARCHIVE="${OPTARG}"
;;

"n")
# Number of backups 
NB_BCK=${OPTARG}
;;

?)
;;

esac

done

#------------------------------------------------------------------
# BACKUP

[ "${ACTION:-BACKUP}" = "BACKUP" ] && {
# List of all profiles
find ${FFX_PROFILES_DIR} -type d -name "sessionstore-backups" | while read FFX_DIR
do

# retrieve the name of profile
PROFILE_DIR=$(echo "${FFX_DIR}" | sed 's/.*\/firefox\/\(.*\)\/sessionstore-backups/\1/g')
mkdir -p ${PROFILE_DIR}
cd ${PROFILE_DIR}

echo "Backing ${PROFILE_DIR} up…"
pwd
# Store the backups of the session
tar -cf sessionstore-backups.${HORODATE}.tar ${FFX_DIR}
# Store the current session
tar -uf sessionstore-backups.${HORODATE}.tar ${FFX_PROFILES_DIR}/${PROFILE_DIR}/sessionstore.js
echo "Backup ${PROFILE_DIR} done"

# Remove old backups
ls -1t ./sessionstore-backups.*.tar | awk -v NB_BCK=${NB_BCK:-60} 'NR>NB_BCK {print $0}' | xargs rm -f

cd "${BCK_DIR}"
done
}

#------------------------------------------------------------------
# [TODO] : RESTORE
[ "${ACTION:-BACKUP}" = "RESTORE" ] && {
echo "Restoring ${ARCHIVE}…"
# Restaurer le fichiers sessionstore.js
# Supprimer le contenu sessionstore-backups et restaurer la sauvegarde
}

#------------------------------------------------------------------
exit 0