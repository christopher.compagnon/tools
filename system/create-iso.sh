#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) create-iso : Create a filesystem in a file
#@(#)--------------------------------------------------------------
#@(#) Create a filesystem in a file
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -d filename ] [ -f format ] [ -s size ] [ -h ]"
echo "-d (disk file) : file name of disk. Required."
echo "-f (filesystem) : filesystem. ext2, ext3 or ext4."
echo "-s (size) : size of the file in Mo (512M, 1024M, etc.). Required. "
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":d:s:f:h" option 
do 
case ${option} in 

d)
# disk file
FILE="${OPTARG}"
HAS_FILE="true"
DIRECTORY="$(dirname ${FILE})"
FILE_NAME="$(basename ${FILE})"
;;

s)
# Size
SIZE="${OPTARG}"
HAS_SIZE="true"
;;

f)
# format
FORMAT="${OPTARG}"
HAS_FORMAT="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Contrôles
[ "${HAS_FILE:-false}" != "true" ] && print_error "Option -f is missing !" && exit 1
[ "${HAS_SIZE:-false}" != "true" ] && print_error "Option -s is missing !" && exit 1
[ -f "${FILE}" ] && print_error "${FILE} already exists !" && exit 1

#------------------------------------------------------------------
#As a first step, simply create a file that acts as a plain, un-encrypted container of information.

#First of all, create an empty file:
#512M
fallocate -l ${SIZE} "${FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Something went wrong during the creation of file." && exit 1

#The next step, is to turn this file into a "virtual disk" by adding a file system to it; doing so is really simple:

[ "${HAS_FORMAT:-false}" = "true" ] && {
mke2fs -F -t ${FORMAT} -j "${FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Something went wrong during the creation of filesystem." && exit 1
}

#------------------------------------------------------------------
exit 0
