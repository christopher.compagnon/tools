#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) rambdisk : Create a ramdisk
#@(#)--------------------------------------------------------------
#@(#) Create a ramdisk
#@(#) Version : 1.1
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#@(#) - 1.1 : option -M added
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_integer () {
# teste si le paramètre est un entier
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -c integer ]  [ -m directory ] [ -l string ] [ -h ]"
echo "-c (create) : create a ramdisk of n Mo."
echo "-m (mount point) : mount the ramdisk on a specific directory. If the directory does not exist, create it. If no option -M or -m, ramdisk automatically mounted in this directory /media by default."
echo '-M (Media) : mount the ramdisk in the "/media" directory to be accessible from GUI as disk. You can also use the option -m "/media/MyRamDisk". If no option -M or -m, ramdisk automatically mounted in this directory /media by default.'
echo "-l (label) : label of the ramdisk."
#echo "-u (unmount) : unmount the ramdisk n."
#echo "-d (destroy) : destroy the ramdisk n."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
[ ${UID:--1} -ne 0 ] && echo "You must be root to execute this script." && exit 1

BLOCK_SIZE=1048576     # Mo
USER_NAME=$(id -nu)

DEFAULT_MOUNT_DIR="/media/ramdisks"
NB_BLOCK=100
DEVICE_NUM=$(ls -1 /dev/ram* | wc -l)
DEVICE="/dev/ram${DEVICE_NUM}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":c:d:m:Mu:l:h" option 
do 
case ${option} in 

c)
# Create RAMDISK of n blocks
NB_BLOCK=${OPTARG}
IS_INTEGER=$(is_integer ${NB_BLOCK})
[ ${IS_INTEGER:-1} -ne 0 ] && print_error "Sorry, ${NB_BLOCK} is not an integer." && exit 1
[ ${NB_BLOCK:-0} -eq 0 ] && print_error "You need to define a size." && usage && exit 1
HAS_SIZE="true"
ACTION="create"
;;

m)
# Mount point of RAMDISK
MOUNT_POINT="${OPTARG}"
HAS_MOUNT_POINT="true"
;;

M)
# Mount point of RAMDISK on /media
MOUNT_POINT="/media/ramdisks/ramdisks${DEVICE_NUM}"
HAS_MEDIA_POINT="true"
;;


l)
# label of RAMDISK
RAMDISK_LABEL="${OPTARG}"
HAS_LABEL="true"
;;

d)
# Destroy RAMDISK --> not used yet
ACTION="delete"
;;

u)
# Unmount RAMDISK --> not used yet
ACTION="unmount"
;;

h)
# Help
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Contrôles

[ "${HAS_MEDIA_POINT}" = "true" -a ${HAS_MOUNT_POINT} = "true" ] && print_error "Options -m and -M cannot be used simultaneously." && usage && exit 1
# If mount point not defined, then creates one automatically
[ "${HAS_MEDIA_POINT}" = "false" -a "${HAS_MOUNT_POINT:-false}" = "false" ] && MOUNT_POINT="${DEFAULT_MOUNT_DIR}/ramdisk${DEVICE_NUM}"

mkdir -p "${MOUNT_POINT}"
[ ! -d "${MOUNT_POINT}" ] && print_error "Mount point not defined or unable to create at ${MOUNT_POINT}." && usage && exit 1

[ "${HAS_SIZE:-false}" = "true" -a ${NB_BLOCK:-0} -eq 0 ] && print_error "Size not defined." && usage && exit 1
[ "${DEVICE:-none}" = "none" ] && print_error "Device not defined." && usage && exit 1

[ "${RAMDISK_LABEL:-none}" = "none" ] && RAMDISK_LABEL="RamDisk_$(expr ${DEVICE_NUM:-0} + 1)"

#------------------------------------------------------------------
# Create/mount RAMDISK
dd if="/dev/zero" of="${DEVICE}" bs=${BLOCK_SIZE} count=${NB_BLOCK}
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to initialize the ramdisk." && exit 1

mke2fs -q -L "${RAMDISK_LABEL}" "${DEVICE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to format the ramdisk." && exit 1

mount "${DEVICE}" "${MOUNT_POINT}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Unable to mount the ramdisk on ${MOUNT_POINT}." && exit 1

chmod 777 "${MOUNT_POINT}"

echo "${RAMDISK_LABEL} is now available at ${MOUNT_POINT}."
#------------------------------------------------------------------
exit 0