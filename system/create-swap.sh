#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) create-swap : Create a swap file
#@(#)--------------------------------------------------------------
#@(#) Create a swap file
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_integer () {
# teste si le paramètre est un entier
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -f filename ] [ -s number ] [ -h ]"
echo "-f (file) : path + name of the swap file. Required."
echo "-s (size) : number of blocks (Mo). 40 by default."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
ROOT_UID=0                  # Root a l'$UID 0.
[ ${UID} -ne ${ROOT_UID} ] && echo "You must be root to execute this script." && exit 1

#BLOCK_SIZE=1024         # Ko
BLOCK_SIZE=1048576     # Mo
NB_BLOCK_MIN=40
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":f:s:h" option 
do 
case ${option} in 

f)
# Swap file
SWAP_FILE="${OPTARG}"
HAS_FILE="true"
DIRECTORY="$(dirname ${SWAP_FILE})"
FILE_NAME="$(basename ${SWAP_FILE})"
;;

s)
# Number of blocks
NB_BLOCK=${OPTARG}
IS_INTEGER=$(is_integer ${NB_BLOCK})
[ ${IS_INTEGER:-1} -ne 0 ] && print_error "Sorry, ${NB_BLOCK} is not an integer." && exit 1
HAS_SIZE="true"
;;

h)
# Help
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Contrôles

[ "${NB_BLOCK:-none}" = "none" ] && NB_BLOCK=${NB_BLOCK_MIN}
[ ${NB_BLOCK} -lt ${NB_BLOCK_MIN} ] && NB_BLOCK=${NB_BLOCK_MIN}
[ "${HAS_FILE:-false}" = "false" ] && print_error "You need to define a swap file." && usage && exit 1

#------------------------------------------------------------------
echo "Creating a swap file with a size of ${NB_BLOCK} blocks (Mo)…"

dd if="/dev/zero" of="${SWAP_FILE}" bs=${BLOCK_SIZE} count=${NB_BLOCK}  # Vide le fichier.
[ ! -f "${SWAP_FILE}" ] && print_error "Error occured when creating the file ${SWAP_FILE}." && exit 1

mkswap "${SWAP_FILE}" ${NB_BLOCK}             # Indique son type : swap.
HAS_ERROR=${?}
[ ${HAS_ERROR} -ne 0 ] && print_error "Error occured when creating the swap on file ${SWAP_FILE}.\nPlease check the system, otherwise it could be unstable…" && exit 1

swapon "${SWAP_FILE}"                    # Active le fichier swap.
HAS_ERROR=${?}
[ ${HAS_ERROR} -ne 0 ] && print_error "Error occured when activating the swap file ${SWAP_FILE}.\nPlease check the system. otherwise it could be unstable…" && exit 1

echo "Swap created and ready for usage on ${SWAP_FILE}."
#------------------------------------------------------------------
exit 0