#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) install: Install Gitea on the system
#@(#)--------------------------------------------------------------
#@(#) Install Gitea on the system
#@(#) Version : 1.0
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#)--------------------------------------------------------------
#@(#) - 1.0 : first version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$( id -u )
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

GITEAVERSION=1.15.7
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Add user
USER_EXISTS=$(cat /etc/passwd | grep "git:x" | wc -l)

[ ${USER_EXISTS:-0} -eq 0 ] && {

adduser \
--system \
--shell /bin/bash \
--gecos 'Git Version Control' \
--group \
--disabled-password \
--home /home/git \
git

print_error "User Git created."
}
#------------------------------------------------------------------
# Install binaries
BIN_EXISTS=$(whereis gitea | cut -d':' -f2- | xargs | wc -c)

[ ${BIN_EXISTS:-0} -lt 5 ] && {

cd /tmp
wget -O gitea https://dl.gitea.io/gitea/${GITEAVERSION}/gitea-${GITEAVERSION}-linux-amd64

mv /tmp/gitea /usr/local/bin
chmod +x /usr/local/bin/gitea
mkdir -p /etc/gitea
mkdir -p /var/lib/gitea/{custom,data,indexers,public,log}
chown -R git:git /var/lib/gitea/
chown root:git /etc/gitea
chmod -R 750 /var/lib/gitea/
chmod 770 /etc/gitea

print_error "Gitea installed."
}
#------------------------------------------------------------------
# Install service

SERVICE_FILE="/etc/systemd/system/gitea.service"

echo '[Unit]' > ${SERVICE_FILE}
echo 'Description=Gitea' >> ${SERVICE_FILE}
echo 'After=syslog.target' >> ${SERVICE_FILE}
echo 'After=network.target' >> ${SERVICE_FILE}
echo 'After=postgresql.service' >> ${SERVICE_FILE}
echo >> ${SERVICE_FILE}
echo '[Service]' >> ${SERVICE_FILE}
echo 'RestartSec=2s' >> ${SERVICE_FILE}
echo 'Type=simple' >> ${SERVICE_FILE}
echo 'User=git' >> ${SERVICE_FILE}
echo 'Group=git' >> ${SERVICE_FILE}
echo 'WorkingDirectory=/var/lib/gitea/' >> ${SERVICE_FILE}
echo 'ExecStart=/usr/local/bin/gitea web -c /etc/gitea/app.ini' >> ${SERVICE_FILE}
echo 'Restart=always' >> ${SERVICE_FILE}
echo 'Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea' >> ${SERVICE_FILE}
echo  >> ${SERVICE_FILE}
echo '[Install]' >> ${SERVICE_FILE}
echo 'WantedBy=multi-user.target' >> ${SERVICE_FILE}

systemctl enable gitea
systemctl daemon-reload
systemctl start gitea

#------------------------------------------------------------------
exit 0