# Install Gitea server on LXD/Ubuntu


## PostgreSGL

Install and configure PostgreSQL.

Create a new user :

    CREATE ROLE gitea WITH LOGIN PASSWORD 'secure@123';

Create a database :

    CREATE DATABASE giteadb;
    GRANT ALL PRIVILEGES ON DATABASE giteadb TO gitea;


## Configure Git

Git is already installed on Ubuntu.

    git config --global user.name "Your Name"
    git config --global user.email "youremail@domain.com”
    git config --list

## Install Gitea

### Create a git user for Gitea

    adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Git Version Control' \
    --group \
    --disabled-password \
    --home /home/git \
    git


### Install Gitea

    cd /tmp

    GITEAVERSION=1.15.7
    wget -O gitea https://dl.gitea.io/gitea/${GITEAVERSION}/gitea-${GITEAVERSION}-linux-amd64

    mv /tmp/gitea /usr/local/bin
    chmod +x /usr/local/bin/gitea
    mkdir -p /etc/gitea
    mkdir -p /var/lib/gitea/{custom,data,indexers,public,log}
    chown -R git:git /var/lib/gitea/
    chown root:git /etc/gitea
    chmod -R 750 /var/lib/gitea/
    chmod 770 /etc/gitea


    vi /etc/systemd/system/gitea.service

Put the config :

    [Unit]
    Description=Gitea
    After=syslog.target
    After=network.target
    After=postgresql.service

    [Service]
    RestartSec=2s
    Type=simple
    User=git
    Group=git
    WorkingDirectory=/var/lib/gitea/
    ExecStart=/usr/local/bin/gitea web -c /etc/gitea/app.ini
    Restart=always
    Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea

    [Install]
    WantedBy=multi-user.target

Save and exit the file. Then reload systemd daemon and start Gitea service using the below command:

    systemctl daemon-reload
    systemctl start gitea

Next, you need to enable Gitea service at system reboot:

    systemctl enable gitea

Next, verify the status of Gitea with the following command:

    systemctl status gitea

Edit the config */etc/gitea/app.ini* and relpace the localhost by the IP of the server. Restart the service.

Now, Access Gitea Web Interface through http://xxx:3000

You can configure the connection to PostgreSQL.