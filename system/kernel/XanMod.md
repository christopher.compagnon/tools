# How to Install XanMod Kernel on Ubuntu 20.04

XanMod is a free, open-source general-purpose Linux Kernel alternative to the stock kernel with Ubuntu 20.04 LTS. It features custom settings and new features and is built to provide a responsive and smooth desktop experience, especially for new hardware.

XanMod is popular amongst Linux Gaming, streaming, and ultra-low latency requirements and often boasts the latest Linux Kernels, having multiple branches to choose from the stable, edge, and development.

For more information on XanMod Kernel before installing, visit the XanMod Kernel features information page.

XanMod only supports x86_64 processor architecture.



## Update Operating System

Update your Ubuntu operating system to make sure all existing packages are up to date:

    sudo apt update && sudo apt upgrade -y

The tutorial will be using the sudo command and assuming you have sudo status.

To verify sudo status on your account:

    sudo whoami

Example output showing sudo status:

    $ sudo whoami
    root

To use the root account, use the following command with the root password to log in.

    su


## Import the XanMod Repository

The first task is to import the XanMod repository. This can be done in several ways, but the tutorial will use the apt package manager’s terminal to keep things simple.

First, import the repository using the following command.

    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list

Verify the command worked using the grep command.

    grep xanmod /etc/apt/sources.list.d/xanmod-kernel.list

Example output:

    deb http://deb.xanmod.org releases main

With the repository successfully imported, the next task is to import the GPG key using the following command.

    wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -

Example output if successful:

    OK

Next, update your APT repository source listing to reflect the new addition.

    sudo apt update

Lastly, before moving to the installation part, upgrade your packages as the repository will bring new versions of required packages to install the XanMod kernel.

    sudo apt upgrade


## Install XanMod Linux Kernel on Ubuntu

With the XanMod repository installed and up to date, you can install the latest Linux kernel from XanMod. The Edge onwards typically has the absolute latest bleeding stable kernel.

The tutorial will go over installing all three, but it’s recommended to stick to stable and edge, do not use development unless you know what you are doing.

To install XanMod stable:

    sudo apt install linux-xanmod

To install XanMod edge:

    sudo apt install linux-xanmod-edge

To install XanMod development:

    sudo apt install linux-xanmod-tt

Type Y, then press the ENTER KEY to proceed with the installation.

Once the chosen repository Linux kernel has been installed, you will need to reboot your system to complete the installation.

    sudo reboot

Once you have returned to your system, verify the installation.

At the tutorial time, the edge branch contained Linux kernel 5.15.2.

    hostnamectl

Or you can use the cat version command.

    cat /proc/version


## How to Update XanMod Kernel

To keep the Linux kernel you installed with XanMod up to date, you will run the standard apt update and upgrade commands if you installed XanMod using APT, which the tutorial has shown.

To check for updates.

    sudo apt update

If an upgrade is available.

    sudo apt upgrade

## How to Remove (Uninstall) XanMod Kernel

To remove XanMod, use the following command.

To Remove XanMod stable:

    sudo apt autoremove linux-xanmod --purge

To remove XanMod edge:

    sudo apt remove linux-xanmod-edge --purge

To remove XanMod development:

    sudo apt remove linux-xanmod-tt --purge

This will entirely remove all traces of XanMod possible and unused dependencies.

Ideally, you can remove the repository as well.

    sudo rm /etc/apt/sources.list.d/xanmod-kernel.list

Alternatively, you may need to remove the XanMod additional packages with the following.

First, you will need to use the exact name of the Linux image and header package. This can be found by using the following command dpkg –list command.

    dpkg --list | grep `uname -r`

The names shown in the output are the ones you need to remove that involve the XanMod Kernel.

    sudo apt-get remove linux-headers-5.15.2-xanmod1 linux-image-5.15.2-xanmod1 --purge

*Warning*, run the following command to make sure you have an existing Linux Kernel installed.

To check or re-install Ubuntu 20.04 LTS HWE Kernel:

    sudo apt install --install-recommends linux-generic-hwe-20.04 

To check or re-install Ubuntu 20.04 LTS Generic Kernel:

    sudo apt install --install-recommends linux-generic 

By default, the Ubuntu 20.04 default kernel should be installed.

Example output (HWE Kernel Rollback):

    linux-generic-hwe-20.04 is already the newest version (5.11.0.40.44~20.04.18).

Failure to check or do this will result in your system being unbootable.

ext, update-grub and initramfs.

    sudo update-grub && sudo update-initramfs -u

Now restart your PC to finalize the removal of XanMod.

    sudo reboot

Once you have returned to your system, verify the installation.

    hostnamectl

Or you can use the cat version command.

    cat /proc/version


As the above output has shown, you have rolled back to the default Linux kernel that Ubuntu 20.04 came with, thus successfully removing XanMod from your system.

## Comments and Conclusion

In the tutorial, you have learned how to install XanMod on Ubuntu 20.04 LTS Focal Fossa. XanMod is designed for desktop users with the latest hardware, but this can be installed on any Ubuntu desktop or server. However, it’s advised to research before upgrading, and it is ill-advised to use it on critical or production-type systems.

One of the best features of using XanMod is that when the next Linux Kernel comes around, it is included on development/edge very quickly. Hence, it is worth the look, especially if you are into Linux gaming.
