# How To Install "linux-lowlatency" Package on Ubuntu

## About RealTime Kernels

Early on in Linux audio production, Real-Time kernels were the only way to get low- and no-latency audio for professional audio applications. However, since Linux 2.6, the real-time stack has been part of the Linux kernel, having a kernel patched with a real-time stack is no longer necessary.

### RealTime Kernels Still Exist

However, there continued to be a demand for real-time kernels with a special patch. A patch does exist to enable process to have real-time process access to any process requesting it. This is good for applicance-like applications, such as audio mixers that use Linux (the Behringer X-series mixers and the Allen & Heath iLive series mixers are good examples). For desktop computer use, THIS IS A BAD IDEA.

### Security Implications

All it would take is one malicious process to execute and take advantage of the real-time code to completely lock-out a user from their machine, turning that machine into part of a botnet or other malicious purpose. Real-Time processes have the potential to completely take-over a machine. This is the number one reason Ubuntu does not carry a Real-Time kernel.

### Low-Latency Kernel

The Low-Latency Kernel included in Ubuntu Studio (and available in the Ubuntu repositories) does not allow such malicious code from locking-out a user from their machine. It does contain other optimizations, such as Preempt-RT being enabled in the kernel configuration, to achieve the lowest possible latency for audio and other applications, while keeping the user interface usable. Latency as low as 0.1 millisecond can and has been achieved using this kernel.

### Summary

For desktop computer usage, using a real-time kernel can cause security nightmares. The low-latency kernel included in Ubuntu Studio is completely capable of low- to no- latency while not enabling malicious processes to lock-out a user from their computer. 

## Update the system

    sudo apt update -y

## Install lowlatency kernel

    sudo apt-get install -y linux-lowlatency

Check the system logs to confirm that there are no related errors. Y

