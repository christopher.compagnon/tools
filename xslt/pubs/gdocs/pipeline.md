# Pipeline

The pipeline is defined as follows :

Google Docs --> LibreOffice (ODF) --> HTML --> XHTML

## Steps

1- write your document on *Google Docs*;
2- export to ODT from *Google Docs*;
3- open the ODT file with *LibreOffice Writer*;
4- export the ODT document as HTML from *LibreOffice Writer*;
5- perform the XSLT *odfhtml2xhtml.xsl* to generate a normalized XHTML document.

## odfhtml2xhtml.xsl

Parameters :

- **lang** : defines the language of the document. *en-US* by default.


## Cascading Style Sheet

**googledocs.css** displays the normalized XHTML document as *Google Docs*.



## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")