<?xml version="1.0" encoding="utf-8"?>
<!-- 
 ========================== Infos ===========================
 Description : 
 Auteur : C. Compagnon
 Courriel : admin@firenode.net
 Site : http://www.firenode.net
 ============================================================
 
 Version			date				Description
 ............................................................
 1.0				YYYY-MM-DD		Version initiale
 ............................................................
 
  Suppléments :

 ============================================================
 -->
<xsl:stylesheet 
version="1.0"
xmlns="http://www.w3.org/1999/xhtml"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xhtml="http://www.w3.org/1999/xhtml"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:pref="http://www.w3.org/2002/Math/preference"
> 

<!-- ========================== Imports =========================== -->

<!-- ========================== Output =========================== -->
<xsl:output 
method="xml" 
indent="yes"
encoding="utf-8"/>

<!-- ========================== Paramètres =========================== -->
<xsl:param name="lang">en-US</xsl:param>
<!-- ========================== Variables =========================== -->

<!-- ========================== Règles =========================== -->

<!-- ========================== Templates =========================== -->
<xsl:template match="/">

<html>
<head>
<title><xsl:value-of select="//xhtml:p[@class='P1']" /></title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="googledocs.css" />
</head>
<body xml:lang="{$lang}">
	
<article>

<header>

<!-- Titre principal -->
<h1><xsl:value-of select="//xhtml:p[@class='P1']" /></h1>
<!-- Sous titre -->
<xsl:if test="count(//xhtml:p[@class='Subtitle']) = 1"><p><xsl:value-of select="//xhtml:p[@class='Subtitle']" /></p></xsl:if>
</header>

<main>
<xsl:apply-templates />
</main>

<footer />
</article>
</body>
</html>

</xsl:template>

<xsl:template match="//xhtml:p[@class='Standard']">
<xsl:variable name="para">|<xsl:value-of select="normalize-space(.)" />|</xsl:variable>
<xsl:if test="$para != '| |'"><xsl:apply-templates select="." mode="copy"/></xsl:if>
</xsl:template>

<xsl:template match="child::node()" mode="copy">
<xsl:copy>
<xsl:apply-templates select="node()" mode="copy"/>
</xsl:copy>
</xsl:template>

<!-- ========================== actions par défaut =========================== -->
<xsl:template match="text()"/>

<!--
<xsl:template match="text()">
<xsl:value-of select="normalize-space()"/>
</xsl:template>
-->
</xsl:stylesheet>
