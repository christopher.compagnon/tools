#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) pdf-extract-images : extract images from PDF
#@(#)--------------------------------------------------------------
#@(#) Extract images from PDF using GhostScript
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -f number ] [ -l number ] [ -i file ] [ -o file ] [ -r number ] [ -c png|jpeg ] [ -z ] [ -h ]"
echo "-f (first) : first page. Optional."
echo "-l (last) : last page. Optional."
echo "-i (input) : input PDF file. Required."
echo "-c (convert) : output format, png or jpeg. Optional. jpeg as default value."
echo "-r (resolution) : resolution of pictures. Optional. 200 (dpi) as default value."
echo "-z (cbz) : create a cbz file."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
RESOLUTION=200
DEVICE="jpeg"
OUTPUT_EXT="jpg"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Gestion des options
while getopts ":f:l:i:o:c:r:zh" option 
do 
case ${option} in 

f)
# First page
FIRST_PAGE=${OPTARG}
HAS_FIRST="true"
FIRT_PAGE_CMD="-dFirstPage=${FIRST_PAGE}"
;;

l)
# Last page
LAST_PAGE=${OPTARG}
HAS_LAST="true"
LAST_PAGE_CMD="-dLastPage=${LAST_PAGE}"
;;

i)
# Input PDF file
INPUT_FILE="${OPTARG}"
HAS_INPUT="true"
;;

o)
# Input PDF file
OUTPUT_FILE="${OPTARG}"
HAS_OUTPUT="true"
;;

c)
# Convert
FORMAT="${OPTARG}"
HAS_FORMAT="true"
if [ "${FORMAT:-jpeg}" = "png" ]
then
DEVICE="png16m"
OUTPUT_EXT="png"
else
DEVICE="jpeg"
OUTPUT_EXT="jpg"
fi
;;

r)
# Resolution
RESOLUTION="${OPTARG}"
HAS_RESOLUTION="true"
;;

z)
# Genere a cbz format
HAS_CBZ="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
usage
exit 1
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_INPUT:-false}" != "true" ] && print_error "Option -i is missing !" && exit 1
[ ! -f "${INPUT_FILE}" ] && print_error "File ${INPUT_FILE} not found" && exit 1
#------------------------------------------------------------------
# Create output directory if not exist.
OUPUT_DIR="$(dirname ${INPUT_FILE})"
FILE_NAME="$(basename ${INPUT_FILE})"
ROOT_FILE="${FILE_NAME%.*}"
EXT_FILE="${FILE_NAME##*.}"
mkdir -p "${OUPUT_DIR}/${ROOT_FILE}"

# Extraction
gs -dNOPAUSE -dBATCH -r${RESOLUTION} -sDEVICE=${DEVICE} ${FIRST_PAGE_CMD} ${LAST_PAGE_CMD} -sOutputFile="${OUPUT_DIR}/${ROOT_FILE}/%06d.${OUTPUT_EXT}" "${INPUT_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Error during the extraction" && exit 1

#------------------------------------------------------------------
# Generate a cbz
[ "${HAS_CBZ:-false}" = "true" ] && {
[ -f "${OUPUT_DIR}/${ROOT_FILE}.cbz" ] && rm -f "${OUPUT_DIR}/${ROOT_FILE}.cbz"
zip "${OUPUT_DIR}/${ROOT_FILE}.cbz" ${OUPUT_DIR}/${ROOT_FILE}/*.${OUTPUT_EXT}
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Error during the compression" && exit 1
[ -d "${OUPUT_DIR}/${ROOT_FILE}" ] && rm -fR "${OUPUT_DIR}/${ROOT_FILE}"
}

echo "Extraction done successfully!"
#------------------------------------------------------------------
exit 0