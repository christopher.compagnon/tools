#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) pdf-extract-pages : extract pages from PDF
#@(#)--------------------------------------------------------------
#@(#) Extract pages from PDF using GhostScript
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - page list management
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -f integer ] [ -l integer ] [ -p options ] [ -i file ] [ -o file ] [ -h ]"
echo "-f (first) : first page."
echo "-l (last) : last page."
echo "-p (pages) : list of pages. Values : odd, even, or list of pages like 1,3,5-8,12-,…"
echo "-i (input) : input PDF file. Required."
echo "-o (output) : output PDF file. Required."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
PAGE_LIST=""
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Gestion des options
while getopts ":f:l:i:o:p:h" option 
do 
case ${option} in 

f)
# First page
FIRST_PAGE="-dFirstPage=${OPTARG}"
HAS_FIRST="true"
;; 

l)
# Last page
LAST_PAGE="-dLastPage=${OPTARG}"
HAS_LAST="true"
;;

i)
# Input PDF file
INPUT_FILE="${OPTARG}"
HAS_INPUT="true"
;; 

o)
# Input PDF file
OUTPUT_FILE="${OPTARG}"
HAS_OUTPUT="true"
;;

p)
# Pages
PAGES="${OPTARG}"
HAS_PAGES="true"
PAGE_LIST="-sPageList=\"${OPTARG}\""
;;

h)
# usage
usage
exit 0
;;

\?) 
echo "Invalid option ${option}!"
usage
exit 1
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_PAGES:-false}" != "true" -a "${HAS_FIRST:-false}" != "true" ] && print_error "Options -f or -p are missing !" && exit 1
[ "${HAS_PAGES:-false}" != "true" -a "${HAS_LAST:-false}" != "true" ] && print_error "Option -l or -p are missing !" && exit 1
[ "${HAS_INPUT:-false}" != "true" ] && print_error "Option -i is missing !" && exit 1
[ "${HAS_OUTPUT:-false}" != "true" ] && print_error "Option -o is missing !" && exit 1

[ ! -f "${INPUT_FILE}" ] && print_error "File ${INPUT_FILE} not found" && exit 1
#------------------------------------------------------------------
# Create output directory if not exist.
OUTPUT_DIR=$(dirname "${OUTPUT_FILE}")
mkdir -p "${OUTPUT_DIR}"

# Extraction
#echo "gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dAutoRotatePages=/None ${FIRST_PAGE} ${LAST_PAGE} ${PAGE_LIST} -sOutputFile=\"${OUTPUT_FILE}\" ${ROTATION} \"${INPUT_FILE}\""
gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dAutoRotatePages=/None ${FIRST_PAGE} ${LAST_PAGE} ${PAGE_LIST} -sOutputFile="${OUTPUT_FILE}" -f "${INPUT_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Error during the extraction" && exit 1

# [ TODO ] : rotation --> does not work
# add -c "<</Orientation 0>> setpagedevice"
# delete -dAutoRotatePages=/None
# with 
#* -c "<</Orientation 3>> setpagedevice" -- sets landscape orientation;
#* -c "<</Orientation 0>> setpagedevice" -- sets portrait orientation;
#* -c "<</Orientation 2>> setpagedevice" -- sets upside down orientation;
#* -c "<</Orientation 1>> setpagedevice" -- sets seascape orientation. 

#------------------------------------------------------------------
exit 0