#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) pdf-extract-pages-gui : Zenity interface for pdf-extract-pages 
#@(#)--------------------------------------------------------------
#@(#) Zenity interface for pdf-extract-pages 
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.1 : multi-language display management
#@(#) Version : 1.2 : even/odd pages, best display of options
#@(#) Version : 1.3 - auto-selection of language
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
FIRST_PAGE=""
LAST_PAGE=""
PAGES=""
#LIST_PAGES_VALUES=$(echo ${LIST_PAGES} | tr '|' '\n' | awk -F'=' '{print $1}' | tr '\n' '|' | sed 's/|$//g')
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/pdf-extract-pages.${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/pdf-extract-pages.${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Gestion des options
while getopts ":i:" option 
do 
case ${option} in 

i)
# Input PDF file
INPUT_FILE="${OPTARG}"
HAS_INPUT="true"
;; 

\?) 
zenity --error --text="${option} ${ERROR_INVALID_OPTION}"
exit 1
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_INPUT:-false}" = "true" -a ! -f "${INPUT_FILE}" ] && zenity --error --text="${INPUT_FILE} ${ERROR_FILE_NOT_FOUND}" && exit 1
#------------------------------------------------------------------

# Input PDF directory + file
[ "${HAS_INPUT:-false}" != "true" ] && INPUT_FILE=$(zenity --file-selection --file-filter="*.pdf" --title="${TITLE_INPUT_FILENAME}")
# If cancel…
[ "${INPUT_FILE:-none}" = "none" ] && zenity --error --text="${WARNING_CANCEL}" && exit 0

# Output PDF directory
OUTPUT_DIR=$(zenity --file-selection --directory --title="${TITLE_OUTPUT_DIRECTORY}")
# If cancel…
[ "${OUTPUT_DIR:-none}" = "none" ] && zenity --error --text="${WARNING_CANCEL}" && exit 0 

# Output PDF file
OUTPUT_FILE=$(zenity --entry --title="${TITLE_OUTPUT_FILENAME}" --text="${LABEL_OUTPUT_FILENAME}")
# If cancel…
[ "${OUTPUT_FILE:-none}" = "none" ] && zenity --error --text="${WARNING_CANCEL}" && exit 0 

#------------------------------------------------------------------
# 1st set of options
OPTIONS_1=$(zenity --forms --title="${TITLE_OPTIONS_1}" \
--text="${LABEL_OPTIONS_1}" \
--add-entry="${LABEL_FIRST_PAGE}" \
--add-entry="${LABEL_LAST_PAGE}" \
--add-entry="${LABEL_PAGES}" 
#--add-list="${LABEL_EXTRACTION}" \
#--list-values="${LIST_PAGES_VALUES}"
)

RETURN=${?}

case ${RETURN} in

0)
# Validated

;;

1)
# Canceled
exit 0
;;

*)
# Other (something went wrong)
exit 1
;;

esac

PARAM_FIRST_PAGE=$(echo ${OPTIONS_1} | awk -F'|' '{print $1}')
PARAM_LAST_PAGE=$(echo ${OPTIONS_1} | awk -F'|' '{print $2}')
PARAM_PAGES=$(echo ${OPTIONS_1} | awk -F'|' '{print $3}')

#EXTRACTION_VALUE=$(echo ${OPTIONS_1} | awk -F'|' '{print $1}')
#EXTRACTION_PAGES=$(echo ${LIST_PAGES} | tr '|' '\n' | awk -F'=' -v rotation="${EXTRACTION_VALUE}" '($1==rotation){print $2}')
#[ "${EXTRACTION_PAGES:-none}" = "all" ] && PARAM_PAGES="none"

# Controls
HAS_EVEN=$(echo ${PARAM_PAGES} | grep 'even' | wc -l)
HAS_ODD=$(echo ${PARAM_PAGES} | grep 'odd' | wc -l)
HAS_SEP=$(echo ${PARAM_PAGES} | grep ',' | wc -l)
HAS_DASH=$(echo ${PARAM_PAGES} | grep '-' | wc -l)
[ ${HAS_EVEN:-0} -gt 0 -a ${HAS_ODD:-0} -gt 0 ] && zenity --width="300" --error --text="${WARNING_CANCEL}" && exit 1
[ ${HAS_EVEN:-0} -gt 0 -a ${HAS_SEP:-0} -gt 0 ] && zenity --width="300" --error --text="${WARNING_CANCEL}" && exit 1
[ ${HAS_EVEN:-0} -gt 0 -a ${HAS_DASH:-0} -gt 0 ] && zenity --width="300" --error --text="${WARNING_CANCEL}" && exit 1
[ ${HAS_ODD:-0} -gt 0 -a ${HAS_SEP:-0} -gt 0 ] && zenity --width="300" --error --text="${WARNING_CANCEL}" && exit 1
[ ${HAS_ODD:-0} -gt 0 -a ${HAS_DASH:-0} -gt 0 ] && zenity --width="300" --error --text="${WARNING_CANCEL}" && exit 1

[ "${PARAM_PAGES:-none}" != "none" ] && PAGES="-p \"${PARAM_PAGES}\""
[ "${PARAM_FIRST_PAGE:-none}" != "none" ] && FIRST_PAGE="-f ${PARAM_FIRST_PAGE}"
[ "${PARAM_LAST_PAGE:-none}" != "none" ] && LAST_PAGE="-l ${PARAM_LAST_PAGE}"

#------------------------------------------------------------------
# Call the script
${SCRIPT_DIR}/pdf-extract-pages.sh ${FIRST_PAGE} ${LAST_PAGE} ${PAGES} -i "${INPUT_FILE}" -o "${OUTPUT_DIR}/${OUTPUT_FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && zenity --width="300" --error --text="${ERROR_EXTRACTION}"
[ ${RETURN:-0} -eq 0 ] && zenity --width="300" --info --text="${SUCCESS_EXTRACTION}"
#------------------------------------------------------------------
exit 0