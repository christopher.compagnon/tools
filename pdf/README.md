# PDF tools

Tools to manipulate PDF files.

## PDF extract pages

2 tools available:

- the core script **pdf-extract-pages.sh** : 

    pdf-extract-pages.sh -f 2 -l 3 -i "/path/to/my_source.pdf" -o "/path/to/my_target.pdf"

Extracts the pages from 2 to 3 of *my_source.pdf* and creates a file *my_target.pdf*.

    pdf-extract-pages.sh -p "1,3,5-8" -i "/path/to/my_source.pdf" -o "/path/to/my_target.pdf"

Extracts pages 1,3 and 5 to 8 of *my_source.pdf* and creates a file *my_target.pdf*.

    pdf-extract-pages.sh -p "even" -i "/path/to/my_source.pdf" -o "/path/to/my_target.pdf"

Extracts even pages of *my_source.pdf* and creates a file *my_target.pdf*. Works also with *-p "odd"*.

- the GUI (Zenity) **pdf-extract-pages-gui.sh**. Simply launches the script or configures it for Actions.

If you want to integrate it in Actions, you have to use the -i option as:

    pdf-extract-pages-gui.sh -i "/path/to/my_source.pdf"

### Multi-language management

The *pdf-extract-pages.xx.lang* file provides a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *pdf-extract-pages.en.lang* file as *pdf-extract-pages.xx.lang* to support your language.


## PDF extract images

pdf-extract-images.sh can be used to convert PDF to images as png or jpeg with options:

- **f (first)** : first page. *Optional*.
- **l (last)** : last page. *Optional*.
- **i (input)** : input PDF file. *Required*.
- **c (convert)** : output format, png or jpeg. *Optional*. jpeg as default value.
- **r (resolution)** : resolution of pictures. *Optional*. 200 (dpi) as default value.
- **z (cbz)** : create a cbz file. *Optional*.
- **h (help)** : display help.

### Examples

    pdf-extract-images.sh -i "/path/to/my_source.pdf"

converts the file *my_source.pdf* into jpeg (200 DPI by default).

    pdf-extract-images.sh -i "/path/to/my_source.pdf" -r 300

converts the file *my_source.pdf* into jpeg, 300 DPI.

    pdf-extract-images.sh -i "/path/to/my_source.pdf" -r 300 -z

converts the file *my_source.pdf* into jpeg 300 DPI and create a cbz file. zip is required as dependency.

    pdf-extract-images.sh -f 2 -l 15 -i "/path/to/my_source.pdf" -r 300
    
converts the pages from 2 to 15 of *my_source.pdf* into jpeg at 300 DPI.

    pdf-extract-images.sh -i "/path/to/my_source.pdf" -c png

converts the file *my_source.pdf* into png. 

## PDF concat

Graphical (Zenity) tool for PDF concatenation.

- **install.app-launcher.sh** : graphical installer for a launcher of *pdf-concat-gui.sh*.

- **pdf-concat-gui.sh** : graphical application to concat 2 or more PDF files.

### Multi-language management

The *pdf-concat.xx.lang* file provides a translation for GUI.

If the file in your language does not exist, please feel free to copy+modify an existing one with the right language code.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
