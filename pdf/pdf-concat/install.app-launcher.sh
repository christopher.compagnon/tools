#!/bin/sh
# set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Install a launcher for pdf-concat
#@(#)--------------------------------------------------------------
#@(#) Graphical (Zenity) installer for the pdf-concat's launcher
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
APP_LAUNCHER="${HOME}/.local/share/applications/pdf-concat-launcher.desktop"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/pdf-concat.${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/pdf-concat.${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Informations à propos du lanceur
LAUNCHER_INFOS=$(zenity --forms --width=300 --title="${TITLE_LAUNCHER_INFOS}" \
	--text="${LABEL_LAUNCHER_INFOS}" \
	--separator="|" \
	--add-entry="${LABEL_LAUNCHER_NAME}" \
	--add-entry="${LABEL_LAUNCHER_COMMENT}")
RETURN_LAUNCHER=${?}

# Cas de l'annulation de création
[ ${RETURN_LAUNCHER:-1} -eq 1 ] && {
zenity --warning --width=300 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_LAUNCHER:-1} -ne 0 ] && {
zenity --error --width=300 --text="${ERROR_UNEXPECTED}"
exit 1
}

#------------------------------------------------------------------
# Création du lanceur
LAUNCHER_LABEL="$(echo "${LAUNCHER_INFOS}" | cut -d '|' -f1)"
LAUNCHER_COMMENT="$(echo "${LAUNCHER_INFOS}" | cut -d '|' -f2)"

echo "[Desktop Entry]" > ${APP_LAUNCHER}
echo "Encoding=UTF-8" >> ${APP_LAUNCHER}
echo "Name=${LAUNCHER_LABEL}" >> ${APP_LAUNCHER}
echo "GenericName=Launcher" >> ${APP_LAUNCHER}
echo "Comment=${LAUNCHER_COMMENT}" >> ${APP_LAUNCHER}
echo "Exec=$(pwd)/pdf-concat-gui.sh" >> ${APP_LAUNCHER}
echo "Icon=$(pwd)/Icon_pdf_file.svg" >> ${APP_LAUNCHER}
echo "Type=Application" >> ${APP_LAUNCHER}
echo "Terminal=false" >> ${APP_LAUNCHER}
echo "Categories=Application;" >> ${APP_LAUNCHER}
echo "StartupNotify=True" >> ${APP_LAUNCHER}

# contrôle
[ ! -f "${APP_LAUNCHER}" ] && {
zenity --error --width=300 --text="${ERROR_CREATION}"
exit 1
}

# Info succès !
zenity --info --width=300 --text="${SUCCESS_LAUNCHER_CREATION}"
#------------------------------------------------------------------
exit 0
