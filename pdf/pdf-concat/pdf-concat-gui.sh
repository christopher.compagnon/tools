#!/bin/sh
#set -x
# Concatène des fichiers pdf
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) pdf-concat-gui : Graphical (Zenity) tool for PDF concatenation 
#@(#)--------------------------------------------------------------
#@(#) Graphical (Zenity) tool for PDF concatenation 
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
INPUT_LIST_FILE=""
NUMBER_OF_FILES=2
N=0
TMP_DIR="${HOME}/.pdf-concat"
LIST_FILE_DIR="${TMP_DIR}/$$.lst"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/pdf-concat.${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/pdf-concat.${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Création du répertoire de travail
mkdir -p "${TMP_DIR}"
# Nettoyage du répertoire
rm -f "${TMP_DIR}/*.pdf"
rm -f "${TMP_DIR}/*.lst"

# Choix des fichiers PDF d'entrée

while [ ${NUMBER_OF_FILES} -gt 0 ]
do
N=$(expr ${N} + 1)

# Choix du fichier PDF source
INPUT_FILE=$(zenity --file-selection --title="${TITLE_INPUT_FILENAME} n°${N}" --file-filter="*.pdf")
OK_INPUT=${?}

case ${OK_INPUT} in

0)
TMP_FILE="${TMP_DIR}/$$.${N}.pdf"
# Normalisation : recopie des fichiers source dans le répertoire de travail pour éviter les problèmes de caractères spéciaux et espaces
cp "${INPUT_FILE}" "${TMP_FILE}"
[ ! -f "${TMP_FILE}" ] && zenity --error --width=300 --text="${ERROR_UNEXPECTED}"

FILE_NAME=$(basename "${INPUT_FILE}")
echo "${N} : ${FILE_NAME}" >> "${LIST_FILE_DIR}"
INPUT_LIST_FILE="${INPUT_LIST_FILE} \"${TMP_FILE}\""
NUMBER_OF_FILES=$(expr ${NUMBER_OF_FILES} - 1)
;;

1)
zenity --error --width=300 --text="${ERROR_NO_FILE_SELECTED}"
exit 1
;;

-1)
zenity --error --width=300 --text="${ERROR_UNEXPECTED}"
exit 1
;;

esac

#------------------------------------------------------------------
# Choix de fichiers supplémentaires

if [ ${NUMBER_OF_FILES} -eq 0 ]
then
zenity --question --width=300 --text="${LABEL_ADD_FILE}"
RETURN=${?}
[ ${RETURN:-1} -eq 0 ] && NUMBER_OF_FILES=$(expr ${NUMBER_OF_FILES} + 1)
fi


done

#------------------------------------------------------------------
# Récapitulatif
zenity --question --width=300 --title="${TITLE_SUMMARY}" --text="$(cat ${LIST_FILE_DIR} | sed 's/$/ \\n/')"
OK_RECAP=${?}
[ ${OK_RECAP:-1} -ne 0 ] && exit 0

#------------------------------------------------------------------
# Choix du fichier de sortie
READY_TO_WRITE="false"

while [ "${READY_TO_WRITE:-false}" = "false" ]
do 

OUTPUT_FILE=$(zenity --file-selection --save --title="${TITLE_OUTPUT_FILENAME}" --file-filter="*.pdf")
OK_OUTPUT=${?}
[ ${OK_OUTPUT:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_UNEXPECTED}"

# Fichier sortie existe ?
if [ -f "${OUTPUT_FILE}" ]
then
FILE_NAME=$(basename "${OUTPUT_FILE}")
zenity --question --width=300 --text="${LABEL_FILE_ALREADY_EXISTS}"
OK_OVERWRITE=${?}

case ${OK_OVERWRITE} in

0)
READY_TO_WRITE="true"
;;

1)
READY_TO_WRITE="false"
;;

-1)
zenity --error --width=300 --text="${ERROR_UNEXPECTED}" 
exit 1
;;

esac

else
READY_TO_WRITE="true"
fi

done


#------------------------------------------------------------------
# Validation ?

zenity --question --width=300 --text="${LABEL_CREATION_CONFIRM}"
RETURN=${?}

case ${RETURN} in

0)
#echo "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=\"${OUTPUT_FILE}\" ${INPUT_LIST_FILE}"
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile="${OUTPUT_FILE}" ${INPUT_LIST_FILE}
OK_PRINT=${?}

[ ${OK_PRINT:-1} -eq 0 ] && zenity --info --width=300 --text="${SUCCESS_CREATION}"
[ ${OK_PRINT:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_CREATION}"
# Nettoyage du répertoire
rm -f "${TMP_DIR}/*.pdf"
rm -f "${TMP_DIR}/*.lst"
;;

1)
zenity --warning --width=300 --text="${WARNING_CANCEL}"
exit 0
;;

-1)
zenity --error --width=300 --text="${ERROR_UNEXPECTED}"
exit 1
;;

esac

#------------------------------------------------------------------
exit 0
