# Gnome Instyle

Install a theme for Gnome from a compressed file.

This tool uses *Zenity*. Make sure it is available.

## Installation

Download and uncompress the sources.

Make sure *gnome-instyle.sh* is executable.

## Usage

There are 2 ways to run the script : admin or user.

If you launch it as admin (root), you will install it on the system available for all users.

If you launch it as user, you will install it on your local environment. In that case, you also can apply it directly from the tool.

Launch *gnome-instyle.sh* as user :

    ./gnome-instyle.sh

Launch *gnome-instyle.sh* as root :

    sudo ./gnome-instyle.sh

Then follow the instructions displayed by GUI.

## Multi-language management

The *xx.lang* file provides a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* (with xx = code of lang) to support your language.

To find the code of the lang used on your system, please run :

    echo ${LANG} | cut -c1-2

in your terminal.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")