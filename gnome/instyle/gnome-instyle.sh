#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) gnome-instyle : Install a theme for Gnome from package
#@(#)--------------------------------------------------------------
#@(#) Install a theme for Gnome from a compressed file
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.2
#------------------------------------------------------------------
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - fix bug on UID
#@(#) Version : 1.2 - management of multiple themes with the same name
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

extract () {
if [ -f "${1}" ]
then

case ${1} in

*.tar.bz2)
tar xjf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.gz)
tar xzf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.bz2)
bunzip2 ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.rar)
unrar x ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.gz)
gunzip ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar)
tar xf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tbz2)
tar xjf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tgz)
tar xzf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.zip)
unzip ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.Z)
uncompress ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.7z)
7z x ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.deb)
ar x ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.xz)
tar xf ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.zst)
unzstd ${1}
[ ${?} -ne 0 ] && zenity --error --text="${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*)
zenity --error --text="${ERROR_UNKNWON_PKG_TYPE}" && exit 1
;;

esac

else
zenity --error --text="${1} ${ERROR_FILE_NOT_FOUND}" && exit 1
fi
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)

if [ ${P_UID:--1} -ne 0 ]
then
THEME_DIR="${HOME}/.local/share/themes"
BACKGROUND_DIR="${HOME}/.local/share/backgrounds"
ICON_DIR="${HOME}/.local/share/icons"
CURSOR_DIR="${HOME}/.local/share/icons"
fi

if [ ${P_UID:--1} -eq 0 ]
then
THEME_DIR="/usr/share/themes"
BACKGROUND_DIR="/usr/share/backgrounds"
ICON_DIR="/usr/share/icons"
CURSOR_DIR="/usr/share/icons"
fi

TMP_DIR="/dev/shm"
#TMP_DIR="$(pwd)/tests"
mkdir -p "${TMP_DIR}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------

# Input directory + file
INPUT_FILE=$(zenity --file-selection --title="${TITLE_INPUT_FILENAME}")
# If cancel…
[ "${INPUT_FILE:-none}" = "none" ] && zenity --error --text="${WARNING_CANCEL}" && exit 0

FILE_NAME="$(basename ${INPUT_FILE})"
# Copy the file into TMP_DIR
WRK_DIR="${TMP_DIR}/$$"
mkdir -p "${WRK_DIR}"
cp "${INPUT_FILE}" "${WRK_DIR}/"
WRK_FILE="${WRK_DIR}/${FILE_NAME}"
FILE_DIR="$(dirname ${INPUT_FILE})"

# Décompresse le paquet
cd "${WRK_DIR}/"
extract "${WRK_FILE}"
[ "${IS_EXTRACTED:-none}" != "true" ] && zenity --error --text="${INPUT_FILE} ${ERROR_EXTRACTION}" && exit 0

#------------------------------------------------------------------
# Installation
THEME_ID=0
THEME_IDX="${WRK_DIR}/themes.idx"
THEME_LST_FILE="${WRK_DIR}/themes.lst"
echo "ID|Path|Name|Type" > "${THEME_IDX}"
# Recherche index.theme
find ${WRK_DIR}/ -type f -name "index.theme" -print | while read IDX_FILE
do
PKG_DIR="$(dirname ${IDX_FILE})"
#PKG_NAME="$(dirname ${IDX_FILE} | xargs basename)"
THEME_NAME="$(cat ${IDX_FILE} | grep "Name=" | head -1 | cut -d'=' -f2)"
THEME_ID=$( expr ${THEME_ID:-0} + 1 )
# Détermination du type d'installation (thème, icônes, curseurs)
# Note : The cursor theme directory structure is theme-name/cursors, for example: ~/.local/share/icons/theme/cursors/; make sure the extracted files follow this structure. 
# Type
IS_ICONS=$(cat ${IDX_FILE} | tr '[a-z]' '[A-Z]' | grep '\[ICON THEME\]' | wc -l)
# Si IS_ICONS > 0 alors icônes/curseurs, sinon theme
[ ${IS_ICONS:-0} -gt 0 ] && IS_THEME=0
[ ${IS_ICONS:-0} -eq 0 ] && IS_THEME=1
[ ${IS_ICONS:-0} -gt 0 -a -d "${PKG_DIR}/cursors" ] && IS_CURSORS=1

# THEME_TYPE
[ ${IS_THEME:-0} -eq 1 ] && THEME_TYPE="theme"
[ ${IS_ICONS:-0} -eq 1 -a ${IS_CURSORS:-0} -eq 1 ] && THEME_TYPE="cursors"
[ ${IS_ICONS:-0} -eq 1 -a ${IS_CURSORS:-0} -eq 0 ] && THEME_TYPE="icons"

echo "${THEME_ID}|${PKG_DIR}|${THEME_NAME}|${THEME_TYPE:-none}" >> "${THEME_IDX}"
done

# Teste si le fichier de configuration est valide
HAS_NO_CONFIG=$(cat "${THEME_IDX}" | wc -l)
[ ${HAS_NO_CONFIG:-0} -le 1 ] && zenity --error --width=300 --text="${ERROR_NO_CONFIG_FOUND}" && exit 1

# Teste si les configurations sont définies
HAS_UNKNWON_THEME=$(echo "${THEME_IDX}" | grep '\[none\]' | wc -l)
[ ${HAS_UNKNWON_THEME:-0} -gt 0 ] && zenity --error --width=300 --text="${ERROR_UNKNWON_CONFIG_FOUND}" && exit 1

# Il peut y avoir plusieurs Noms identiques pour des chemins différents

# Choisir les thèmes disponibles à installer
THEMES_LIST=$(cat ${THEME_IDX} | awk -F'|' '(NR > 1) {print "FALSE;" $1 ";" $3 " [" $4 "]"}' | tr ';' '\n' | zenity --list --checklist  --separator="|" --title="${TITLE_THEMES_CHOOSE}" --column="#" --column="${THEMES_CHOOSE_COL1}" --column="${THEMES_CHOOSE_COL2}" --width=500 --height=300)
RETURN_THEMES=${?}

# Cas de l'annulation de sélection
[ ${RETURN_THEMES:-1} -eq 1 ] && {
zenity --error --width=300 --text="${ERROR_NO_THEME_DEFINED}"
exit 1
}

# Installer les thèmes sélectionnés
echo "${THEMES_LIST}" | tr '|' '\n' | while read THEME_ID
do
cat "${THEME_IDX}" | awk -v ID="${THEME_ID}" -F'|' '(NR > 1 && $1 == ID) {print $0}' >>  ${THEME_LST_FILE}
THEME_NAME="$(cat ${THEME_IDX} | awk -v ID="${THEME_ID}" -F'|' '(NR > 1 && $1 == ID) {print $3}')"
THEME_TYPE="$(cat ${THEME_IDX} | awk -v ID="${THEME_ID}" -F'|' '(NR > 1 && $1 == ID) {print $4}')"
THEME_PATH="$(cat ${THEME_IDX} | awk -v ID="${THEME_ID}" -F'|' '(NR > 1 && $1 == ID) {print $2}')"
THEME_ROOT_DIR="$( basename ${THEME_PATH} )"

[ "${THEME_TYPE:-none}" = "theme" ] && {
mkdir -p "${THEME_DIR}"
cp -R "${THEME_PATH}" "${THEME_DIR}/${THEME_ROOT_DIR}/"
}

[ "${THEME_TYPE:-none}" = "icons" ] && {
mkdir -p "${ICON_DIR}"
cp -R "${THEME_PATH}" "${ICON_DIR}/${THEME_ROOT_DIR}/"
}

[ "${THEME_TYPE:-none}" = "cursors" ] && {
mkdir -p "${CURSOR_DIR}"
cp -R "${THEME_PATH}" "${CURSOR_DIR}/${THEME_ROOT_DIR}/"
}

done

#------------------------------------------------------------------
# Choisir le thème à activer
HAS_THEME=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "theme") {print $3}' | wc -l)
if [ ${HAS_THEME:-0} -gt 0 -a ${P_UID:--1} -ne 0 ]
then
THEME_REQUEST=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "theme") {print "FALSE;" $1 ";" $3 " [" $4 "]"}' | tr ';' '\n' | zenity --list --radiolist --title="${TITLE_THEME_APPLY}" --column="#" --column="${CONFIG_CHOOSE_COL1}" --column="${CONFIG_CHOOSE_COL2}" --width=500 --height=300)
RETURN_CONFIGURATION=${?}

# Cas de l'annulation de sélection
[ ${HAS_THEME:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 1 ] && {
zenity --warning --width=300 --text="${WARNING_NO_THEME_DEFINED}"
#exit 0
}

# Activer le thème sélectionné
[ ${HAS_THEME:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 0 ] && {
#THEME_NAME="$(echo ${THEME_REQUEST} | sed 's/ \[/|/g' | cut -d'|' -f1)"
THEME_NAME="$(cat ${THEME_IDX} | awk -v ID="${THEME_REQUEST}" -F'|' '(NR > 1 && $1 == ID) {print $3}')"
gsettings set org.gnome.desktop.interface gtk-theme ${THEME_NAME}
gsettings set org.gnome.desktop.wm.preferences theme ${THEME_NAME}

zenity --notification --text="${THEME_NAME} ${SUCCESS_DONE}"	
}
fi

#------------------------------------------------------------------
# Choisir les icônes à activer
HAS_ICONS=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "icons") {print $3}' | wc -l)
if [ ${HAS_ICONS:-0} -gt 0 -a ${P_UID:--1} -ne 0 ]
then
ICONS_REQUEST=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "theme") {print "FALSE;" $1 ";" $3 " [" $4 "]"}' | tr ';' '\n' | zenity --list --radiolist --title="${TITLE_ICONS_APPLY}" --column="#" --column="${CONFIG_CHOOSE_COL1}" --column="${CONFIG_CHOOSE_COL2}" --width=500 --height=300)
RETURN_CONFIGURATION=${?}

# Cas de l'annulation de sélection
[ ${HAS_ICONS:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 1 ] && {
zenity --warning --width=300 --text="${WARNING_NO_ICONS_DEFINED}"
#exit 0
}

# Activer les icônes sélectionnées
[ ${HAS_THEME:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 0 ] && {
THEME_NAME="$(cat ${THEME_IDX} | awk -v ID="${ICONS_REQUEST}" -F'|' '(NR > 1 && $1 == ID) {print $3}')"
gsettings set org.gnome.desktop.interface icon-theme ${THEME_NAME}

zenity --notification --text="${THEME_NAME} ${SUCCESS_DONE}"	
}
fi
#------------------------------------------------------------------
# Choisir les curseurs à activer
HAS_CURSORS=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "cursors") {print $3}' | wc -l)
if [ ${HAS_CURSORS:-0} -gt 0 -a ${P_UID:--1} -ne 0 ]
then
CURSORS_REQUEST=$(cat ${THEME_LST_FILE} | awk -F'|' '($4 == "cursors") {print $3}' | tr ';' '\n' | zenity --list --radiolist --title="${TITLE_CURSORS_APPLY}" --column="#" --column="${CONFIG_CHOOSE_COL1}" --column="${CONFIG_CHOOSE_COL2}" --width=500 --height=300)
RETURN_CONFIGURATION=${?}

# Cas de l'annulation de sélection
[ ${HAS_CURSORS:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 1 ] && {
zenity --warning --width=300 --text="${WARNING_NO_CURSORS_DEFINED}"
#exit 0
}

# Activer les icônes sélectionnées
[ ${HAS_CURSORS:-0} -gt 0 -a ${RETURN_CONFIGURATION:-1} -eq 0 ] && {
THEME_NAME="$(cat ${THEME_IDX} | awk -v ID="${CURSORS_REQUEST}" -F'|' '(NR > 1 && $1 == ID) {print $3}')"
gsettings set org.gnome.desktop.interface cursor-theme ${THEME_NAME}

#Change the cursor size with (depending on the theme, sizes are 24, 32, 48, 64):
#gsettings set org.gnome.desktop.interface cursor-size cursor_theme_size

zenity --notification --text="${THEME_NAME} ${SUCCESS_DONE}"	
}
fi
#------------------------------------------------------------------
zenity --info  --width=300 --text="${SUCCESS_CONFIG_DONE}"
#------------------------------------------------------------------
# Nettoyage
[ -d "${WRK_DIR}" ] && rm -Rf "${WRK_DIR}"
#------------------------------------------------------------------
exit 0
