#!/bin/sh
# Gestion d'un diaporama
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Diaporama : rotates the wallpapers
#@(#)--------------------------------------------------------------
#@(#) Rotate the wallpapers
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage: ${SCRIPT_NAME} [-m integer] [-h]"
echo "-m (mode) : 0 = alphabetical (default value), 1 = shuffle"
echo "-h (help) : affichage de l'aide."
}

is_number () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
DATA_DIR="${HOME}/.diaporama"
DATA_FILE="${DATA_DIR}/index.dat"
HASH_FILE="${DATA_DIR}/hash.dat"
IMG_DIR="${DATA_DIR}/wallpapers"
RUN_FILE="${DATA_DIR}/index.run"
LAST_EXEC_FILE="${DATA_DIR}/last.run"
ETC_FILE="${DATA_DIR}/diaporama.conf"
[ -f "${ETC_FILE}" ] && . "${ETC_FILE}"
# MODE in (alpha, shuffle)
MODE="alpha"
CURRENT_EXEC=$(date -u '+%s')
#------------------------------------------------------------------
# Paramètres
#------------------------------------------------------------------
# Récupération des options
while getopts ":m:h" OPTION
do

case "${OPTION}" in

"m")
# mode
MODE_VALUE=${OPTARG}
IS_NUMBER=$(is_number ${MODE_VALUE})
[ ${IS_NUMBER:-1} -ne 0 ]  && print_error "Sorry, ${MODE_VALUE} is not an integer." && exit 1
[ "${MODE_VALUE:-0}" = "1" ] && MODE="shuffle"
;;

"h")
# help
usage
exit 0
;;


?)
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Contrôles
[ ! -f "${ETC_FILE}" ] && print_error "Please configure the diaporama first!" && exit 1

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Execution possible ?
[ ! -f ${LAST_EXEC_FILE} ] && LAST_EXEC=${CURRENT_EXEC}
[ -f ${LAST_EXEC_FILE} ] && LAST_EXEC=$(cat ${LAST_EXEC_FILE} | grep "^LAST_EXEC=" | cut -d'=' -f2)

# Différence (en minutes)
DIFF_MINUTES=$(expr \(${CURRENT_EXEC} - ${LAST_EXEC} \) \/ 60)

# Si la darenière exécution est inférieure à la cofiguration, on ne fait rien.
[ ${DIFF_MINUTES} -lt ${DURATION} ] && exit 0

#------------------------------------------------------------------
# Création de la liste des wallpapers

# On crée d'index en alimentant les images par ordre alphabétique (dans ce fichier, la liste des images est indiquée avec le répertoire)
find ${IMG_DIR}/ -type f -print | sort  > "${DATA_FILE}"
[ $(cat "${DATA_FILE}" | wc -l) -eq 0 ] && print_error "No wallpaper found in ${IMG_DIR}." && exit 1

# On Calcule un hashage pour voir si l'index a changé)
CURRENT_HASH=$(md5sum "${DATA_FILE}")
[ ! -f "${HASH_FILE}" ] && echo "HASH=${CURRENT_HASH}" > "${HASH_FILE}"

PREVIOUS_HASH="$(cat "${HASH_FILE}" | grep "^HASH=" | cut -d'=' -f2-)"

# Si le HASH a changé, c'est que la liste des images a changé, il faut repartir à zéro (on efface le .run)
[ "${CURRENT_HASH:-none}" != "${PREVIOUS_HASH:-enon}" ] && rm -f "${RUN_FILE}"

#------------------------------------------------------------------
# Génération du fichier d'exécution

# Si le fichier .run n'existe pas, on le crée
[ ! -f "${RUN_FILE}" ] && cp "${DATA_FILE}" "${RUN_FILE}"

# Nombre de wallpapers à traiter
NB_WALLPAPERS=$(cat "${RUN_FILE}" | wc -l)
#echo ${NB_WALLPAPERS}

# Si le nombre est nul alors erreur
[ ${NB_WALLPAPERS} -eq 0 ] && rm -f "${RUN_FILE}" && print_error "No wallpaper found in the current run!" && exit 1

#------------------------------------------------------------------
# Cas shuffle
if [ "${MODE:-alpha}" = "shuffle" ] 
then
# Retourne un nombre aléatoire entre 0 et 1 , sans jamais atteindre 1
#RND_IDX=$(awk -v NUM=${NB_WALLPAPERS} 'BEGIN {srand(); printf "%.f\n",(rand() * NUM) + 1}')
# Au cas où : borner les possibililés
#[ ${RND_IDX:-0} -gt ${NB_WALLPAPERS:-0} ] && RND_IDX=${NB_WALLPAPERS}
#[ ${RND_IDX:-0} -le 0 ] && RND_IDX=1
#CURRENT_IMG="$(cat "${RUN_FILE}" | head -${RND_IDX} | tail -1)"
CURRENT_IMG="$(cat "${RUN_FILE}" | shuf -n1)"
fi

#------------------------------------------------------------------
# Cas alphabétique : on prend le premier de la liste du .run (car le fichier est déjà trié dans cet ordre)
if [ "${MODE:-alpha}" = "alpha" ] 
then
CURRENT_IMG="$(cat "${RUN_FILE}" | head -1)"
fi

#------------------------------------------------------------------
# Définition du wallpaper
CURRENT_WALLPAPER="${CURRENT_IMG}"
[ ! -f "${CURRENT_WALLPAPER}" ] && print_error "${CURRENT_WALLPAPER} not found!" && exit 1
[ -f "${CURRENT_WALLPAPER}" ] && {

# 3  methods to launch gsettings :

# 1- first method
# Get the Real Username
#RUID=$(who | awk 'FNR == 1 {print $1}')
# Translate Real Username to Real User ID
#RUSER_UID=$(id -u ${RUID})
# Launch with sudo
#sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" gsettings set org.gnome.desktop.background picture-uri "file://${CURRENT_WALLPAPER}"

# 2- second method
# Get the Real Username
RUID=$(who | awk 'FNR == 1 {print $1}')
# Translate Real Username to Real User ID
RUSER_UID=$(id -u ${RUID})
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus"
gsettings set org.gnome.desktop.background picture-uri "file://${CURRENT_WALLPAPER}"

# 3- third method (not stable)
#dbus-launch gsettings set org.gnome.desktop.background picture-uri "file://${CURRENT_WALLPAPER}"

# Write the last execution
echo "LAST_EXEC=$(date -u '+%s')" > ${LAST_EXEC_FILE}
}

#echo "file://${CURRENT_WALLPAPER}"

#------------------------------------------------------------------
# Rafraîchissement du .run

# On retire le fichier de la liste
cat "${RUN_FILE}" | grep -v "${CURRENT_IMG}" > "${RUN_FILE}.tmp"
# Remplacement de l'ancien par le nouveau
mv "${RUN_FILE}.tmp" "${RUN_FILE}"

[ ${NB_WALLPAPERS} -eq 0 ] && rm -f "${RUN_FILE}"
#------------------------------------------------------------------
exit 0