#!/bin/sh
# Interface graphique pour la configuration
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Diaporama : Configure Diaporomo
#@(#)--------------------------------------------------------------
#@(#) Define the duration and the mode of the diaporama
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_number () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}


#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
DATA_DIR="${HOME}/.diaporama"
ETC_FILE="${DATA_DIR}/diaporama.conf"
[ -f "${ETC_FILE}" ] && . "${ETC_FILE}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Définition des cases à cocher
[ ${MODE:-0} -eq 0 ] && MODE_ALPHA_CHECK="TRUE"
[ ${MODE:-0} -eq 1 ] && MODE_SHUFFLE_CHECK="TRUE"

#------------------------------------------------------------------
# Formulaires

MODE_VALUE=$(zenity --width 350 --list --title="Mode" --text "Select the mode:" --radiolist --separator="|" --print-column=2 --column="#" --column="Mode" ${MODE_ALPHA_CHECK:-FALSE} "Alphabetical (default)" ${MODE_SHUFFLE_CHECK:-FALSE} "Shuffle")

[ "${MODE_VALUE:-none}" = "none" ] && MODE_VALUE="Alphabetical"

DURATION_VALUE=$(zenity --width 350 --entry --title="Diaporama - Configuration" --text="Duration :" --entry-text "${DURATION:-15}")

[ "${DURATION_VALUE:-none}" = "none" ] && DURATION_VALUE=${DURATION:-15}

[ "${MODE_VALUE:-none}" = "Alphabetical" ] && MODE=0
[ "${MODE_VALUE:-none}" = "Shuffle" ] && MODE=1

#echo "MODE_VALUE : ${MODE_VALUE}"
#echo "DURATION_VALUE : ${DURATION_VALUE}"

zenity --width 350 --question --text="Configuration to apply \n MODE : ${MODE_VALUE} \n DURATION : ${DURATION_VALUE} minutes"
APPLY=${?}
echo "APPLY : ${APPLY}"
[ ${APPLY:-1} -ne 0 ] && zenity --width 350 --info --text="Configuration canceled!" && exit 0
[ ${APPLY:-1} -eq 0 ] && ./config.sh -m ${MODE} -d ${DURATION_VALUE}

#------------------------------------------------------------------
exit 0