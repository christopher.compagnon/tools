# Diaporama

A simple tool to display your favorite wallpapers as background on the Gnome Desktop.

## Installation

Download and uncompress the sources.

## The files

### core.diaporama.sh

Core of the diaporama, launched through crontab.

### config.sh

Command line tool for configuration and managing the wallpaper. Find help with :

    ./config.sh -h

### gui.config.sh

Graphical tool for configuration. Uses Zenity.

### gui.add.sh

Graphical tool for adding files. Uses Zenity.

## Configuration

### Command line

Execute *config.sh* as :

    ./config.sh -f /path/to/my/image.jpg

This command install the selected image as background. By default, the diaporama is set to change every 15 minutes.
To change the rotation properties, execute :

    ./config.sh -d 5

This command sets the duration (d = duration) to 5 minutes.

    ./config.sh -m 1

This command shuffles (1 = shuffle) the order of the wallpapers.

    ./config.sh -m 0

This command selects the wallpapers according to an alphabetical order (0 = alphabetical).


### Graphical interface

Execute the *gui.config.sh* to set mode and duration.
Execute *gui.add.sh* to add a wallpaper to the diaporama.
You can also configure the *config.sh* with an action on the current file with -f option, set as :

    ./config.sh -f %f

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")