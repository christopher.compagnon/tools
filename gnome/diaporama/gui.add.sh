#!/bin/sh
# Interface graphique pour l'ajout d'un fichier dans le diaporama
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Diaporama : add a wallpaper
#@(#)--------------------------------------------------------------
#@(#) Add a wallpaper to Diaporama using Zenity (GUI)
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
FILE_NAME=$(zenity --width 350 --file-selection --title="Select a File"  --filename="${HOME}/")
RETURN=${?}
[ ${RETURN:-1} -eq 0 ] && ./config.sh -f ${FILE_NAME}
#------------------------------------------------------------------
exit 0