#!/bin/sh
# Gestion d'un diaporama
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) setDiaporama : configure diaporama.sh
#@(#)--------------------------------------------------------------
#@(#) configure the Diaporama
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

is_number () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}

usage () {
echo "Usage: ${SCRIPT_NAME} [-f filename] [-d integer ] [-m integer] [-h]"
echo "-f (filename) : file + directory of the wallpaper."
echo "-m (mode) : 0 = alphabetical (default value), 1 = shuffle"
echo "-d (duration) : change the wallpaper every n minutes (default = 15)"
echo "-h (help) : display help."
}

WriteEtc () {
echo "# $(date -u '+%c')" > ${ETC_FILE}
echo "MODE=${MODE:-0}" >> ${ETC_FILE}
echo "DURATION=${DURATION:-15}" >> ${ETC_FILE}
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
DATA_DIR="${HOME}/.diaporama"
IMG_DIR="${DATA_DIR}/wallpapers"
CRONFILE="${DATA_DIR}/crontab.tmp"
ETC_FILE="${DATA_DIR}/diaporama.conf"
LOG_FILE="${DATA_DIR}/diaporama.log"

# Création du répertoire
[ ! -d ${DATA_DIR} ] && {
mkdir -p ${DATA_DIR}
mkdir -p ${IMG_DIR}
WriteEtc
}

[ ! -f "${ETC_FILE}" ] && WriteEtc
. "${ETC_FILE}"
#------------------------------------------------------------------
# Paramètres
#------------------------------------------------------------------
# Récupération des options
while getopts ":f:m:d:h" OPTION
do

case "${OPTION}" in

"f")
# File to copie
IMG_FILE="${OPTARG}"
NEW_FILE="true"
;;

"m")
# mode
MODE_VALUE=${OPTARG}
IS_NUMBER=$(is_number ${MODE_VALUE})
[ ${IS_NUMBER:-1} -ne 0 ]  && print_error "Sorry, ${MODE_VALUE} is not an integer." && exit 1
CHANGE_MODE="true"
;;

"d")
# duration
DURATION=${OPTARG}
# DURATION est un nombre
IS_NUMBER=$(is_number ${DURATION})
[ ${IS_NUMBER:-1} -ne 0 ]  && print_error "Sorry, ${DURATION} is not an integer." && exit 1
CHANGE_DURATION="true"
;;

"h")
usage
exit 0
;;


?)
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Contrôles
 [ "${NEW_FILE:-false}" = "true" -a ! -f "${IMG_FILE}" ] && print_error "File ${IMG_FILE} not found!" && exit 1
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Copie du fichier dans le répertoire du diaporama

[ "${NEW_FILE:-false}" = "true" -a -f "${IMG_FILE}" ] && cp "${IMG_FILE}" "${IMG_DIR}/"

#------------------------------------------------------------------
# Application de la configuration

[ "${CHANGE_DURATION:-false}" = "true" -o "${CHANGE_MODE:-false}" = "true" ] && {
# Enregistrement du paramétrage
WriteEtc

# Mise à jour du crontab
# Note : crontab -l : liste le crontab de l'utilisateur courant
crontab -l | grep -v "$(pwd)/core.diaporama.sh" > "${CRONFILE}"
echo "* * * * * $(pwd)/core.diaporama.sh -m ${MODE_VALUE:-0} 2> ${LOG_FILE}" >> "${CRONFILE}"
cat "${CRONFILE}" | crontab -

}
#------------------------------------------------------------------
exit 0