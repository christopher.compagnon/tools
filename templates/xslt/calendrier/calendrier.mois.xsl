<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml"
version="1.0">
<!-- ========================== Imports =========================== -->
<xsl:import href="calendrier.perpetuel.xsl"/>

<xsl:variable name="date"><xsl:value-of select="/calendar/date"/></xsl:variable>
<xsl:variable name="year" select="substring($date,1,4)"/>
<xsl:variable name="month" select="substring($date,5,2)"/>

<xsl:variable name="DOW">
<xsl:call-template name="DayOfWeek">
<xsl:with-param name="date" select="$date"/>
</xsl:call-template>
</xsl:variable>

<xsl:variable name="DureeMois">
<xsl:call-template name="duree-mois">
<xsl:with-param name="month" select="$month"/>
<xsl:with-param name="year" select="$year"/>
</xsl:call-template>
</xsl:variable>

<xsl:variable name="NomMois">
<xsl:call-template name="month.name">
<xsl:with-param name="month" select="$month"/>
</xsl:call-template>
</xsl:variable>

<xsl:template match="/">
<html>
<head>
<title>Calendrier</title>
</head>
<body>
	
<table rules="groups" border="1" cellpadding="3">

<thead>
	
<tr>
<th><a href="">&lt;</a></th>
<th colspan="5"><xsl:value-of select="$NomMois"/><xsl:text> </xsl:text><xsl:value-of select="$year"/>

</th>
<th><a href="">&gt;</a></th>
</tr>
<!-- partie mobile -->
<tr>
	
<xsl:choose>
<xsl:when test="$DOW = 7">
<th>Di</th>	
<th>Lu</th>
<th>Ma</th>
<th>Me</th>
<th>Je</th>
<th>Ve</th>
<th>Sa</th>
</xsl:when>

<xsl:when test="$DOW = 1">
<th>Lu</th>
<th>Ma</th>
<th>Me</th>
<th>Je</th>
<th>Ve</th>
<th>Sa</th>
<th>Di</th>
</xsl:when>

<xsl:when test="$DOW = 2">
<th>Ma</th>
<th>Me</th>
<th>Je</th>
<th>Ve</th>
<th>Sa</th>
<th>Di</th>
<th>Lu</th>
</xsl:when>

<xsl:when test="$DOW = 3">
<th>Me</th>
<th>Je</th>
<th>Ve</th>
<th>Sa</th>
<th>Di</th>
<th>Lu</th>
<th>Ma</th>
</xsl:when>

<xsl:when test="$DOW = 4">
<th>Je</th>
<th>Ve</th>
<th>Sa</th>
<th>Di</th>
<th>Lu</th>
<th>Ma</th>
<th>Me</th>
</xsl:when>

<xsl:when test="$DOW = 5">
<th>Ve</th>
<th>Sa</th>
<th>Di</th>
<th>Lu</th>
<th>Ma</th>
<th>Me</th>
<th>Je</th>
</xsl:when>

<xsl:when test="$DOW = 6">
<th>Sa</th>
<th>Di</th>
<th>Lu</th>
<th>Ma</th>
<th>Me</th>
<th>Je</th>
<th>Ve</th>
</xsl:when>
</xsl:choose> 

</tr>
</thead>

<tbody>
<tr>
<td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
</tr>

<tr>
<td>8</td>
<td>9</td>
<td>10</td>
<td>11</td>
<td>12</td>
<td>13</td>
<td>14</td>
</tr>

<tr>
<td>15</td>
<td>16</td>
<td>17</td>
<td>18</td>
<td>19</td>
<td>20</td>
<td>21</td>
</tr>

<tr>
<td>22</td>
<td>23</td>
<td>24</td>
<td>25</td>
<td>26</td>
<td>27</td>
<td>28</td>
</tr>

<tr>
<!-- partie mobile en fonction du mois et de l'année -->
<td>
<xsl:if test="$DureeMois &gt;= 29">29</xsl:if>
</td>

<td><xsl:if test="$DureeMois &gt;= 30">30</xsl:if></td>
<td><xsl:if test="$DureeMois &gt;= 31">31</xsl:if></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

</tbody>


</table>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
