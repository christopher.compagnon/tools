#!/bin/sh
#set -x

ROOT="$(dirname ${0})"
cd "${ROOT}"
echo "${ROOT}"
XSLT_FILE="calendrier.mois.xsl"
DATA_FILE="today.xml"

./today.calendar.sh > ${DATA_FILE}

xsltproc ${XSLT_FILE} ${DATA_FILE} > test.html

exit 0