<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml"
version="1.0">

<xsl:variable name="vision" select="/calendrier/params/vision"/>
<xsl:variable name="start.date" select="/calendrier/params/datedeb"/>
<xsl:variable name="end.date" select="/calendrier/params/datefin"/>
<xsl:variable name="start.day" select="substring($start.date,7,2)"/>
<xsl:variable name="start.month" select="substring($start.date,5,2)"/>
<xsl:variable name="start.year" select="substring($start.date,1,4)"/>
<xsl:variable name="end.day" select="substring($end.date,7,2)"/>
<xsl:variable name="end.month" select="substring($end.date,5,2)"/>
<xsl:variable name="end.year" select="substring($end.date,1,4)"/>

<xsl:template name="month.name">
<xsl:param name="month"/>
<xsl:choose>
<xsl:when test="$month = 1">Janvier</xsl:when>
<xsl:when test="$month = 2">Février</xsl:when>
<xsl:when test="$month = 3">Mars</xsl:when>
<xsl:when test="$month = 4">Avril</xsl:when>
<xsl:when test="$month = 5">Mai</xsl:when>
<xsl:when test="$month = 6">Juin</xsl:when>
<xsl:when test="$month = 7">Juillet</xsl:when>
<xsl:when test="$month = 8">Août</xsl:when>
<xsl:when test="$month = 9">Septembre</xsl:when>
<xsl:when test="$month = 10">Octobre</xsl:when>
<xsl:when test="$month = 11">Novembre</xsl:when>
<xsl:when test="$month = 12">Décembre</xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="is-leap-year">
<xsl:param name="year"/>
<xsl:choose>
<xsl:when test="($year mod 4 = 0 and  $year mod 100 != 0) or ($year mod 400 = 0)">true</xsl:when>
<xsl:otherwise>false</xsl:otherwise>
</xsl:choose>   
</xsl:template>

<xsl:template name="duree-mois">
<xsl:param name="month"/>
<xsl:param name="year"/>
<xsl:variable name="year.is-leap-year">
<xsl:call-template name="is-leap-year">
<xsl:with-param name="year" select="$year"/>
</xsl:call-template>
</xsl:variable>

<xsl:choose>
<xsl:when test="$month = 2"><xsl:choose>
<xsl:when test="$year.is-leap-year='true'">29</xsl:when>
<xsl:otherwise>28</xsl:otherwise>
</xsl:choose></xsl:when>
<xsl:when test="$month = 4 or $month = 6 or $month = 9 or $month = 11">30</xsl:when>
<xsl:otherwise>31</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="DayOfWeek">
<xsl:param name="date"/><!-- date au format yyyymmdd pour éviter les suppression des 0 à gauche -->

<!-- calendrier perpétuel
selon l'algorithme de Claus Tondering :
Claus Tondering propose l'algorithme ci-dessous, où les divisions sont entières et leurs restes ignorés,
sauf ceux relatifs à la division par 7 (ou modulo 7).

a = (14 - M) ÷ 12
avec M = rang du mois, avec M = 1 à 12: jan=1, fév=2, mars=3, avril=4, mai=5, juin=6, juil=7, août=8, sep=9, oct=10, nov=11, et déc=12
y = Y - a où Y = année entière à 4 chiffres
m = M + 12a - 2
j = Jour où j = 1 à 31; max de 31 jours dans un mois

Pour le calendrier Julien (dates avant le 15 octobre 1582) :
d = [5 + j + y + y/4 + (31m)/12] mod 7

Pour le calendrier Grégorien (dates depuis le 15 octobre 1582, inclusivement) :
d = [j + y + y/4 - y/100 + y/400 + (31m)/12] mod 7
-->
<xsl:variable name="yyyy" select="substring($date,1,4)"/>
<xsl:variable name="yy" select="substring($date,3,2)"/>
<xsl:variable name="mm" select="substring($date,5,2)"/>
<xsl:variable name="dd" select="substring($date,7,2)"/>

<xsl:variable name="a" select="floor(( 14 - $mm ) div 12)"/>
<xsl:variable name="y" select="$yyyy - $a"/>
<xsl:variable name="m" select="$mm + (12 * $a) - 2"/>

<xsl:variable name="d">
<xsl:choose>
<xsl:when test="$date &lt; 15821015"><xsl:value-of select="(5 + $dd + $y + floor($y div 4) + floor((31 * $m) div 12)) mod 7"/></xsl:when> <!-- cas du calendrier julien -->
<xsl:otherwise><xsl:value-of select="($dd + $y + floor($y div 4) - floor($y div 100) + floor($y div 400) + floor((31 * $m) div 12)) mod 7"/></xsl:otherwise> <!-- calendrier grégorien -->
</xsl:choose>
</xsl:variable>

<!-- version officielle 
<xsl:choose>
<xsl:when test="$d = 0">dimanche</xsl:when>
<xsl:when test="$d = 1">lundi</xsl:when>
<xsl:when test="$d = 2">mardi</xsl:when>
<xsl:when test="$d = 3">mercredi</xsl:when>
<xsl:when test="$d = 4">jeudi</xsl:when>
<xsl:when test="$d = 5">vendredi</xsl:when>
<xsl:when test="$d = 6">samedi</xsl:when>
</xsl:choose> -->

<!-- version modifiée pour affichage courant -->
<xsl:choose>
<xsl:when test="$d = 0">7</xsl:when> <!-- le dimanche devient le dernier jour de la semaine -->
<xsl:when test="$d = 1">1</xsl:when>
<xsl:when test="$d = 2">2</xsl:when>
<xsl:when test="$d = 3">3</xsl:when>
<xsl:when test="$d = 4">4</xsl:when>
<xsl:when test="$d = 5">5</xsl:when>
<xsl:when test="$d = 6">6</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>
