# newfile

Create a file from template.



## Examples

    ./newfile.sh -d /put/the/file/here/newfile.sh -t sh

Creates a .sh file from the template.

## Add/modify templates

Simply add or modify the template in *templates* directory.

## Desktop integration

Can easily be integrated in the desktop using FileManager-Actions.

![desktop integration](newfile-integration.png "desktop integration")

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")