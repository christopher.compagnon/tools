#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) newfile : Create a new file from template
#@(#)--------------------------------------------------------------
#@(#) Create a new file from template.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} [ -l ] [ -d directory ] [ -t ] [ -h ]"
echo "-t (type) : types of file"
list
echo "-d (directory): directory where the file will be created"
echo "-l (list) : list of all available types."
echo "-h (help) : display help."
}

list () {
echo "Types of files available :"
ls -1 ${TEMPL_DIR}/ | xargs -n1 basename | awk -F"." '{print "- " $2}'
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
PROG_PATH=$(dirname ${0})
TEMPL_DIR="${PROG_PATH}/templates"
NAME_FILE="NewFile"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Gestion des options
while getopts ":d:t:lh" option 
do 
case ${option} in

d)
DEST_DIR=${OPTARG}
HAS_DESTINATION="true"
;;  

t)
TYPE_FILE=${OPTARG}
[ ! -f ${TEMPL_DIR}/template.${TYPE_FILE} ] && {
print_error "Incorrect type ${TYPE_FILE} !"
print_error "Please select one of these:" 
list 1>&2
exit 1
}
HAS_TYPE="true"
;; 

l)
list
exit 0
;; 

\?) 
usage
exit 0
;;

esac
done 
#------------------------------------------------------------------
# Controls
[ "${HAS_DESTINATION:-false}" != "true" ] && print_error "Option -d is missing !" && exit 1
[ "${HAS_TYPE:-false}" != "true" ] && print_error "Option -t is missing !" && exit 1
[ ! -d "${DEST_DIR}" ] && print_error "Directory ${DEST_DIR} not found !" && exit 1 
#------------------------------------------------------------------
# Copy template to target directory
cp ${TEMPL_DIR}/template.${TYPE_FILE} "${DEST_DIR}/${NAME_FILE}.${TYPE_FILE}"
#------------------------------------------------------------------
exit 0
