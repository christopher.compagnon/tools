<?xml version="1.0" encoding="utf-8"?>
<!-- 
 ========================== Infos ===========================
 Description : 
 Auteur : C. Compagnon
 Courriel : admin@firenode.net
 Site : http://www.firenode.net
 ============================================================
 
 Version			date				Description
 ............................................................
 1.0				YYYY-MM-DD		Version initiale
 ............................................................
 
  Suppléments :

 ============================================================
 -->
<xsl:stylesheet 
version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 

<!-- ========================== Imports =========================== -->
<xsl:import href="import.xsl"/>

<!-- ========================== Output =========================== -->
<xsl:output 
method="xml" 
indent="yes"
encoding="utf-8"/>

<!-- ========================== Paramètres =========================== -->
<xsl:param name="param"></xsl:param>
<!-- ========================== Variables =========================== -->
<xsl:variable name="variable" select="path"/>
<!-- ========================== Règles =========================== -->

<!-- ========================== Templates =========================== -->
<xsl:template match="/">
	
</xsl:template>	
<!-- ========================== actions par défaut =========================== -->
<xsl:template match="text()"/>
</xsl:stylesheet>
