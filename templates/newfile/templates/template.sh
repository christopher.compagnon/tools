#!/bin/sh
# set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Script's name : short description
#@(#)--------------------------------------------------------------
#@(#) Description
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -o option ] [ -h ]"
echo "-o (option 1) : a simple option."
echo "-h (help) : display help."
}


functionName () {
# Description
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Management of parameters
while getopts ":o:h" PARAM
do
case "${PARAM}" in

"o")
# description
A=${OPTARG}
HAS_OPTION_A="true"
;;

"h")
# description
usage
exit 0
;;

?)
# else
echo "Invalid option ${PARAM} !"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Controls of options
[ "${HAS_OPTION_A:-false}" != "true" ] && print_error "Option -o is missing !" && exit 1

#------------------------------------------------------------------
# Part 1 : Description


#------------------------------------------------------------------
# Part 2 : Description


#------------------------------------------------------------------
exit 0
