# Tools

My tools and templates for productivity on Linux.

## Audio

### srt2mp3

Conversion of *.srt* file to *.mp3* using **Google tts**.

### tts

Converts sentences to sounds from command line using **Google tts**.

Webservice also available.

### speech

Reads a speech formatted file using *tts.sh* (avaible in the same project).

## Image

## Image Compressor

A simple tool to compress images using *Zenity* and *image magick*.

## Image Converter

A simple tool to convert images into another formats, using *Zenity* and *image magick*.

## Gnome

### Diaporama

A diaporama for wallpapers. Display your favorite wallpapers as background of your Gnome desktop.

### Gnome Instyle

A graphical tool to install and activate theme/icons/cursors on Gnome desktop.

## Conversions

### text2url.sed

A sed file for converting text to URL compatible string.

### html2utf8

A script for converting HTML encoding to XML/UTF8 entities.

## Templates

### newfile

Creates a file from template.

## Clocks & Timers

Some clocks/timers.

### term_clock

A digital clock coded in unix shell displayed in the terminal.

### term_timer

A digital timer coded in unix shell displayed in the terminal. Can execute commands at the end.

## PDF

### PDF extract pages

Script to extract pages from PDF file.

### PDF extract images

Script to extract pages as image from PDF file.

## System

### Firefox session backup

Script to save the current session of Firefox.

### Swap alert

Script to alert when RAM or Swap reached a threshold.

## Security

### Stegano

2 scripts (**Stegano Encrypter** and **Stegano Decrypter**) to hide and unhide data into another file.

## Video

### hypnotix-install

Script to install **Hypnotix** on Linux/Debian.

### popcorn-time-install

Script to install **Popcorn Time** on Linux/Debian.

### molotov-install

Script to install **Molotov** on Linux/Debian.

## Web

### Email munger

Convert an email address to something disguised for spammers but still readable for users.

### ffx-profiles

Manage the profiles of Firefox from a simple graphical tool.

## ffx-webapp-creator

Create WebApps from Websites with Firefox.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

