#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) ffx-profiles : Create profiles for Firefox on Linux systems
#@(#)--------------------------------------------------------------
#@(#) Create profiles for Firefox on Linux systems
#@(#) Dependencies : Zenity
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.4
#@(#)--------------------------------------------------------------
#@(#) History :
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - manages icon for the launcher
#@(#) Version : 1.2 - tests if Zeniy is installed
#@(#) Version : 1.3 - defines the launcher icon properly
#@(#) Version : 1.4 - improves display of launchers in multi-language env
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)

# Default value
LAUNCHER_ICON="firefox"
ICONS_DIR="${HOME}/.local/share/icons"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------

PROFILE_REQUEST=$(zenity --entry --title="${TITLE_PROFILE_REQUEST}" --text="${LABEL_PROFILE_REQUEST}" --entry-text="${PLACEHOLDER_PROFILE_REQUEST}")
RETURN_PROFILE=${?}

# Cas de l'annulation de création
[ ${RETURN_PROFILE:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_PROFILE:-1} -ne 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

[ "${PROFILE_REQUEST}" = "" ] && {
zenity --error --width=500 --text="${ERROR_NO_PROFILE_DEFINED}"
exit 1
}

# Normalisation du nom de profil sans espaces
PROFILE_NAME="$(echo "${PROFILE_REQUEST}" | sed 's/ /-/g' | sed 's/=/-/g')"

[ "${PROFILE_NAME}" = "" ] && {
zenity --error --width=500 --text="${ERROR_PROFILE_INCORRECT}"
exit 1
}


# Définition du répertoire du lanceur
FFX_LAUNCHER="${HOME}/.local/share/applications/firefox-${PROFILE_NAME}.desktop"
#------------------------------------------------------------------
# Contrôles

# Vérification du profil existant
[ -f "${FFX_LAUNCHER}" ] && {
zenity --error --width=500 --text="${ERROR_CONFIG_ALREADY_EXISTS}"
# Lancement du gestionnaire de profils Firefox
firefox --profilemanager

exit 1
}

#------------------------------------------------------------------
# Choix d'une icône spécifique pour le lanceur
zenity --question --width=500 --text="${LABEL_ICON_REQUEST}"
RETURN_ICON=${?}

[ ${RETURN_ICON:-1} -lt 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}" 
exit 1
}

if [ ${RETURN_ICON:-1} -eq 0 ]
then

# Choix de l'icône
LAUNCHER_ICON=$(zenity --file-selection --title="${TITLE_INPUT_ICON}" --file-filter="*.svg *.png")
OK_ICON=${?}

[ ${OK_ICON:-1} -lt 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

[ ${OK_ICON:-1} -gt 0 ] && {
zenity --error --width=500 --text="${ERROR_NO_FILE_SELECTED}"
exit 1
}

# Define the new icon
mkdir -p "${ICONS_DIR}"
cp "${LAUNCHER_ICON}" "${ICONS_DIR}"
ICON_NAME="$( basename ${LAUNCHER_ICON} )"
LAUNCHER_ICON="${ICONS_DIR}/${ICON_NAME}"

fi

#------------------------------------------------------------------
# Création du profil dans Firefox
firefox -CreateProfile "${PROFILE_NAME}"
zenity --info --width=500 --text="${SUCCESS_PROFILE_CREATION}"

#------------------------------------------------------------------
# Création du lanceur
zenity --question --width=500 --text="${LABEL_LAUNCHER_REQUEST}"
RETURN_LAUNCHER=${?}

# Cas de l'annulation de création
[ ${RETURN_LAUNCHER:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_LAUNCHER:-1} -ne 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

# Création du lanceur
echo "[Desktop Entry]" > ${FFX_LAUNCHER}
echo "Name=${PROFILE_REQUEST}" >> ${FFX_LAUNCHER}
echo "GenericName=Firefox : ${PROFILE_NAME} " >> ${FFX_LAUNCHER}
echo "Comment=${PROFILE_REQUEST} ${COMMENT_PROFILE}" >> ${FFX_LAUNCHER}
echo "Exec=firefox -P ${PROFILE_NAME} -no-remote --class ${PROFILE_NAME}Profile" >> ${FFX_LAUNCHER}
echo "Icon=${LAUNCHER_ICON}" >> ${FFX_LAUNCHER}
echo "Type=Application" >> ${FFX_LAUNCHER}
echo "Terminal=false" >> ${FFX_LAUNCHER}
echo "Categories=GTK;Network;WebBrowser;" >> ${FFX_LAUNCHER}
echo "StartupNotify=true" >> ${FFX_LAUNCHER}
echo "StartupWMClass=${PROFILE_NAME}Profile" >> ${FFX_LAUNCHER}
echo "Actions=profile-manager;new-window;" >> ${FFX_LAUNCHER}
echo "X-FFXProfile-Name=${PROFILE_REQUEST}" >> ${FFX_LAUNCHER}

echo "" >> ${FFX_LAUNCHER}
echo "[Desktop Action profile-manager]" >> ${FFX_LAUNCHER}
echo "Name=${NAME_PROFILE}" >> ${FFX_LAUNCHER}
echo "Exec=firefox --profilemanager" >> ${FFX_LAUNCHER}

echo "" >> ${FFX_LAUNCHER}
echo "[Desktop Action new-window]" >> ${FFX_LAUNCHER}
echo "Name=${NAME_NEW_WINDOW}" >> ${FFX_LAUNCHER}
echo "Exec=firefox -new-window %u" >> ${FFX_LAUNCHER}

# contrôle
[ ! -f "${FFX_LAUNCHER}" ] && {
zenity --error --width=500 --text="${ERROR_CREATION}"
exit 1
}

# - 7 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${FFX_LAUNCHER}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && zenity --warning --width=500 --text="${WARNING_LAUNCHER_ISSUES}"

# Info succès !
zenity --info --width=500 --text="${SUCCESS_LAUNCHER_CREATION}"
#firefox -P "${PROFILE_NAME}" -no-remote --class ${PROFILE_NAME}ProfileIcon=firefox

#------------------------------------------------------------------
exit 0
