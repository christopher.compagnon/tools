# Web Tools

## ffx-profiles

Graphical tool (zenity) to create profiles for Firefox.

Usefull to keep the activities separated on Firefox.

### Installation

1- Download the application (script + languages + icons).

2- Set the execution rights to the scripts **ffx-profiles.sh** + **install.sh** if not set.

You can add a launcher manually through your menu editor application or use **install.sh** to do it automatically.

Launch **install.sh**, enter the name and the comment, confirm (OK) and that's it. You should have a new launcher for **ffx-profiles.sh**.

You can also launch **ffx-profiles.sh** manually.

### Usage

Launch **ffx-profiles.sh** manually or through the launcher (see Installation).

The tool creates a new profile for Firefox Web Browser.

You can check the existing profiles executing the following command :

    firefox -- profilemanager

**Note** : the profile manager is accessible from the application's launcher or the profile's launcher (right-click on the icon's launcher > *Profile manager*).

After the creation, it asks if you want to generate a launcher on your desktop for the profile. This launcher will be added to the other launchers.

**Note** : From this launcher, you can also launch the *Firefox Profile Manager* (right-click on the icon's launcher > *Profile manager*).

You can also manage the profiles from *Firefox Web Browser > about:profiles*.

### Multi-language management

The *xx.lang* file provides a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* (with xx = code of lang) to support your language.

To find the code of the lang used on your system, please run :

    echo ${LANG} | cut -c1-2

in your terminal.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

