#!/bin/sh
# set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Install a launcher for ffx-profiles
#@(#)--------------------------------------------------------------
#@(#) Graphical (Zenity) installer for the ffx-profiles' launcher
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
APP_LAUNCHER="${HOME}/.local/share/applications/firefox-profiles-launcher.desktop"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Informations à propos du lanceur
LAUNCHER_INFOS=$(zenity --forms --width=500 --title="${TITLE_LAUNCHER_INFOS}" \
	--text="${LABEL_LAUNCHER_INFOS}" \
	--separator="|" \
	--add-entry="${LABEL_LAUNCHER_NAME}" \
	--add-entry="${LABEL_LAUNCHER_COMMENT}")
RETURN_LAUNCHER=${?}

# Cas de l'annulation de création
[ ${RETURN_LAUNCHER:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_LAUNCHER:-1} -ne 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

#------------------------------------------------------------------
# Création du lanceur
LAUNCHER_LABEL="$(echo "${LAUNCHER_INFOS}" | cut -d '|' -f1)"
LAUNCHER_COMMENT="$(echo "${LAUNCHER_INFOS}" | cut -d '|' -f2)"

echo "[Desktop Entry]" > ${APP_LAUNCHER}
echo "Encoding=UTF-8" >> ${APP_LAUNCHER}
echo "Name=${LAUNCHER_LABEL}" >> ${APP_LAUNCHER}
echo "GenericName=Launcher" >> ${APP_LAUNCHER}
echo "Comment=${LAUNCHER_COMMENT}" >> ${APP_LAUNCHER}
echo "Exec=$(pwd)/ffx-profiles.sh" >> ${APP_LAUNCHER}
echo "Icon=$(pwd)/ffx-profiles.svg" >> ${APP_LAUNCHER}
echo "Type=Application" >> ${APP_LAUNCHER}
echo "Terminal=false" >> ${APP_LAUNCHER}
echo "Categories=GTK;Network;WebBrowser;" >> ${APP_LAUNCHER}
echo "StartupNotify=True" >> ${APP_LAUNCHER}
echo "Actions=profile-manager;" >> ${APP_LAUNCHER}
echo "" >> ${APP_LAUNCHER}
echo "[Desktop Action profile-manager]" >> ${APP_LAUNCHER}
echo "Name=Profile Manager" >> ${APP_LAUNCHER}
echo "Exec=firefox --profilemanager" >> ${APP_LAUNCHER}

# contrôle
[ ! -f "${APP_LAUNCHER}" ] && {
zenity --error --width=500 --text="${ERROR_CREATION}"
exit 1
}

HAS_ISSUES=$( desktop-file-validate "${APP_LAUNCHER}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && zenity --warning --width=500 --text="${WARNING_LAUNCHER_ISSUES}"

# Info succès !
zenity --info --width=500 --text="${SUCCESS_LAUNCHER_CREATION}"
#------------------------------------------------------------------
exit 0
