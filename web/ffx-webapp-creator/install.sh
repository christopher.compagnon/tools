#!/bin/sh
#set -x
cd "$(dirname ${0})"
SCRIPT_DIR="$(pwd)"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) install : Install ffx-webapp-creator
#@(#)--------------------------------------------------------------
#@(#) Install the shortcut for ffx-webapp-creator on system
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0
#------------------------------------------------------------------
#@(#) History : 
#@(#) Version : 1.0 - initial version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)

if [ ${P_UID:--1} -ne 0 ]
then
DESKTOP_ENTRY="${HOME}/.local/share/applications/ffx-webapps.desktop"
ICONS_DIR="${HOME}/.local/share/icons"
mkdir -p "${ICONS_DIR}"
fi

if [ ${P_UID:--1} -eq 0 ]
then
DESKTOP_ENTRY="/usr/share/applications/ffx-webapps.desktop"
ICONS_DIR="/usr/share/pixmaps"
fi

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# - 1 : Copy the icon
[ ! -f "${SCRIPT_DIR}/Oxygen480-apps-internet-web-browser.svg" ] && print_error "The application's icon does not exist!"  && sleep 5 && exit 1
cp "${SCRIPT_DIR}/Oxygen480-apps-internet-web-browser.svg" "${ICONS_DIR}/"

# - 2 : Create .desktop file (so the launcher)
echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Version=1.0' >> "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=FFX WebApp Creator' >> "${DESKTOP_ENTRY}"
echo 'Comment=Create a Firefox WebApp from a website' >> "${DESKTOP_ENTRY}"
echo "Exec=${SCRIPT_DIR}/ffx-webapp-creator.sh" >> "${DESKTOP_ENTRY}"
echo "Icon=${ICONS_DIR}/Oxygen480-apps-internet-web-browser.svg" >> "${DESKTOP_ENTRY}"
echo 'MimeType=' >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=Utility;WebBrowser' >> "${DESKTOP_ENTRY}"

# - 3 : validate shortcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

print_error "Installation successfully done!"
print_error "ENJOY!!!!"
sleep 5
#------------------------------------------------------------------
exit 0





