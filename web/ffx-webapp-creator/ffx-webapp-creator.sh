#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) ffx-webapps : Create webapps with Firefox on Linux systems
#@(#)--------------------------------------------------------------
#@(#) Create webapps with Firefox on Linux systems
#@(#) Dependencies : Zenity
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0
#@(#)--------------------------------------------------------------
#@(#) History
#@(#)--------------------------------------------------------------
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang
#------------------------------------------------------------------
P_UID=$(id -u)
#[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

if [ ${P_UID:--1} -ne 0 ]
then
DESKTOP_ENTRY_DIR="${HOME}/.local/share/applications"
ICONS_DIR="${HOME}/.local/share/icons"
mkdir -p "${ICONS_DIR}"
fi

if [ ${P_UID:--1} -eq 0 ]
then
DESKTOP_ENTRY_DIR="/usr/share/applications"
ICONS_DIR="/usr/share/pixmaps"
fi

LAUNCHER_ICON="${ICONS_DIR}/Oxygen480-apps-internet-web-browser.svg"
DEFAULT_LAUNCHER_ICON="${LAUNCHER_ICON}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------
# List of existing webapps
HAS_EXISTING_WEBAPPS=$(grep "X-FFXWebApp-Name=" ${DESKTOP_ENTRY_DIR}/*.desktop | awk -F':' '{print $1}' | wc -l)

if  [ ${HAS_EXISTING_WEBAPPS:-0} -gt 0 ]
then

WEBAPP_ACTION=$(grep "X-FFXWebApp-Name=" ${DESKTOP_ENTRY_DIR}/*.desktop | awk -F':' '{print $2}' | cut -d'=' -f2- | sort | uniq | awk '{print "FALSE;" $0}' | tr ';' '\n' | zenity --list --radiolist  --separator="|" --title="${TITLE_WEBAPP_CHOOSE}" --column="#" --column="${WEBAPP_CHOOSE_COL1}" --width=500 --height=300)
RETURN_ACTION=${?}

[ ${RETURN_ACTION:-0} -eq 0 -a "x${WEBAPP_ACTION}x" != "xx" ] && {
DESKTOP_FILE="$( grep "X-FFXWebApp-Name=${WEBAPP_ACTION}" ${DESKTOP_ENTRY_DIR}/*.desktop | awk -F':' '{print $1}' )"
PROFILE_NAME="$( cat ${DESKTOP_FILE} | grep "StartupWMClass=" | cut -d'=' -f2- )"
EXEC_CMD="$( cat ${DESKTOP_FILE} | grep "Exec=" | head -1 | cut -d'=' -f2- )"
FFX_PID=$( ps -aux | grep "${PROFILE_NAME}" | grep -v ' grep ' | awk '{print $2}' )

ACTION_REQUEST="$( zenity --question --width=500 --text="${TEXT_LAUNCH_WEBAPP}" --ok-label="${BUTTON_CREATE_WEBAPP}" --cancel-label="${BUTTON_CANCEL}" --extra-button="${BUTTON_LAUNCH_WEBAPP}" --extra-button="${BUTTON_DELETE_WEBAPP}" )"
RETURN_REQUEST=${?}

# Launch webapp ?
[ "${ACTION_REQUEST:-none}" = "${BUTTON_LAUNCH_WEBAPP}" ] && {
[ ${FFX_PID:-0} -gt 0 ] && zenity --error --width=500 --text="${ERROR_RUN_ALREADY}" && exit 1
${EXEC_CMD} &
exit 0
}

# Delete the webApp ?
[ "${ACTION_REQUEST:-none}" = "${BUTTON_DELETE_WEBAPP}" ] && {
# kill the processus
[ ${FFX_PID:-0} -gt 0 ] && kill -s TERM ${FFX_PID}
# remove the profile
[ -f "${DESKTOP_FILE}" ] && rm -f "${DESKTOP_FILE}"
zenity --info --width=500 --text="${SUCCESS_WEBAPP_DELETE}"
exit 0
}

# Cas de l'annulation de l'action
[ ${RETURN_REQUEST:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

}
fi
#------------------------------------------------------------------
# Define Webapp's name
WEBAPP_REQUEST=$(zenity --entry --title="${TITLE_WEBAPP_REQUEST}" --text="${LABEL_WEBAPP_REQUEST}" --entry-text="${PLACEHOLDER_WEBAPP_REQUEST}" --width=500)
RETURN_WEBAPP=${?}

# Cas de l'annulation de création
[ ${RETURN_WEBAPP:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_WEBAPP:-1} -ne 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

[ "${RETURN_WEBAPP}" = "" ] && {
zenity --error --width=500 --text="${ERROR_NO_WEBAPP_DEFINED}"
exit 1
}

# Normalisation du nom de profil sans espaces
WEBAPP_NAME="$(echo "${WEBAPP_REQUEST}" | sed 's/ /-/g' | sed 's/=/-/g')"

[ "${WEBAPP_NAME}" = "" ] && {
zenity --error --width=500 --text="${ERROR_WEBAPP_INCORRECT}"
exit 1
}

# Définition du lanceur
WEBAPP_LAUNCHER="${DESKTOP_ENTRY_DIR}/webapp-${WEBAPP_NAME}.desktop"
#------------------------------------------------------------------
# Define Webapp's URL
WEBAPP_URL=$(zenity --entry --title="${WEBAPP_REQUEST} | ${TITLE_URL_REQUEST}" --text="${LABEL_URL_REQUEST}" --entry-text="${PLACEHOLDER_URL_REQUEST}" --width=500)
RETURN_URL=${?}

# Cas de l'annulation de création
[ ${RETURN_URL:-1} -eq 1 ] && {
zenity --warning --width=500 --text="${WARNING_CANCEL}"
exit 0
}

# Cas non défini
[ ${RETURN_URL:-1} -ne 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

# Cas vide
[ "${WEBAPP_URL}" = "" ] && {
zenity --error --width=500 --text="${ERROR_URL_INCORRECT}"
exit 1
}

#------------------------------------------------------------------
# Contrôles

# Vérification du profil existant
[ -f "${WEBAPP_LAUNCHER}" ] && {
zenity --error --width=500 --text="${ERROR_CONFIG_ALREADY_EXISTS}"
# Lancement du gestionnaire de profils Firefox
firefox --profilemanager

exit 1
}

#------------------------------------------------------------------
# Choix d'une icône spécifique pour le lanceur
zenity --question --width=500 --text="${LABEL_ICON_REQUEST}"
RETURN_ICON=${?}

[ ${RETURN_ICON:-1} -lt 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}" 
exit 1
}

if [ ${RETURN_ICON:-1} -eq 0 ]
then

# Choix de l'icône
LAUNCHER_ICON=$(zenity --file-selection --title="${WEBAPP_REQUEST} | ${TITLE_INPUT_ICON}" --file-filter="*.svg *.png")
OK_ICON=${?}

[ ${OK_ICON:-1} -lt 0 ] && {
zenity --error --width=500 --text="${ERROR_UNEXPECTED}"
exit 1
}

[ ${OK_ICON:-1} -gt 0 ] && zenity --warning --width=500 --text="${WARNING_NO_FILE_SELECTED}"
[ ${OK_ICON:-1} -gt 0 ] && LAUNCHER_ICON="${DEFAULT_LAUNCHER_ICON}"

# Define the new icon
mkdir -p "${ICONS_DIR}"
cp "${LAUNCHER_ICON}" "${ICONS_DIR}/"
ICON_NAME="$( basename ${LAUNCHER_ICON} )"
LAUNCHER_ICON="${ICONS_DIR}/${ICON_NAME}"

fi

# --> If not defined, the default value is used

#------------------------------------------------------------------
# Création du profil dans Firefox
firefox -CreateProfile "${WEBAPP_NAME}"
zenity --info --width=500 --text="${SUCCESS_PROFILE_CREATION}"

#------------------------------------------------------------------
# Private window
zenity --question --width=500 --text="${LABEL_PRIVATE_REQUEST}"
RETURN_PRIVATE=${?}
PRIVATE_OPTION=""

# Cas non défini
[ ${RETURN_PRIVATE:-1} -ne 0 ] && zenity --warning --width=500 --text="${WARNING_NO_PRIVATE}"
[ ${RETURN_PRIVATE:-1} -eq 0 ] && PRIVATE_OPTION="--private-window"
#------------------------------------------------------------------
# Création du lanceur
#Exec=firefox --class Sample -P Sample https://duckduckgo.com

# The -no-remote flag allows multiple firefox profiles to run at the same time.
echo "[Desktop Entry]" > ${WEBAPP_LAUNCHER}
echo "Version=1.0" >> ${WEBAPP_LAUNCHER}
echo "Name=${WEBAPP_REQUEST}" >> ${WEBAPP_LAUNCHER}
echo "GenericName=Web App : ${WEBAPP_NAME} " >> ${WEBAPP_LAUNCHER}
echo "Comment=${WEBAPP_REQUEST} ${COMMENT_PROFILE}" >> ${WEBAPP_LAUNCHER}
echo "Exec=firefox -P ${WEBAPP_NAME} -no-remote --class ${WEBAPP_NAME}Profile ${PRIVATE_OPTION} ${WEBAPP_URL}" >> ${WEBAPP_LAUNCHER}
echo "Icon=${LAUNCHER_ICON}" >> ${WEBAPP_LAUNCHER}
echo "Type=Application" >> ${WEBAPP_LAUNCHER}
echo "Terminal=false" >> ${WEBAPP_LAUNCHER}
echo "Categories=GTK;Network;WebBrowser;" >> ${WEBAPP_LAUNCHER}
echo "StartupNotify=true" >> ${WEBAPP_LAUNCHER}
echo "StartupWMClass=${WEBAPP_NAME}Profile" >> ${WEBAPP_LAUNCHER}
echo "Actions=profile-manager;new-window;" >> ${WEBAPP_LAUNCHER}
echo "MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;" >> ${WEBAPP_LAUNCHER}
#MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/chrome;video/webm;application/x-xpinstall;
#echo "X-FFXWebApp-Browser=Firefox" >> ${WEBAPP_LAUNCHER}
echo "X-FFXWebApp-URL=${WEBAPP_URL}" >> ${WEBAPP_LAUNCHER}
echo "X-FFXWebApp-Name=${WEBAPP_REQUEST}" >> ${WEBAPP_LAUNCHER}
#echo "X-FFXWebApp-Isolated=true" >> ${WEBAPP_LAUNCHER}

echo "" >> ${WEBAPP_LAUNCHER}
echo "[Desktop Action profile-manager]" >> ${WEBAPP_LAUNCHER}
echo "Name=${NAME_PROFILE}" >> ${WEBAPP_LAUNCHER}
echo "Exec=firefox --profilemanager" >> ${WEBAPP_LAUNCHER}

echo "" >> ${WEBAPP_LAUNCHER}
echo "[Desktop Action new-window]" >> ${WEBAPP_LAUNCHER}
echo "Name=${NAME_NEW_WINDOW}" >> ${WEBAPP_LAUNCHER}
echo "Exec=firefox -new-window %u" >> ${WEBAPP_LAUNCHER}

# contrôle
[ ! -f "${WEBAPP_LAUNCHER}" ] && {
zenity --error --width=500 --text="${ERROR_CREATION}"
exit 1
}

# - 7 : validate shortcut 
HAS_ISSUES=$( desktop-file-validate "${WEBAPP_LAUNCHER}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && zenity --warning --width=500 --text="${WARNING_LAUNCHER_ISSUES}"

# Info succès !
zenity --info --width=500 --text="${SUCCESS_LAUNCHER_CREATION}"

#------------------------------------------------------------------
# Launch webapp ?
zenity --question --width=500 --text="${TEXT_LAUNCH_WEBAPP}"
WEBAPP_LAUNCH=${?}
[ ${WEBAPP_LAUNCH:-1} -eq 0 ] && firefox -P ${WEBAPP_NAME} -no-remote --class ${WEBAPP_NAME}Profile ${PRIVATE_OPTION} ${WEBAPP_URL} &
#------------------------------------------------------------------
exit 0
