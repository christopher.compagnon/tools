# FFX WebApp Creator

**ffx-webapp-creator** is a tool to create WebApp from a Web site.


## Installation (command line)

1- download the sources and uncompress if needed.

2- go to the directory and copy it where you want to install the application.

3- open the application's directory and find *install.sh*.

2- make it executable (from command line):

    chmod +x ./install.sh

3- execute it (from command line):

    ./install.sh

## Installation (GUI)

1- download the sources and uncompress if needed.

2- go to the directory and copy it where you want to install the application.

3- open the application's directory and find *install.sh*.

4- right-click on the script *install.sh*, go to **Properties > Permissions** and check the box *Execution*.

3- right-click on the script *install.sh* and select *Run as a program…*.

4- let the script execute until the end.

## Multi-language management

The *xx.lang* files provide a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* (with xx = code of lang) to support your language.

To find the code of the lang used on your system, please run :

    echo ${LANG} | cut -c1-2

in your terminal.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")