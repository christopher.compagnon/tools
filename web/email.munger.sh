#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#)  A simple email munger
#@(#)  Convert an email address to something disguised for spammers
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

munger() {
cat - | tr '[:upper:]' '[:lower:]' | sed -e 's/\(.\)/\1\n/g' | while read character
do
[ "${character}" = "0" ] && echo '&#48;'
[ "${character}" = "1" ] && echo '&#49;'
[ "${character}" = "2" ] && echo '&#50;'
[ "${character}" = "3" ] && echo '&#51;'
[ "${character}" = "4" ] && echo '&#52;'
[ "${character}" = "5" ] && echo '&#53;'
[ "${character}" = "6" ] && echo '&#54;'
[ "${character}" = "7" ] && echo '&#55;'
[ "${character}" = "8" ] && echo '&#56;'
[ "${character}" = "9" ] && echo '&#57;'
[ "${character}" = "a" ] && echo '&#97;'
[ "${character}" = "b" ] && echo '&#98;'
[ "${character}" = "c" ] && echo '&#99;'
[ "${character}" = "d" ] && echo '&#100;'
[ "${character}" = "e" ] && echo '&#101;'
[ "${character}" = "f" ] && echo '&#102;'
[ "${character}" = "g" ] && echo '&#103;'
[ "${character}" = "h" ] && echo '&#104;'
[ "${character}" = "i" ] && echo '&#105;'
[ "${character}" = "j" ] && echo '&#106;'
[ "${character}" = "k" ] && echo '&#107;'
[ "${character}" = "l" ] && echo '&#108;'
[ "${character}" = "m" ] && echo '&#109;'
[ "${character}" = "n" ] && echo '&#110;'
[ "${character}" = "o" ] && echo '&#111;'
[ "${character}" = "p" ] && echo '&#112;'
[ "${character}" = "q" ] && echo '&#113;'
[ "${character}" = "r" ] && echo '&#114;'
[ "${character}" = "s" ] && echo '&#115;'
[ "${character}" = "t" ] && echo '&#116;'
[ "${character}" = "u" ] && echo '&#117;'
[ "${character}" = "v" ] && echo '&#118;'
[ "${character}" = "w" ] && echo '&#119;'
[ "${character}" = "x" ] && echo '&#120;'
[ "${character}" = "y" ] && echo '&#121;'
[ "${character}" = "z" ] && echo '&#122;'
[ "${character}" = "." ] && echo '&#46;'
[ "${character}" = "-" ] && echo '&#45;'
[ "${character}" = "_" ] && echo '&#95;'
[ "${character}" = "@" ] && echo '&#64;'
done
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
OUTPUT_TEXT=""
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

OUTPUT_TEXT=$(echo "${1}" | munger | tr -d '\n' | tr -d ' ')
echo ${OUTPUT_TEXT}

#------------------------------------------------------------------
exit 0