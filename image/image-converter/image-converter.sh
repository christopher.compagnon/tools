#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) image-converter : convert image formats
#@(#)--------------------------------------------------------------
#@(#) Convert image format with image-magick
#@(#) Dependencies : image-magick, Zenity
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - some improvments
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

getImageFormat () {

case ${1} in

*.webp)
echo "WEBP"
;;

*.png)
echo "PNG"
;;

*.jpg|*.jpeg)
echo "JPG"
;;

*.tif|*.tiff)
echo "TIFF"
;;

*)
echo "NONE"
;;

esac

}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
FORMAT_LST="jpg,jpeg,png,tif,tiff,webp"
FILE_FILTER_LST="$( echo ${FORMAT_LST} | tr ',' '\n' | awk  '{ print "*." $1 }' | tr '\n' ' ')"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------
# Input directory + file
INPUT_FILE="$(zenity --file-selection --file-filter="${FILE_FILTER_LST}" --title="${TITLE_INPUT_FILENAME}")"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_NO_FILE_DEFINED}" && exit 1
[ ! -f "${INPUT_FILE}" ] && zenity --error --width=300 --text="${INPUT_FILE}${ERROR_FILE_NOT_FOUND}" && exit 1

#------------------------------------------------------------------
FILE_DIR="$(dirname "${INPUT_FILE}")"
FILE_NAME="$(basename "${INPUT_FILE}")"
FILE_REQUEST="$( echo "${FILE_NAME}" | tr '[A-Z]' '[a-z]' )"
FILE_ROOT=$(echo "${FILE_NAME%.*}" | sed 's/[[:punct:]]/_/g' | sed 's/[[:space:]]/_/g')
FILE_EXT=$(echo "${FILE_NAME##*.}")
image="${FILE_ROOT}.${FILE_EXT}"

INPUT_FORMAT="$( getImageFormat "image.${FILE_EXT}" )"
[ "${INPUT_FORMAT}" = "NONE" ] && zenity --error --width=300 --text="${INPUT_FORMAT}${ERROR_UNKNWON_IMG_TYPE}" && exit 1
FILTER_INPUT_FORMAT="$( echo ${INPUT_FORMAT} | tr '[A-Z]' '[a-z]' )"
#------------------------------------------------------------------
# Choose output format
OUTPUT_EXT=$(echo ${FORMAT_LST} | tr ',' '\n' | grep -v "${FILTER_INPUT_FORMAT}" | awk  '{ print "FALSE;" $1 }' | tr ';' '\n' | zenity --list --radiolist --separator="," --title="${TITLE_FORMAT_CHOOSE}" --column="#" --column="${FORMAT_CHOOSE_COL1}" --width=500)
RETURN_REQUEST=${?}

# Cas de l'annulation de sélection
[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_NO_FORMAT_DEFINED}" && exit 1

OUTPUT_FILE="${FILE_DIR}/${FILE_ROOT}.${OUTPUT_EXT}"
# Si l'image existe déjà --> remplacer ?
if [ -f "${OUTPUT_FILE}" ]
then
REPLACE_REQUEST="$(zenity --question --text="${OUTPUT_FILE}${QUESTION_FILE_REPLACE}")"
RETURN_REQUEST=${?}
[ ${RETURN_REQUEST:-1} -lt 0 ] && zenity --error --width=300 --text="${ERROR_INVALID_CHOICE}" && exit 1
[ ${RETURN_REQUEST:-1} -gt 0 ] && zenity --info --width=300 --text="${INFO_NO_REPLACE}" && exit 0
fi
#------------------------------------------------------------------
# Convert
# [TODO]: jpg--> png - sometimes, the conversion rotates the image
convert "${INPUT_FILE}" "${OUTPUT_FILE}"
RETURN_CONVERSION=${?}

[ ${RETURN_CONVERSION:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_CONVERSION}" && exit 1
[ ! -f "${OUTPUT_FILE}" ] && zenity --error --width=300 --text="${OUTPUT_FILE}${ERROR_FILE_NOT_FOUND}" && exit 1
#------------------------------------------------------------------
zenity --info --width=300 --text="${SUCCESS_CONVERT_DONE}"
#------------------------------------------------------------------
exit 0
