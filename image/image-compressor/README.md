# Image Compressor

A simple tool to compress images using *Zenity* and *image magick*.

## Installation

Download the sources.

Make *install.sh* executable:

    chmod +x install.sh

Execute *install.sh*:

    ./install.sh

This installation can be performed by the user or root and creates shortcuts into the desktop.

## Usage

Use *image-compressor.sh* to convert an image to another format.

If you installed the script already, you just launch it from its shortcut.

## Multi-language management

The *xx.lang* files provide a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* (with xx = code of lang) to support your language.

To find the code of the lang used on your system, please run :

    echo ${LANG} | cut -c1-2

in your terminal.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")