#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) image-compressor : compress images
#@(#)--------------------------------------------------------------
#@(#) Compress images with image-magick
#@(#) Dependencies : image-magick, Zenity
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

is_integer () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
# true
echo 1
else
# false
echo 0
fi
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
TMP_DIR="/dev/shm/$$"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------

# Input directory + file
INPUT_FILE=$(zenity --file-selection --file-filter="*.png" --title="${TITLE_INPUT_FILENAME}")
RETURN_REQUEST=${?}

[ ! -f "${INPUT_FILE}" ] && zenity --error --text="${INPUT_FILE}${ERROR_FILE_NOT_FOUND}" && exit 1

#------------------------------------------------------------------
# Compressing

# Create output directory if not exist.
FILE_DIR="$(dirname ${INPUT_FILE})"
FILE_NAME="$(basename ${INPUT_FILE})"
FILE_ROOT=$(echo "${FILE_NAME%.*}" | sed 's/[[:punct:]]/_/g' | sed 's/[[:space:]]/_/g')
FILE_EXT=$(echo "${FILE_NAME##*.}")
image="${FILE_ROOT}.${FILE_EXT}"
mkdir -p "${TMP_DIR}"

IMG_FILE="${TMP_DIR}/${image}"
META_DATA="${TMP_DIR}/${FILE_ROOT}.dat"

# Déplacement dans le répertoire de travail
[ -f "${INPUT_FILE}" ] && cp "${INPUT_FILE}" "${IMG_FILE}"

# Détermination des propriétés de l'image
# Génération du fichier de métadonnées
identify -verbose "${IMG_FILE}" > "${META_DATA}"

# Vérification de la rotation
IMG_ORIENTATION=$(cat "${META_DATA}" | grep 'exif:Orientation:' | cut -d':' -f3- | tr -d ' ')

# Gestion de la rotation
[ ${IMG_ORIENTATION:-0} -gt 1 ] && {
convert -auto-orient "${IMG_FILE}" "${IMG_FILE}.tmp"
[ -f "${IMG_FILE}.tmp" ] && mv "${IMG_FILE}.tmp" "${IMG_FILE}"
identify -verbose "${IMG_FILE}" > "${META_DATA}"
}

# Redimensionnement
IMG_DIM=$(cat "${META_DATA}" | grep 'Geometry: ' | cut -d':' -f2- | cut -d'+' -f1)
#IMG_SIZE=$(cat "${META_DATA}" | grep 'Filesize: ' | cut -d':' -f2- | cut -d'+' -f1)
IMG_WIDTH=$(echo ${IMG_DIM} | cut -d'x' -f1)
IMG_HEIGHT=$(echo ${IMG_DIM} | cut -d'x' -f2)

#------------------------------------------------------------------
# Size
MIN_VALUE=$( expr ${IMG_WIDTH} \/ 20 )
DIFF_VALUE=$( expr ${IMG_WIDTH} - ${MIN_VALUE:-0} )
AVG_VALUE=$( expr ${DIFF_VALUE} \/ 2 )
STEP_VALUE=$( expr ${DIFF_VALUE} \/ 20 ) 
INPUT_SIZE=$(zenity --scale --title="${TITLE_INPUT_SIZE}" --text="${LABEL_INPUT_SIZE}" --max-value=${IMG_WIDTH} --min-value=${MIN_VALUE} --value=${AVG_VALUE} --step=${STEP_VALUE})
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-0} -ne 0 ] && zenity --error --text="${ERROR_SIZE_SELECTION}" && exit 1

IS_INTEGER=$(is_integer "${INPUT_SIZE}")
[ ${IS_INTEGER:-0} -ne 1 ] && zenity --error --text="${ERROR_VALUE_NOT_INTEGER}" && exit 1
IMG_WIDTH_SMALL=${INPUT_SIZE}

# Si taille demandée supérieure à l'image alors erreur
[ ${IMG_WIDTH:-0} -lt ${IMG_WIDTH_SMALL} ] && zenity --error --text="${ERROR_SIZE_TO_BIG}" && exit 1
#------------------------------------------------------------------

# Changement de dimensions pour compresser l'image
IMG_RATIO=$(echo "${IMG_WIDTH}/${IMG_WIDTH_SMALL}" | bc -l)
IMG_HEIGHT_SMALL=$(echo "${IMG_HEIGHT}/${IMG_RATIO}" | bc)
IMG_FILE_SMALL="${TMP_DIR}/${FILE_ROOT}_${IMG_WIDTH_SMALL}x${IMG_HEIGHT_SMALL}.${FILE_EXT}"

# Uniquement si l'image est plus grande
[ ${IMG_WIDTH:-0} -gt ${IMG_WIDTH_SMALL} ] && CONVERT=$(convert "${IMG_FILE}" -resize ${IMG_WIDTH_SMALL}x${IMG_HEIGHT_SMALL} ${IMG_FILE_SMALL})

# Définir l'image redimensionnée comme fichier de référence à utiliser
[ -f "${IMG_FILE_SMALL}" ] && IMG_FILE="${IMG_FILE_SMALL}"

# Copie du fichier dans le répertoir d'origine
cp "${IMG_FILE}" "${FILE_DIR}"
#------------------------------------------------------------------
zenity --info --width=300 --text="${SUCCESS_EXECUTION}"
#------------------------------------------------------------------
# Nettoyage
[ -d "${TMP_DIR}" ] && rm -Rf "${TMP_DIR}"
#------------------------------------------------------------------
exit 0
