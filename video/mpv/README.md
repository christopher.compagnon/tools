# Tools for MPV

## mpv-radio

Plays a radio stream from Internet.

### Installation

Download the sources from directory *mpv-radio*.

**Note** : you need to install *mpv* if you do not have on your system.


### Usage

Check if *mpv-radio-gui.sh* and *mpv-radio-notification.sh* are executable. If not, set the scripts to be.
Run the GUI (Zenity required) from *mpv-radio-gui.sh* as follows:

    ./mpv-radio-gui.sh

This GUI manages the streams, the start/stop playing.

## mpv-install-player-dvd

Install a shortcut to play a DVD with MPV.

Execute *mpv-install-player-dvd.sh*.

2 modes available :

- User
- Admin (root)

Executed as root :

    sudo ./mpv-install-player-dvd.sh

installs the shortcut for the whole system.

Executed as user :

    ./mpv-install-player-dvd.sh

installs the shortcut for the user only as local application.


## Licence

All the sources are licenced by [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
