#!/bin/sh
#set -x
cd "$(dirname ${0})"
SCRIPT_DIR="$(pwd)"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) mpv radio : notifications
#@(#)--------------------------------------------------------------
#@(#) Script managing notifications for mpv-radio
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - initial version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

status_stream () {

[ -f "${PID_FILE}" ] && PLAYER_PID=$( cat "${PID_FILE}" | head -1 )
C_PID=$( ps -aux | grep mpv | awk -v PID="${PLAYER_PID}" '( $2 == PID ) { print $2 }' )

[ ${C_PID:--1} -gt 0 ] && echo "1"
[ ${C_PID:--1} -le 0 ] && echo "0"

}

read_current_station () {
cat "${PLAYER_ACTIONS}" | grep "stream-play:" | tail -1 | cut -d':' -f2
}

read_current_title () {
cat "${PLAYER_ACTIONS}" | grep "icy-title:" | tail -1 | cut -d':' -f2-
}

hashsum () {
cat - | openssl dgst -sha256 | cut -d'=' -f2- | xargs
}

read_current_notification_status () {

[ ! -f "${NOTIFICATION_STATUS}" ] && echo "0"
[ -f "${NOTIFICATION_STATUS}" ] && cat "${NOTIFICATION_STATUS}" | head -1

}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
CONFIG_DIR="${HOME}/.config/mpv-radio"
mkdir -p ${CONFIG_DIR}
APP_CONF="${CONFIG_DIR}/app.conf"
PLAYLIST_FILE="${CONFIG_DIR}/app.m3u8"
PID_FILE="${CONFIG_DIR}/player.pid"
PLAYER_ACTIONS="${CONFIG_DIR}/app.tmp"
NOTIFICATION_STATUS="${CONFIG_DIR}/notification.status"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Init
IS_RUNNING=$( status_stream )
[ ! -f "${PLAYER_ACTIONS}" ] && print_error "One required file is missing..." && exit 1

while [ ${IS_RUNNING:-0} -eq 1 ]
do

# Afficher la piste courante
STATION_CURRENT="$( read_current_station )"
TITLE_CURRENT="$( read_current_title )"
PREVIOUS_STATUS="$( read_current_notification_status )"
CURRENT_STATUS="$( echo "${STATION_CURRENT}|${TITLE_CURRENT}" | hashsum )"
[ "${CURRENT_STATUS}" != "${PREVIOUS_STATUS}" ] && zenity --notification --window-icon="info" --text="${NOTIFICATION_LABEL}${STATION_CURRENT:-unknown}\n${TITLE_CURRENT:-unknown}"
echo "${CURRENT_STATUS}" > "${NOTIFICATION_STATUS}"

sleep 3
IS_RUNNING=$( status_stream )
done
#------------------------------------------------------------------
# Nettoyage
#[ -f "${NOTIFICATION_STATUS}" ] && rm -f "${NOTIFICATION_STATUS}"
#------------------------------------------------------------------
exit 0
