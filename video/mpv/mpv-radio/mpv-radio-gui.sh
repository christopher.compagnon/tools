#!/bin/sh
#set -x
cd "$(dirname ${0})"
SCRIPT_DIR="$(pwd)"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) mpv Radio : a simple radio player
#@(#)--------------------------------------------------------------
#@(#) A simple radio player using mpv to play streams
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - initial version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

play_stream () {
STREAM_NAME="${1}"

# A partir du nom, on doit trouver le flux dans le fichier
STREAM_URL="$( cat "${PLAYLIST_FILE}" | grep -v "#EXTM3U" | sed 's/$/##@@##/g' | tr -d '\n' | sed 's/#EXTINF:/\#EXTINF:/g' | sed 's/##@@##/,/g' | awk -F',' -v NAME="${STREAM_NAME}" '( $2 == NAME ) { print $3 }' )"

stop_stream
echo "stream-play:${STREAM_NAME}:${STREAM_URL}" > "${PLAYER_ACTIONS}"
mpv --cache=yes --demuxer-max-bytes=8192k --no-video --quiet "${STREAM_URL}" >> "${PLAYER_ACTIONS}" &
echo ${!} > "${PID_FILE}"

}

stop_stream () {
[ -f "${PID_FILE}" ] && PLAYER_PID=$( cat "${PID_FILE}" | head -1 )
[ ${PLAYER_PID:--1} -gt 0 ] && kill -15 ${PLAYER_PID}

}

status_stream () {

[ -f "${PID_FILE}" ] && PLAYER_PID=$( cat "${PID_FILE}" | head -1 )
C_PID=$( ps -aux | grep mpv | awk -v PID="${PLAYER_PID}" '( $2 == PID ) { print $2 }' )

[ ${C_PID:--1} -gt 0 ] && echo "1"
[ ${C_PID:--1} -le 0 ] && echo "0"

}


read_current_title () {
cat "${PLAYER_ACTIONS}" | grep "icy-title:" | tail -1 | cut -d':' -f2-

}

read_current_station () {
cat "${PLAYER_ACTIONS}" | grep "stream-play:" | tail -1 | cut -d':' -f2

}

display_gui () {

# Afficher l'interface de contrôle
STATION_CURRENT="$( read_current_station )"
ACTION_REQUEST="$( zenity --info  --width=400 --title="${TITLE_CURRENT_STATION}" --text="${STATION_CURRENT:-unknown}" --extra-button="${GUI_CTRL_STOP_BUTTON}" )"
RETURN_REQUEST=${?}

[ "${ACTION_REQUEST:-none}" = "${GUI_CTRL_STOP_BUTTON}" ] && stop_stream

}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
CONFIG_DIR="${HOME}/.config/mpv-radio"
mkdir -p ${CONFIG_DIR}
APP_CONF="${CONFIG_DIR}/app.conf"
PLAYLIST_FILE="${CONFIG_DIR}/app.m3u8"
PID_FILE="${CONFIG_DIR}/player.pid"
PLAYER_ACTIONS="${CONFIG_DIR}/app.tmp"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)

[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && exit 1

#------------------------------------------------------------------
# Créer la playlist si elle n'existe pas
[ ! -f "${PLAYLIST_FILE}" ] && echo "#EXTM3U" > "${PLAYLIST_FILE}"

# Si la radio est en train de jouer, revenir à l'interface
IS_RUNNING=$( status_stream )
[ ${IS_RUNNING:-0} -eq 1 ] && display_gui

# Ajouter un nouvel enregistrement
ADD_RECORD=$(zenity --forms --separator="|" --title="${TITLE_NEW_STREAM}" --text="${LABEL_NEW_STREAM}" --add-entry="${LABEL_NEW_STREAM_NAME}" --add-entry="${LABEL_NEW_STREAM_URL}")
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -eq 0 ] && {

STREAM_NAME="$( echo ${ADD_RECORD} | cut -d'|' -f1 )"
STREAM_URL="$( echo ${ADD_RECORD} | cut -d'|' -f2 )"

# Teste l'existence de flux
IS_EXIST=$( cat "${PLAYLIST_FILE}" | grep "${STREAM_URL}" | wc -l )

# Si le flux n'existe pas dans la playlist, alors on l'ajoute
[ ${IS_EXIST:-0} -eq 0 ] && {
echo "#EXTINF:-1,${STREAM_NAME}" >> "${PLAYLIST_FILE}"
echo "${STREAM_URL}" >> "${PLAYLIST_FILE}"
}

}

# Proposer une station à lire
HAS_STREAM=$( cat "${PLAYLIST_FILE}" | grep "^#EXTINF:" | wc -l )
[ ${HAS_STREAM:-0} -gt 0 ] && {

# Liste les stations
SELECT_STREAM=$( cat "${PLAYLIST_FILE}" | grep -v "#EXTM3U" | grep "^#EXTINF:" | awk -F',' '{ print "FALSE;" $2 }' | tr ';' '\n' | zenity --list --radiolist --title="${TITLE_STATION_LIST}" --column="#" --column="${STATION_LIST_COL1}" --width=500 )
RETURN_REQUEST=${?}

[ "x${SELECT_STREAM}x" != "xx" -a ${RETURN_REQUEST:-1} -eq 0 ] && play_stream "${SELECT_STREAM}"

}
# Y a-t-il des flux à jouer ?
HAS_STREAM=$( cat "${PLAYLIST_FILE}" | grep "^#EXTINF:" | wc -l )
[ ${HAS_STREAM:-0} -eq 0 ] && {
zenity --error --width=300 --text="${ERROR_NO_STREAM_TO_PLAY}"
exit 1
}

# La radio joue-t-elle ?
IS_RUNNING=$( status_stream )

# Si la radio n'est pas lancée
[ ${IS_RUNNING:-0} -eq 0 ] && {
zenity --error --width=300 --text="${ERROR_NOT_RUNNING}"
exit 1
}

# Si la radio est lancée
[ ${IS_RUNNING:-0} -eq 1 ] && {

# Lancement des notifications
${SCRIPT_DIR}/mpv-radio-notification.sh &

# Affichage de l'interface
display_gui

}

#------------------------------------------------------------------
exit 0
