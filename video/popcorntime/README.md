# Popcorn Time

**Popcorn Time** is an peer-to-peer streaming application with nice interface, working on GNU+Linux operating systems.

The software must be installed manually.

On **Ubuntu** – and others **Debian** heirs – this script installs automatically the software (version 0.4.6) as follows :

1- download the script on your Linux computer.

2- make it executable (from command line):

    chmod +x ./popcorn-time-install.sh

3- execute it as root (from command line):

    sudo ./popcorn-time-install.sh


## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")