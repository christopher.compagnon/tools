#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
ROOT_DIR="$(pwd)"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Popcorn Time install : Install Popcorn Time
#@(#)--------------------------------------------------------------
#@(#) Install Popcorn Time on Linux/Debian
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.2
#------------------------------------------------------------------
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - manages desktop-file-validate
#@(#) Version : 1.2 - fix bug on UID
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

install_package () {
# Check if package exists
# Note : dpkg -l does not return the snaps
STR_LENGTH=$( echo "${PACKAGE}" | wc -c )
HAS_LIBR=$( dpkg -l "{PACKAGE}" | grep "^ii" | wc -l )
HAS_PACKAGE=$( whereis "${PACKAGE}" | cut -d':' -f2- | sed 's/ //g' | wc -c )

# Install package if missing
[ ${HAS_LIBR:-0} -eq 0 -a ${HAS_PACKAGE:-0} -le 1 ] && apt install ${PACKAGE} -y
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

TMP_DIR="/tmp"
WRK_DIR="${TMP_DIR}/$$"
mkdir -p "${WRK_DIR}"
RESOURCE_URL="https://github.com/popcorn-official/popcorn-desktop/releases/download/v0.4.6/Popcorn-Time-0.4.6-linux64.zip"
FILE_NAME="$( basename ${RESOURCE_URL} )"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# - 1 : Download the binaries
print_error "Downloading the binaries…"
cd "${WRK_DIR}"
wget -v -a "${WRK_DIR}/resources.log" -P "${WRK_DIR}" ${RESOURCE_URL}

# Tests if the file is here
PKG_FILE="${WRK_DIR}/${FILE_NAME}"
[ ! -f "${PKG_FILE}" ] && print_error "Something went wrong when downloading the binaries ${RESOURCE_URL}." && exit 1

# - 2 : Install unzip && dependencies (they should not be always required but some users needed them to make Popcorn Time working)
print_error "Install dependencies…"
apt update
# apt install unzip libcanberra-gtk-module libgconf-2-4 libatomic1 -y
for PACKAGE in unzip libcanberra-gtk-module libgconf-2-4 libatomic1
do
install_package ${PACKAGE}
done

# - 3 : Extract the zip
print_error "Prepare installation…"
PKG_DIR="$( dirname "${PKG_FILE}" )/popcorn-time"
mkdir -p "${PKG_DIR}"
unzip "${PKG_FILE}" -d "${PKG_DIR}"

# - 4 : remove the old version
print_error "Uninstall the previous version…"
APP_DIR="/opt/popcorn-time"
[ -d "${APP_DIR}" ] && rm -fR "${APP_DIR}"
[ "$( ls -l /usr/bin/popcorn-time | cut -c1 )" = "l" ] && rm -f /usr/bin/popcorn-time

# - 4 : install the new version of application
print_error "Install the new version…"
mv "${PKG_DIR}" "/opt/"
[ ! -f "/opt/popcorn-time/Popcorn-Time" ] && print_error "Error during the installation!" && exit 1
[ -f "/opt/popcorn-time/Popcorn-Time" ] && ln -sf /opt/popcorn-time/Popcorn-Time /usr/bin/popcorn-time

# - 6 : Create .desktop file (so the launcher)
DESKTOP_ENTRY="/usr/share/applications/popcorn-time.desktop"

echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Version=0.4.6' >> "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=Popcorn Time' >> "${DESKTOP_ENTRY}"
echo 'Comment=Movies/series player based on P2P' >> "${DESKTOP_ENTRY}"
echo 'Exec=/usr/bin/popcorn-time' >> "${DESKTOP_ENTRY}"
echo 'Icon=/opt/popcorn-time/src/app/images/icon.png' >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=Application;Video' >> "${DESKTOP_ENTRY}"

# - 7 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

print_error "Installation successfully done!"
print_error "ENJOY!!!!"
#------------------------------------------------------------------
# Nettoyage
[ -d "${WRK_DIR}" ] && rm -Rf "${WRK_DIR}"
#------------------------------------------------------------------
exit 0
