# Hypnotix

**Hypnotix** is an IPTV streaming application with support for live TV, movies and series.

The interface, based on MPV, is available on **Linux Mint** by default.

For others distributions, you have to install it manually.

On **Ubuntu** – and others **Debian** heirs – this script installs automatically the software (version 1.1) as follows :

1- download the script on your Linux computer.

2- make it executable (from command line):

    chmod +x ./hypnotix-install.sh

3- execute it as root (from command line):

    sudo ./hypnotix-install.sh


## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
