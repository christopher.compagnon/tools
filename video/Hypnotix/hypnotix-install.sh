#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
ROOT_DIR="$(pwd)"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Hypnotix install : Install Hypnotix
#@(#)--------------------------------------------------------------
#@(#) Install Hypnotix on Linux/Debian
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

install_package () {
# Check if package exists
# Note : dpkg -l does not return the snaps
STR_LENGTH=$( echo "${PACKAGE}" | wc -c )
HAS_LIBR=$( dpkg -l "{PACKAGE}" | grep "^ii" | wc -l )
HAS_PACKAGE=$( whereis "${PACKAGE}" | cut -d':' -f2- | sed 's/ //g' | wc -c )

# Install package if missing
[ ${HAS_LIBR:-0} -eq 0 -a ${HAS_PACKAGE:-0} -le 1 ] && apt install ${PACKAGE} -y
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
[ ${UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

TMP_DIR="/tmp"
WRK_DIR="${TMP_DIR}/$$"
mkdir -p "${WRK_DIR}"
RESOURCE_URL="https://github.com/linuxmint/hypnotix/releases/download/1.1/hypnotix_1.1_all.deb"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# - 1 : Download the binaries
print_error "Downloading the binaries…"
cd "${WRK_DIR}"
wget -v -a "${WRK_DIR}/resources.log" -P "${WRK_DIR}" ${RESOURCE_URL}

# Tests if the file is here
PKG_FILE="${WRK_DIR}/hypnotix_1.1_all.deb"
[ ! -f "${PKG_FILE}" ] && print_error "Something went wrong when downloading the binaries ${RESOURCE_URL}." && exit 1

# - 2 : Update system
print_error "Install dependencies…"
apt update

# - 3 : install the new version of application
print_error "Install the new version…"
dpkg -i "${PKG_FILE}"
RETURN_DPKG=${?}
#[ ${RETURN:-1} -ne 0 ] && print_error "Error during the installation!" && exit 1

# Install dependencies
apt -y -f install
RETURN_DEPS=${?}
[ ${RETURN_DEPS:-1} -ne 0 ] && print_error "Error during the installation!" && exit 1

print_error "Installation successfully done!"
print_error "ENJOY!!!!"
#------------------------------------------------------------------
# Nettoyage
[ -d "${WRK_DIR}" ] && rm -Rf "${WRK_DIR}"
#------------------------------------------------------------------
exit 0
