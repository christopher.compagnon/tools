#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
ROOT_DIR="$(pwd)"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Molotov install : Install Molotov on Linux
#@(#)--------------------------------------------------------------
#@(#) Install Molotov on Linux/Debian
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.1
#------------------------------------------------------------------
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - fix bug on UID
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

CleanTempData () {
rm -fR "${TMP_DIR}"
}

usage () {
echo "Usage : ${SCRIPT_NAME} AppImage"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
#[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

TMP_DIR="/tmp/$$"


if [ ${P_UID:--1} -ne 0 ]
then
DESKTOP_ENTRY="${HOME}/.local/share/applications/molotov.desktop"
ICONS_DIR="${HOME}/.local/share/icons"
APP_DIR="${HOME}/Applications"
mkdir -p "${ICONS_DIR}"
mkdir -p "${APP_DIR}"
fi

if [ ${P_UID:--1} -eq 0 ]
then
DESKTOP_ENTRY="/usr/share/applications/molotov.desktop"
ICONS_DIR="/usr/share/pixmaps"
APP_DIR="/opt/Applications"
mkdir -p "${APP_DIR}"
fi

APP_LOGO_URL="https://inceptum-stor.icons8.com/3X11spLPC7eS/logo_molotov.png"
RESOURCE_URL="https://desktop-auto-upgrade.molotov.tv/linux/Molotov-4.4.8.AppImage"
APP_FILENAME="$( basename ${RESOURCE_URL} )"
APP_BIN="${APP_DIR}/${APP_FILENAME}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# - 0 : Check if binaries already installed
[ -f "${APP_BIN}" ] && IS_INSTALLED="true"
[ "${IS_INSTALLED:-false}" = "true" ] && print_error "Application already installed." && exit 0

mkdir -p "${TMP_DIR}"

# - 1 : Download the binaries
print_error "Downloading the binaries…"
cd "${TMP_DIR}"
print_error "Plase wait!"
wget -v -a "${TMP_DIR}/resources.log" -P "${TMP_DIR}" "${RESOURCE_URL}"
print_error "Done!"

# Tests if the file is here
PKG_FILE="${TMP_DIR}/${APP_FILENAME}"
[ ! -f "${PKG_FILE}" ] && print_error "Something went wrong when downloading the binaries ${RESOURCE_URL}." && CleanTempData && exit 1

# - 2 : let's remove the old versions
print_error "Uninstall the previous version…"
find ${APP_DIR} -maxdepth 1 -type f -name "Molotov-*.AppImage" -print | xargs rm -fR
print_error "Done!"

# - 3 : install the new version of application
print_error "Install the new version…"
mv "${PKG_FILE}" "${APP_DIR}/"
[ ! -f "${APP_BIN}" ] && print_error "Error during the installation!" && CleanTempData && exit 1
chmod +x "${APP_BIN}"
print_error "Done!"

# - 4 : install the icon
wget -v -a "${TMP_DIR}/resources.log" -O "${ICONS_DIR}/logo_molotov.png" "${APP_LOGO_URL}"

# - 5 : Create .desktop file (so the launcher)
echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=Molotov' >> "${DESKTOP_ENTRY}"
echo 'Comment=Voir la TV par Internet' >> "${DESKTOP_ENTRY}"
#echo "Exec=${APP_BIN} --no-sandbox %U" >> "${DESKTOP_ENTRY}"
echo "Exec=${APP_BIN}" >> "${DESKTOP_ENTRY}"
echo "TryExec=${APP_BIN}" >> "${DESKTOP_ENTRY}"
echo "Icon=${ICONS_DIR}/logo_molotov.png" >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=AudioVideo' >> "${DESKTOP_ENTRY}"

# - 6 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

print_error "Installation successfully done!"
print_error "ENJOY!!!!"
#------------------------------------------------------------------
# Nettoyage
CleanTempData
#------------------------------------------------------------------
exit 0
