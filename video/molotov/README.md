# Molotov

**Molotov** is a legal application to watch TV streams on computer.

The software must be installed manually.

On **Ubuntu** – and others **Debian** heirs – this script installs automatically the software (version 4.4.8) as follows.

## Installation (command line)

1- download the script *molotov-install.sh* on your Linux computer.

2- make it executable (from command line):

    chmod +x ./molotov-install.sh

3- execute it (from command line):

    ./molotov-install.sh

## Installation (GUI)

1- download the script *molotov-install.sh* on your Linux computer.

2- open the file manager and right-click on the script *molotov-install.sh*, go to **Properties > Permissions** and check the box *Execution*.

3- right-click on the script *molotov-install.sh* and select *Run as a program…*.

4- let the script execute until the end.


## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")