#!/bin/sh
# Gestion d'un diaporama
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Webcam On Live : tests if the webcam is on live
#@(#)--------------------------------------------------------------
#@(#) Tests if the webcam is on live
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage: ${SCRIPT_NAME} [-h]"
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
WAIT=1
#------------------------------------------------------------------
# Paramètres
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Tests if webcam is on live
IS_ON_LIVE=$(lsmod | grep '^uvcvideo' | awk '{print $3}')

# 0 - not on live
[ ${IS_ON_LIVE:-0} -eq 0 ] && {
# Put your actions here

}

# 1 - on live
[ ${IS_ON_LIVE:-0} -ne 0 ] && {
# Put your actions here

}
#------------------------------------------------------------------
# Wait n seconds more
#sleep ${WAIT:-1}
#------------------------------------------------------------------
exit 0
