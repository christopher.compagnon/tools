#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
ROOT_DIR="$(pwd)"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Super Tux Kartinstall : Install Super Tux Kart
#@(#)--------------------------------------------------------------
#@(#) Install Super Tux Kart on Linux/Debian
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original --> Super Tux Kart v 1.3
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

extract () {
if [ -f "${1}" ]
then
print_error "Uncompress the package ${1}…"
ERROR_EXTRACTING="Error extraction ${1}."
ERROR_UNKNWON_PKG_TYPE="Error unknwon."
ERROR_FILE_NOT_FOUND="File ${1} not found."

case ${1} in

*.tar.bz2)
tar xjf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.gz)
tar xzf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.bz2)
bunzip2 ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.rar)
unrar x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.gz)
gunzip ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar)
tar xf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tbz2)
tar xjf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tgz)
tar xzf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.zip)
unzip ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.Z)
uncompress ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.7z)
7z x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.deb)
ar x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.xz)
tar xf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.zst)
unzstd ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*)
print_error "${ERROR_UNKNWON_PKG_TYPE}" && exit 1
;;

esac

else
print_error "${ERROR_FILE_NOT_FOUND}" && exit 1
fi
print_error "Done!"
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
[ ${UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

TMP_DIR="/tmp"
APP_DIR="/opt"
WRK_DIR="${TMP_DIR}/$$"
mkdir -p "${WRK_DIR}"
RESOURCE_URL="https://github.com/supertuxkart/stk-code/releases/download/1.3/SuperTuxKart-1.3-linux-64bit.tar.xz"
FILE_NAME="$( basename ${RESOURCE_URL} )"
ROOT_FILE_1="${FILE_NAME%.*}"
DIR_NAME="${ROOT_FILE_1%.*}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# - 1 : Download the binaries
print_error "Downloading the binaries…"
cd "${WRK_DIR}"
wget -v -a "${WRK_DIR}/resources.log" -P "${WRK_DIR}" ${RESOURCE_URL}

# Tests if the file is here
PKG_FILE="${WRK_DIR}/${FILE_NAME}"
[ ! -f "${PKG_FILE}" ] && print_error "Something went wrong when downloading the binaries ${RESOURCE_URL}." && exit 1

# - 2 : Extract the zip
print_error "Prepare installation…"
extract "${PKG_FILE}"

# - 3 : remove the old version
print_error "Uninstall the previous version…"
find ${APP_DIR} -maxdepth 1 -type d -name "SuperTuxKart*" -print | xargs rm -fR

# - 4 : install the new version of application
print_error "Install the new version…"
mv "${PKG_DIR}" "${APP_DIR}/"
[ ! -d "${APP_DIR}/${DIR_NAME}" ] && print_error "Error during the installation!" && exit 1
# Copy the icon
cp "${SCRIPT_DIR}/supertuxkart.svg" "${APP_DIR}/${DIR_NAME}/"
# set the execution rigths on the launcher
LAUNCHER_EXEC="$( ls -1 ${APP_DIR}/${DIR_NAME}/*.sh | head -1 )"
chmod +x ${LAUNCHER_EXEC}

# - 5 : Create .desktop file (so the launcher)
DESKTOP_ENTRY="/usr/share/applications/SuperTuxKart.desktop"

echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=SuperTuxKart' >> "${DESKTOP_ENTRY}"
echo 'Comment=Open source game SuperTuxKart v1.3' >> "${DESKTOP_ENTRY}"
echo "Exec=${LAUNCHER_EXEC}" >> "${DESKTOP_ENTRY}"
echo "Icon=${APP_DIR}/${DIR_NAME}/supertuxkart.svg" >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=Games' >> "${DESKTOP_ENTRY}"

# - 6 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

print_error "Installation successfully done!"
print_error "ENJOY!!!!"
#------------------------------------------------------------------
# Nettoyage
[ -d "${WRK_DIR}" ] && rm -Rf "${WRK_DIR}"
#------------------------------------------------------------------
exit 0
