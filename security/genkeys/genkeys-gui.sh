#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) stegano-dec : RSA/DSA Keys generator
#@(#)--------------------------------------------------------------
#@(#) Generates RSA and DSA keys
#@(#) Dependencies : Zenity
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0
#------------------------------------------------------------------
#@(#) Version : 1.0 - initial version
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

StringEncode () {
cat - | openssl enc -a -base64 | tr '\n' ' ' | sed 's/ /%0A/g'
}

StringDecode () {
cat - | sed 's/%0A/ /g' | tr ' ' '\n'| openssl enc -d -base64  
}

is_integer () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
# true
echo 1
else
# false
echo 0
fi
}

CleanTempData () {
rm -fR "${TMP_DIR}"
[ -f "${LOG_SUB_FILE}" ] && rm -fR "${LOG_SUB_FILE}"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
APP_NAME="genkeys"
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
KEY_LENGTH_LST="2048,4096,8192,10240"
TMP_DIR="/dev/shm/$$"
APP_PRIVKEY_STORE_DIR="${HOME}/.config/${genkeys}/keys/privs"
APP_PUBKEY_STORE_DIR="${HOME}/.config/${genkeys}/keys/pubs"
FORCE="false"
LOG_SUB_FILE="${SCRIPT_DIR}/$$.log"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && CleanTempData && exit 1
#------------------------------------------------------------------
# Choisir la longueur des clefs
KEY_SIZE=$(echo ${KEY_LENGTH_LST} | tr ',' '\n' | awk '{print "FALSE;" $0}' | tr ';' '\n' | zenity --list --radiolist --separator="|" --title="${TITLE_LENGTH_CHOOSE}" --column="${LENGTH_CHOOSE_COL1}" --column="${LENGTH_CHOOSE_COL2}" --width=500 --height=300)
RETURN_LENGTH=${?}

# Cas de l'annulation de sélection
[ ${RETURN_LENGTH:-1} -eq 1 ] && {
zenity --error --width=300 --text="${ERROR_NO_LENGTH_DEFINED}"
exit 1
}

IS_INTEGER=$(is_integer ${KEY_SIZE})
[ ${IS_INTEGER:-0} -ne 1 ] && zenity --error --width=300 --text="${ERROR_NOT_INTEGER}" && exit 1
#------------------------------------------------------------------
# Génération des clefs
KEY_ID="$(date -u +'%c' | openssl dgst -md5 | cut -d'=' -f2 | xargs)"
. ./crypto.GenXSAKeySet.fct
RETURN_EXEC=${?}

[ ${RETURN_EXEC:-0} -ne 0 ] && zenity --error --width=300 --text="$(cat ${LOG_SUB_FILE} | tail -1)" && exit 1
#------------------------------------------------------------------
# Nettoyage
CleanTempData
#------------------------------------------------------------------
zenity --info --width=300 --text="${SUCCESS_DONE}:${KEY_ID}"
exit 0