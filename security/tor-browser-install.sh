#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
ROOT_DIR="$(pwd)"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) tor-browser-install : Install Tor Browser
#@(#)--------------------------------------------------------------
#@(#) Install Tor Browser
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

extract () {
if [ -f "${1}" ]
then
print_error "Uncompress the package ${1}…"
ERROR_EXTRACTING="Error extraction ${1}."
ERROR_UNKNWON_PKG_TYPE="Error unknwon."
ERROR_FILE_NOT_FOUND="File ${1} not found."

case ${1} in

*.tar.bz2)
tar xjf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.gz)
tar xzf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.bz2)
bunzip2 ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.rar)
unrar x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.gz)
gunzip ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar)
tar xf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tbz2)
tar xjf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tgz)
tar xzf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.zip)
unzip ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.Z)
uncompress ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.7z)
7z x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.deb)
ar x ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.xz)
tar xf ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*.tar.zst)
unzstd ${1}
[ ${?} -ne 0 ] && print_error "${ERROR_EXTRACTING}" && exit 1
IS_EXTRACTED="true"
;;

*)
print_error "${ERROR_UNKNWON_PKG_TYPE}" && exit 1
;;

esac

else
print_error "${ERROR_FILE_NOT_FOUND}" && exit 1
fi
print_error "Done!"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
UID=$(id -u)
[ ${UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

APP_VERSION="11.0.6"
APP_NAME="tor-browser-linux64-${APP_VERSION}_fr"
APP_DIR="/opt/${APP_NAME}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

TMP_DIR="/tmp/$$"
WRK_DIR="${TMP_DIR}/${APP_NAME}"
mkdir -p "${WRK_DIR}"
RESOURCE_URL="https://www.torproject.org/dist/torbrowser/${APP_VERSION}/${APP_NAME}.tar.xz"
SIGNATURE_URL="https://www.torproject.org/dist/torbrowser/${APP_VERSION}/${APP_NAME}.tar.xz.asc"

# - 1 : Download the binaries
print_error "Downloading the binaries…"
cd "${WRK_DIR}"
wget -v -a "${TMP_DIR}/resources.log" -P "${WRK_DIR}" ${RESOURCE_URL}

# Tests if the file is here
PKG_FILE="${WRK_DIR}/${APP_NAME}.tar.xz"
[ ! -f "${PKG_FILE}" ] && print_error "Something went wrong when downloading the binaries ${RESOURCE_URL}." && exit 1

# - 2 : Download the signature
print_error "Downloading the signature"
cd "${WRK_DIR}"
wget -v -a "${TMP_DIR}/resources.log" -P "${WRK_DIR}" ${SIGNATURE_URL}

# Tests if the file is here
SIGN_FILE="${WRK_DIR}/${APP_NAME}.tar.xz.asc"
[ ! -f "${SIGN_FILE}" ] && print_error "Something went wrong when downloading the signature ${SIGNATURE_URL}." && exit 1

# - Uncompress the package
extract "${PKG_FILE}"
PKG_DIR="$( dirname "${PKG_FILE}" )"

# Installation
print_error "Installing the application…"
[ -d "${APP_DIR}" ] && rm -fR "${APP_DIR}"
mv "${PKG_DIR}" "${APP_DIR}"
APP_LAUNCHER=$( find ${APP_DIR}/ -type f -name "start-tor-browser.desktop" -print | head -1 )

# Launch the launcher
[ ! -f "${APP_LAUNCHER}" ] && print_error "Cannot find the launcher ${APP_LAUNCHER}." && exit 1 
chmod +x "${APP_LAUNCHER}"
${APP_LAUNCHER} --register-app
print_error "Done!"
#------------------------------------------------------------------
# Nettoyage
[ -d "${WRK_DIR}" ] && rm -fR ${WRK_DIR}
rm -f ${APP_DIR}/*.tar.xz
rm -f ${APP_DIR}/*.tar.xz.asc

#------------------------------------------------------------------
exit 0
