#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) stegano-dec : Stegano Decrypter
#@(#)--------------------------------------------------------------
#@(#) Unhide data hidden in a file
#@(#) Dependencies : Zenity
#@(#) Author : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - manages zip files
#@(#) Version : 1.2 - manages gif files
#@(#) Version : 1.3 - manages tar files
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

StringEncode () {
cat - | openssl enc -a -base64 | tr '\n' ' ' | sed 's/ /%0A/g'
}

StringDecode () {
cat - | sed 's/%0A/ /g' | tr ' ' '\n'| openssl enc -d -base64  
}

CleanTempData () {
rm -fR "${TMP_DIR}"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
FORMAT_LST="gif,jpg,jpeg,png,pdf,tar,zip"
FILE_FILTER_LST="$( echo ${FORMAT_LST} | tr ',' '\n' | awk  '{ print "*." $1 }' | tr '\n' ' ')"
TMP_DIR="/dev/shm/$$"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && CleanTempData && exit 1

#------------------------------------------------------------------
# Input directory + file
INPUT_FILE=$(zenity --file-selection --file-filter="${FILE_FILTER_LST}" --title="${TITLE_INPUT_FILENAME}")
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_FILE_DEFINED}" && CleanTempData && exit 1
[ ! -f "${INPUT_FILE}" ] && zenity --error --width=400 --text="${INPUT_FILE}${ERROR_FILE_NOT_FOUND}" && CleanTempData && exit 1

# Has stegano data
HAS_STEGANO=$(cat "${INPUT_FILE}" | sed 's/<!\[CDATA\[/\n<!\[CDATA\[/g' | grep -a -n '<!\[CDATA\[' | cut -d':' -f1)
[ ${HAS_STEGANO:-0} -eq 0 ] && zenity --error --width=400 --text="${ERROR_NO_STEG_DATA}" && CleanTempData && exit 1

INPUT_FILE_DIR="$(dirname "${INPUT_FILE}")"
#------------------------------------------------------------------
# Passcode key
PASSCODE="$(zenity --password --title="${TITLE_INPUT_PASSCODE_1}")"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_PASSCODE_DEFINED}" && CleanTempData && exit 1
#------------------------------------------------------------------
# Extract secret part from file
mkdir -p "${TMP_DIR}"
INPUT_DATA_FILE="${TMP_DIR}/$$.dat"

NB_LINES=$(cat "${INPUT_FILE}" | sed 's/<!\[CDATA\[/\n<!\[CDATA\[/g' | wc -l)
DATA_PART=$(expr ${NB_LINES} - ${HAS_STEGANO} + 1)

cat "${INPUT_FILE}" | sed 's/<!\[CDATA\[/\n<!\[CDATA\[/g' | tail -${DATA_PART} | tr '\n' '#' | sed 's/#/%0A/g' > "${INPUT_DATA_FILE}"

# Extract mode
MODE="$( cat "${INPUT_DATA_FILE}" | sed 's/<!\[CDATA\[\(.*\)\]\]>/\1/g' | sed 's/%0A/#/g' | tr '#' '\n' | head -1 | cut -d ':' -f1)"

[ "${MODE:-none}" = "none" ] && zenity --error --width=400 --text="${ERROR_NO_STEG_DATA}" && CleanTempData && exit 1 

[ "${MODE:-none}" = "FILE" ] && {
DATA_FILE_NAME="$( cat "${INPUT_DATA_FILE}" | sed 's/<!\[CDATA\[FILE:.*:\(.*\):.*\]\]>/\1/g' | sed 's/%0A//g' )"
HASH_FILE="$( cat "${INPUT_DATA_FILE}" | sed 's/<!\[CDATA\[FILE:.*:.*:\(.*\)\]\]>/\1/g' | sed 's/%0A//g' )"
DATA_FILE_ENC="${TMP_DIR}/${HASH_FILE}.aes"
TEMP_FILE="${TMP_DIR}/${DATA_FILE_NAME}"

# Extract data
cat "${INPUT_DATA_FILE}" | sed 's/<!\[CDATA\[FILE:\(.*\):.*:.*\]\]>/\1/g' | sed 's/%0A/#/g' | tr '#' '\n' > "${DATA_FILE_ENC}"

# Decrypt file
TEMP_FILE_COMP="${TMP_DIR}/${HASH_FILE}.tar.xz"
cat "${DATA_FILE_ENC}" | openssl aes-256-cbc -d -salt -pbkdf2 -base64 -k "${PASSCODE}" > "${TEMP_FILE_COMP}"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_DECRYPT_MSG}" && CleanTempData && exit 1

# If file compressed, then uncompress
IS_COMPRESSED=$(echo "${DATA_FILE_NAME}" | tr '[A-Z]' '[a-z]' | grep "*.tar.xz" | wc -l)
if [ ${IS_COMPRESSED:-0} -eq 0 ]
then
tar -J -C "${TMP_DIR}" -xf "${TEMP_FILE_COMP}"
else
mv "${TEMP_FILE_COMP}" "${TEMP_FILE}"
fi

[ ! -f "${TEMP_FILE}" ] && zenity --error --width=400 --text="${DATA_FILE_NAME}${ERROR_FILE_NOT_FOUND}" && CleanTempData && exit 1

# Output directory
OUTPUT_FILE="${INPUT_FILE_DIR}/${DATA_FILE_NAME}"
OUTPUT_FILE="$(zenity --file-selection --save --filename="${OUTPUT_FILE}" --title="${TITLE_OUTPUT_FILENAME}")"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_FILE_DEFINED}" && CleanTempData && exit 1

# Si l'image existe déjà --> remplacer ?
if [ -f "${OUTPUT_FILE}" ]
then
zenity --question --width=400 --text="${OUTPUT_FILE}${QUESTION_FILE_REPLACE}"
RETURN_REQUEST=${?}
[ ${RETURN_REQUEST:-1} -lt 0 ] && zenity --error --width=400 --text="${ERROR_INVALID_CHOICE}" && CleanTempData && exit 1
[ ${RETURN_REQUEST:-1} -gt 0 ] && zenity --info --width=400 --text="${INFO_NO_REPLACE}" && CleanTempData && exit 0
fi

mv "${TEMP_FILE}" "${OUTPUT_FILE}"

zenity --info --width=400 --text="${SUCCESS_STEGANO_DONE}"
}

[ "${MODE:-none}" = "TEXT" ] && {
ENC_MSG="$( cat "${INPUT_DATA_FILE}" | sed 's/<!\[CDATA\[TEXT:\(.*\)\]\]>/\1/g' | sed 's/%0A//g' )"

# Decrypts message
INPUT_MSG="$( echo "${ENC_MSG}" | openssl aes-256-cbc -d -salt -pbkdf2 -base64 -k "${PASSCODE}" )"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_DECRYPT_MSG}" && CleanTempData && exit 1

zenity --info --width=400 --text="${INPUT_MSG}"
}

#------------------------------------------------------------------
# Nettoyage
CleanTempData
#------------------------------------------------------------------
exit 0
