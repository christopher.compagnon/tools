#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) stegano-enc : Stegano Encrypter
#@(#)--------------------------------------------------------------
#@(#) Hide data into a file
#@(#) Dependencies : Zenity
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - manages zip files
#@(#) Version : 1.2 - manages gif files
#@(#) Version : 1.3 - manages tar files
# Notes : 
# - webp partially compatible : can be opened by Web browser but not Gimp.
# - odt partially compatible : can store message but not file.
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

StringEncode () {
cat - | openssl enc -a -base64 | tr '\n' ' ' | sed 's/ /%0A/g'
}

StringDecode () {
cat - | sed 's/%0A/ /g' | tr ' ' '\n'| openssl enc -d -base64  
}

CleanTempData () {
rm -fR "${TMP_DIR}"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
FORMAT_LST="gif,jpg,jpeg,png,pdf,tar,zip"
FILE_FILTER_LST="$( echo ${FORMAT_LST} | tr ',' '\n' | awk  '{ print "*." $1 }' | tr '\n' ' ')"
TMP_DIR="/dev/shm/$$"
mkdir -p "${TMP_DIR}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)
[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && CleanTempData && exit 1

#------------------------------------------------------------------
# Input directory + file (image)
INPUT_FILE=$(zenity --file-selection --file-filter="${FILE_FILTER_LST}" --title="${TITLE_INPUT_FILENAME}")
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_FILE_DEFINED}" && CleanTempData && exit 1
[ ! -f "${INPUT_FILE}" ] && zenity --error --text="${INPUT_FILE}${ERROR_FILE_NOT_FOUND}" && CleanTempData && exit 1

#------------------------------------------------------------------
# Has stegano data ?
HAS_STEGANO=$(cat "${INPUT_FILE}" | sed 's/<!\[CDATA\[/\n<!\[CDATA\[/g' | grep -a -n '<!\[CDATA\[' | cut -d':' -f1)
[ ${HAS_STEGANO:-0} -ne 0 ] && zenity --error --width=400 --text="${ERROR_HAS_STEG_DATA}" && CleanTempData && exit 1

#------------------------------------------------------------------
# Defines standard variables
INPUT_FILE_DIR="$(dirname "${INPUT_FILE}")"
INPUT_FILE_NAME="$(basename "${INPUT_FILE}")"
INPUT_FILE_REQUEST="$( echo "${INPUT_FILE_NAME}" | tr '[A-Z]' '[a-z]' )"
INPUT_FILE_ROOT=$(echo "${INPUT_FILE_NAME%.*}" | sed 's/[[:punct:]]/_/g' | sed 's/[[:space:]]/_/g')
INPUT_FILE_EXT=$(echo "${INPUT_FILE_NAME##*.}")
TEMP_FILE_STEG="${TMP_DIR}/${INPUT_FILE_ROOT}.secret.${INPUT_FILE_EXT}"

#------------------------------------------------------------------
# Passcode key
INPUT_KEY_1="$(zenity --password --title="${TITLE_INPUT_PASSCODE_1}")"
RETURN_REQUEST=${?}
[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_PASSCODE_DEFINED}" && CleanTempData && exit 1

INPUT_KEY_2="$(zenity --password --title="${TITLE_INPUT_PASSCODE_2}")"
RETURN_REQUEST=${?}
[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_PASSCODE_DEFINED}" && CleanTempData && exit 1

[ "${INPUT_KEY_1:-none}" != "${INPUT_KEY_2:-enon}" ] && zenity --error --width=400 --text="${ERROR_KEYS_INVALID}" && CleanTempData && exit 1
[ "${INPUT_KEY_1:-none}" = "${INPUT_KEY_2:-enon}" ] && PASSCODE="${INPUT_KEY_1}"

#------------------------------------------------------------------
# Cas du texte interactif
MODE="TEXT"
INPUT_MSG="$(zenity --entry --width=400 --title="${TITLE_INPUT_MSG}" --text="${LABEL_INPUT_MSG}" --entry-text="${PLACEHOLDER_INPUT_MSG}")"
RETURN_REQUEST=${?}

if [ ${RETURN_REQUEST} -eq 0 ]
then

# Encrypts message
ENC_MSG="$( echo "${INPUT_MSG}" | openssl aes-256-cbc -e -salt -pbkdf2 -base64 -k "${PASSCODE}" )"

# Hides message
echo "<![CDATA[TEXT:${ENC_MSG}]]>" | cat "${INPUT_FILE}" - > "${TEMP_FILE_STEG}"

fi

#------------------------------------------------------------------
# Cas du fichier à cacher
if [ ${RETURN_REQUEST} -gt 0 ]
then
MODE="FILE"

# Data directory + file
DATA_FILE=$(zenity --file-selection --file-filter="*.*" --title="${TITLE_DATA_FILENAME}")
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_FILE_DEFINED}" && CleanTempData && exit 1
[ ! -f "${DATA_FILE}" ] && zenity --error --width=400 --text="${DATA_FILE}${ERROR_FILE_NOT_FOUND}" && CleanTempData && exit 1

DATA_FILE_DIR="$(dirname "${DATA_FILE}")"
DATA_FILE_NAME="$(basename "${DATA_FILE}")"
HASH_FILE="$(cat "${DATA_FILE}" | openssl dgst -sha256 | cut -d "=" -f2- | xargs)"

# If file not compressed, then compress
IS_COMPRESSED=$(echo "${DATA_FILE_NAME}" | tr '[A-Z]' '[a-z]' | grep "*.tar.xz" | wc -l)
if [ ${IS_COMPRESSED:-0} -eq 0 ]
then
DATA_FILE_COMP="${TMP_DIR}/${HASH_FILE}.tar.xz"
tar -J -C "${DATA_FILE_DIR}" -cf "${DATA_FILE_COMP}" "${DATA_FILE_NAME}"
else
DATA_FILE_COMP="${DATA_FILE}"
fi

TEMP_FILE_ENC="${TMP_DIR}/${HASH_FILE}.aes"

# Encrypt file
[ ! -f "${DATA_FILE_COMP}" ] && zenity --error --text="${DATA_FILE_COMP}${ERROR_FILE_NOT_FOUND}" && CleanTempData && exit 1
cat "${DATA_FILE_COMP}" | openssl aes-256-cbc -e -salt -pbkdf2 -base64 -k "${PASSCODE}" > "${TEMP_FILE_ENC}"

# Encode
echo "<![CDATA[FILE:$(cat ${TEMP_FILE_ENC}):${DATA_FILE_NAME}:${HASH_FILE}]]>" | cat "${INPUT_FILE}" - > "${TEMP_FILE_STEG}"

fi

#------------------------------------------------------------------
# Output directory + file
OUTPUT_FILE_STEG="${INPUT_FILE_DIR}/${INPUT_FILE_ROOT}.secret.${INPUT_FILE_EXT}"
OUTPUT_FILE_STEG="$(zenity --file-selection --save --filename="${OUTPUT_FILE_STEG}" --title="${TITLE_OUTPUT_FILENAME}")"
RETURN_REQUEST=${?}

[ ${RETURN_REQUEST:-1} -ne 0 ] && zenity --error --width=400 --text="${ERROR_NO_FILE_DEFINED}" && CleanTempData && exit 1

# Si l'image existe déjà --> remplacer ?
if [ -f "${OUTPUT_FILE_STEG}" ]
then
zenity --question --width=400 --text="${OUTPUT_FILE_STEG}${QUESTION_FILE_REPLACE}"
RETURN_REQUEST=${?}
[ ${RETURN_REQUEST:-1} -lt 0 ] && zenity --error --width=400 --text="${ERROR_INVALID_CHOICE}" && CleanTempData && exit 1
[ ${RETURN_REQUEST:-1} -gt 0 ] && zenity --info --width=400 --text="${INFO_NO_REPLACE}" && CleanTempData && exit 0
fi

mv "${TEMP_FILE_STEG}" "${OUTPUT_FILE_STEG}"
#------------------------------------------------------------------
zenity --info --width=400 --text="${SUCCESS_STEGANO_DONE}"
#------------------------------------------------------------------
# Nettoyage
CleanTempData
#------------------------------------------------------------------
exit 0
