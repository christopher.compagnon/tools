# Stegano

This tool hides a message or a file into another file using *Zenity* GUI.

## Container data format

The files containing the encrypted data are **gif**, **jpg/jpeg**, **png**, **pdf**, **tar** and **zip**.

## Installation

### Installation (command line)

Download the sources.

Make *install.sh* executable:

    chmod +x install.sh

Execute *install.sh*:

    ./install.sh

### Installation (GUI)

1- download the script *install.sh* on your Linux computer.

2- open the file manager and right-click on the script *install.sh*, go to **Properties > Permissions** and check the box *Execution*.

3- right-click on the script *install.sh* and select *Run as a program…*.

4- let the script execute until the end.

## To launch the application

This installation can be performed by the user or root and creates shortcuts for both **Stegano Encrypter** and **Stegano Decrypter** into the desktop.

**Stegano Encrypter** appears as:

![Stegano Encrypter](Crystal128-file-locked.svg)

**Stegano Decrypter** appears as:

![Stegano Decrypter](Crystal128-binary.svg)

You can also execute **Stegano Encrypter** and **Stegano Decrypter** manually without any installation, by hand or through *Alacarte*. Make previously the scripts executable.

## Usage

### Stegano Encrypter

Use *stegano-enc.sh* to hide a message or a file into another file.

If you installed the script already, you just launch it from its shortcut.

**Note** : By default, the GUI asks for a message. If you want to encrypt a file, cancel the question about the message.

### Stegano Decrypter

Use *stegano-dec.sh* to decrypt a message or a file hidden in a file.

If you installed the script already, you just launch it from its shortcut.

## Multi-language management

The *xx.lang* files provide a translation for GUI.

If the file does not exist, please feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* (with xx = code of lang) to support your language.

To find the code of the lang used on your system, please run :

    echo ${LANG} | cut -c1-2

in your terminal.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")