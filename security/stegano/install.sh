#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) stegano install : install stegano scripts
#@(#)--------------------------------------------------------------
#@(#) Install stegano scripts
#@(#) Dependencies : none
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#@(#) Version : 1.1 - fix bug on UID
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
#[ ${P_UID:--1} -ne 0 ] && print_error "You need to be root to execute this script." && exit 1

if [ ${P_UID:--1} -ne 0 ]
then
cd "${SCRIPT_DIR}"
APP_DIR="$( pwd )"
DESKTOP_ENTRY_DIR="${HOME}/.local/share/applications"
ICONS_DIR="${HOME}/.local/share/icons"
mkdir -p "${ICONS_DIR}"
fi

if [ ${P_UID:--1} -eq 0 ]
then
DESKTOP_ENTRY_DIR="/usr/share/applications"
ICONS_DIR="/usr/share/pixmaps"
APP_DIR="/opt/stegano"
mkdir -p "${APP_DIR}"
fi

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Install Stegano Encrypter

# -1 copy the script
[ ${P_UID:--1} -eq 0 ] && {
cp "${SCRIPT_DIR}/stegano-enc.sh" "${APP_DIR}/"
cp "${SCRIPT_DIR}/*.lang" "${APP_DIR}/"
}
LAUNCHER_EXEC="${APP_DIR}/stegano-enc.sh"
[ ! -f "${LAUNCHER_EXEC}" ]  && print_error "ERROR: cannot install ${LAUNCHER_EXEC}." && exit 1 
chmod +x ${LAUNCHER_EXEC}

# - 2 : copy the icon
cp "${SCRIPT_DIR}/Crystal128-file-locked.svg" "${ICONS_DIR}/"

# - 3 : Create .desktop file (so the launcher)
DESKTOP_ENTRY="${DESKTOP_ENTRY_DIR}/stegano-enc.desktop"

echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Version=1.0' >> "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=Stegano Encrypter' >> "${DESKTOP_ENTRY}"
echo 'Comment=Hide message or file into another file' >> "${DESKTOP_ENTRY}"
echo "Exec=${LAUNCHER_EXEC}" >> "${DESKTOP_ENTRY}"
echo "Icon=${ICONS_DIR}/Crystal128-file-locked.svg" >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=Utility' >> "${DESKTOP_ENTRY}"

# - 4 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

#------------------------------------------------------------------
# Install Stegano Encrypter

# -1 copy the script
[ ${P_UID:--1} -eq 0 ] && {
cp "${SCRIPT_DIR}/stegano-dec.sh" "${APP_DIR}/"
cp "${SCRIPT_DIR}/*.lang" "${APP_DIR}/"
}

LAUNCHER_EXEC="${APP_DIR}/stegano-dec.sh"
[ ! -f "${LAUNCHER_EXEC}" ]  && print_error "ERROR: cannot install ${LAUNCHER_EXEC}." && exit 1 
chmod +x ${LAUNCHER_EXEC}

# - 2 : copy the icon
cp "${SCRIPT_DIR}/Crystal128-binary.svg" "${ICONS_DIR}/"

# - 3 : Create .desktop file (so the launcher)
DESKTOP_ENTRY="${DESKTOP_ENTRY_DIR}/stegano-dec.desktop"

echo '[Desktop Entry]' > "${DESKTOP_ENTRY}"
echo 'Version=1.2' >> "${DESKTOP_ENTRY}"
echo 'Type=Application' >> "${DESKTOP_ENTRY}"
echo 'Terminal=false' >> "${DESKTOP_ENTRY}"
echo 'Name=Stegano Decrypter' >> "${DESKTOP_ENTRY}"
echo 'Comment=Unhide data from file' >> "${DESKTOP_ENTRY}"
echo "Exec=${LAUNCHER_EXEC}" >> "${DESKTOP_ENTRY}"
echo "Icon=${ICONS_DIR}/Crystal128-binary.svg" >> "${DESKTOP_ENTRY}"
echo 'StartupNotify=true' >> "${DESKTOP_ENTRY}"
echo 'Categories=Utility' >> "${DESKTOP_ENTRY}"

# - 4 : validate shorcut 
HAS_ISSUES=$( desktop-file-validate "${DESKTOP_ENTRY}" | wc -l )
[ ${HAS_ISSUES:-0} -gt 0 ] && print_error "The launcher contains issues. Execute desktop-file-validate to see more details."

#------------------------------------------------------------------
print_error "Installation successfully done!"
print_error "ENJOY!!!!"
#------------------------------------------------------------------
exit 0