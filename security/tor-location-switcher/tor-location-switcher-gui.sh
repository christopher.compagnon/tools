#!/bin/sh
#set -x
cd "$(dirname ${0})"
SCRIPT_DIR="$(pwd)"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Tor Location Switcher
#@(#)--------------------------------------------------------------
#@(#) A simple tool to switch the location of Tor (with gui)
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - initial version
#@(#) Version : 1.1 - compatibility with -cmd alternative
#@(#) Version : 1.2 - add options for admin (root) + advanced options
#@(#) Version : 1.3 - fix bug on UID
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@"| sed 's/\\n/$/g' | tr '$' '\n' >&2
}

upd_param () {
PARAM="${1}"
VALUE="${2}"

HAS_PARAM=$(cat ${TORRC_CONF} | sed 's/#//g' | grep -n "^${PARAM} "  | cut -d':' -f1)
[ ${HAS_PARAM:-0} -gt 0 ] && {
NB_LINES=$(cat ${TORRC_CONF} | wc -l)
FIRST_PART=$(expr ${HAS_PARAM} - 1)
[ ${HAS_PARAM} -eq ${NB_LINES:-0} ] && LAST_PART=0
[ ${HAS_PARAM} -lt ${NB_LINES:-0} ] && LAST_PART=$(expr ${NB_LINES} - ${HAS_PARAM})
cat ${TORRC_CONF} | head -${FIRST_PART} > ${TORRC_CONF}.$$
echo "${PARAM} ${VALUE}" >> ${TORRC_CONF}.$$
[ ${LAST_PART} -gt 0 ] && cat ${TORRC_CONF} | tail -${LAST_PART} >> ${TORRC_CONF}.$$
}

[ ${HAS_PARAM:-0} -eq 0 ] && {
cat ${TORRC_CONF} > ${TORRC_CONF}.$$
echo "${PARAM} ${VALUE}" >> ${TORRC_CONF}.$$
}

# Application de la configuration
mv ${TORRC_CONF}.$$ ${TORRC_CONF}

}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
STRICT_EXIT_NODES="0"
ISO_3611="${SCRIPT_DIR}/ISO-3611.txt"
CONFIG_DIR="${HOME}/.config/tor-location-switcher"
HAS_CONFIGURATION="false"
TORRC_CONF="/etc/tor/torrc"
ENODE_CONF="${CONFIG_DIR}/enode.conf"
APP_CONF="${CONFIG_DIR}/app.conf"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
# Teste si Zenity est installé
HAS_ZENITY=$(whereis zenity | cut -d':' -f2- | wc -c)

[ ${HAS_ZENITY:-0} -lt 6 ] && print_error "${ERROR_ZENITY_NOT_INSTALLED}" && sleep 3 && exit 1

#------------------------------------------------------------------
mkdir -p "${CONFIG_DIR}"
[ -f ${APP_CONF} ] && . ${APP_CONF}

touch ${ENODE_CONF}

# Vérifier si Tor est en cours de fonctionnement
IS_RUNNING=$(ps -aux | grep torrc | grep -v "grep" | wc -l)
[ ${IS_RUNNING:-0} -gt 0 -a ${P_UID:--1} -ne 0 ] && {

zenity --error --width=300 --text="${ERROR_ALREADY_RUNNING}"
exit 1

}

# Si admin, alors proposer d'arrêter le service
[ ${IS_RUNNING:-0} -gt 0 -a ${P_UID:--1} -eq 0 ] && {

zenity --question --width=400 --text="${Q_STOP_SERVICE}"
RETURN_QUESTION=${?}

[ ${RETURN_QUESTION:-1} -eq 0 ] && service tor stop
[ ${RETURN_QUESTION:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_ALREADY_RUNNING}" && exit 0

}

#------------------------------------------------------------------
# Vérifier si le répertoire de torrc est trouvé

if [ ! -f "${TORRC_CONF}" ]
then
	
TORRC_CONF=$(zenity --file-selection --title="${TITLE_CONFIG_FILE}")
RETURN_TORRC=${?}
case ${RETURN_TORRC} in

0)
#Enregistrement du chemin de configuration
echo "TORRC_CONF=${TORRC_CONF}" > "${APP_CONF}"
;;

1)
zenity --error --text="${ERROR_NO_TORRC_DEFINED}"
exit 1
;;

-1)
zenity --error --text="${ERROR_UNEXPECTED}"
exit 1
;;
esac
	
fi

TORRC_DIR="$(dirname ${TORRC_CONF})"

#------------------------------------------------------------------
# Récupération des configurations
NB_CONF=$(cat ${ENODE_CONF} | wc -l)
CURRENT_CONF=$( expr ${NB_CONF:-0} + 1 )
if [ ${NB_CONF:-0} -gt 0 ]
then
COUNTRIES_REQUEST=$(cat ${ENODE_CONF} | awk -F';' '{print $2 ";" $3}' | tr ';' '\n' | zenity --list --radiolist --title="${TITLE_CONFIG_CHOOSE}" --column="#" --column="${CONFIG_CHOOSE_COL1}" --width=500)
RETURN_CONFIGURATION=${?}

# Cas de l'annulation de sélection
[ ${RETURN_CONFIGURATION:-1} -eq 1 ] && {
zenity --warning --width=300 --text="${WARNING_NO_COUNTRY_DEFINED}"
}

[ ${RETURN_CONFIGURATION:-1} -eq 0 ] && HAS_CONFIGURATION="true"
[ ${RETURN_CONFIGURATION:-1} -eq 0 ] && CURRENT_CONF=$(cat ${ENODE_CONF} | awk -F';' -v request="${COUNTRIES_REQUEST}" '( $3==request ) { print $1 }')
fi


if [ "${HAS_CONFIGURATION:-false}" = "false" ]
then
# Choix du pays
COUNTRIES_REQUEST=$(cat ${ISO_3611} | awk -F';' '( NR > 1 ) { print "FALSE;" $2 ";" $1 }' | tr ';' '\n' | zenity --list --checklist  --separator="," --title="${TITLE_COUNTRIES_CHOOSE}" --column="#" --column="${COUNTRIES_CHOOSE_COL1}" --column="${COUNTRIES_CHOOSE_COL2}" --width=500)
RETURN_COUNTRIES=${?}

# Cas de l'annulation de sélection
[ ${RETURN_COUNTRIES:-1} -eq 1 ] && {
zenity --error --width=300 --text="${ERROR_NO_COUNTRY_DEFINED}"
exit 1
}

fi

# Constitution de la ligne de commande
COUNTRIES_LIST="$(echo "{${COUNTRIES_REQUEST}}" | tr '[A-Z]' '[a-z]' | sed 's/,/},{/g')"
COUNTRIES_CMD="ExitNodes ${COUNTRIES_LIST} StrictNodes ${STRICT_EXIT_NODES}"

# Enregistrement de l'ancienne configuration
[ -f ${TORRC_CONF} ] && cp ${TORRC_CONF} ${TORRC_CONF}.previous
[ -f ${TORRC_CONF} ] && cp ${TORRC_CONF} ${TORRC_CONF}.$(date -u +'%Y%m%d_%H%M%S')

# Remplacement de la configuration dans le fichier
HAS_ENODE=$(grep -n 'ExitNodes ' ${TORRC_CONF} | cut -d':' -f1)
[ ${HAS_ENODE:-0} -gt 0 ] && {
NB_LINES=$(cat ${TORRC_CONF} | wc -l)
FIRST_PART=$(expr ${HAS_ENODE} - 1)
[ ${HAS_ENODE} -eq ${NB_LINES:-0} ] && LAST_PART=0
[ ${HAS_ENODE} -lt ${NB_LINES:-0} ] && LAST_PART=$(expr ${NB_LINES} - ${HAS_ENODE} - 1)
cat ${TORRC_CONF} | head -${FIRST_PART} > ${TORRC_CONF}.$$
echo ${COUNTRIES_CMD} >> ${TORRC_CONF}.$$
[ ${LAST_PART} -gt 0 ] && cat ${TORRC_CONF} | tail -${LAST_PART} > ${TORRC_CONF}.$$
}

[ ${HAS_ENODE:-0} -eq 0 ] && {
cat ${TORRC_CONF} > ${TORRC_CONF}.$$
echo "${COUNTRIES_CMD}" >> ${TORRC_CONF}.$$
}

# Application de la configuration
mv ${TORRC_CONF}.$$ ${TORRC_CONF}

# Sauvegarde de la configuration --> on désactive les autres config
cat "${ENODE_CONF}" | awk -F';' -v configuration="${CURRENT_CONF}" '($1 != configuration) {print $1 ";FALSE;" $3}' > ${ENODE_CONF}.$$
echo "${CURRENT_CONF};TRUE;${COUNTRIES_REQUEST}" >> ${ENODE_CONF}.$$
mv ${ENODE_CONF}.$$ ${ENODE_CONF}

#------------------------------------------------------------------
# Configuration avancée
CONFIG_COUNTRY_OK=$( zenity --info --width=500 --text="${SUCCESS_CONFIG_DONE}" --extra-button "${ADVANCED_CONFIG_BUTTON}" )
RETURN_CONFIG=${?}

[ "${CONFIG_COUNTRY_OK:-none}" = "${SUCCESS_CONFIG_ADVANCED_BUTTON}" ] && {
BWRATE_VALUE_CURRENT=$( cat ${TORRC_CONF} | sed 's/#//g' | grep '^RelayBandwidthRate ' | awk '{print $2}' )
BWRATE_UNIT_CURRENT=$( cat ${TORRC_CONF} | sed 's/#//g' | grep '^RelayBandwidthRate ' | awk '{print $3}' )
# Conversion in KB
[ "${BWRATE_UNIT_CURRENT:-KB}" = "MB" ] && BWRATE_VALUE_CURRENT=$( expr ${BWRATE_VALUE_CURRENT:-200} \* 1000 )
BWRATE_UNIT_CURRENT="KB"

BWRATE_VALUE=$( zenity --entry --title="${TITLE_CONFIG_BWRATE}" --text="${BWRATE_CHOOSE}" --entry-text="${BWRATE_VALUE_CURRENT:-500}" )
RETURN_BWRATE=${?}
BWBURST_VALUE=$( expr ${BWRATE_VALUE} \* 15  \/  10 )
RETURN_BWBURST=${?}

#RelayBandwidthRate 100 KB  # Throttle traffic to 100KB/s (800Kbps)
upd_param "RelayBandwidthRate" "${BWRATE_VALUE:-200} KB"
#RelayBandwidthBurst 200 KB # But allow bursts up to 200KB/s (1600Kbps)
upd_param "RelayBandwidthBurst" "${BWBURST_VALUE:-300} KB"

}
#------------------------------------------------------------------
# Nettoyage de l'historique
NB_HISTO=$(ls -1 ${TORRC_DIR}/torrc.*_* | wc -l)
[ ${NB_HISTO:-0} -gt 10 ] && {
NB_DELETE=$(expr ${NB_HISTO} - 10)
ls -1a ${TORRC_DIR}/torrc.*_* | head -${NB_DELETE} | xargs rm -f
}

# Si admin, alors proposer le démarrage du service
[ ${P_UID:--1} -eq 0 ] && {

zenity --question --width=300 --text="${Q_START_SERVICE}"
RETURN_QUESTION=${?}

[ ${RETURN_QUESTION:-1} -eq 0 ] && service tor start
#[ ${RETURN_QUESTION:-1} -ne 0 ] && zenity --error --width=300 --text="${ERROR_ALREADY_RUNNING}" && exit 0

}

zenity --notification --text="${SUCCESS_DONE} ${COUNTRIES_REQUEST}"	
#------------------------------------------------------------------
exit 0