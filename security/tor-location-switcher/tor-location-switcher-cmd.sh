#!/bin/sh
#set -x
cd "$(dirname ${0})"
SCRIPT_DIR="$(pwd)"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Tor Location Switcher
#@(#)--------------------------------------------------------------
#@(#) A simple tool to switch the location of Tor (no gui)
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0
#@(#) Version : 1.1 - add options for admin (root) + advanced options
#@(#) Version : 1.2 - fix bug on UID
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" | sed 's/\\n/$/g' | tr '$' '\n' >&2
}

print_text () {
# affiche un message sur la sortie standard
echo
echo "$@" | sed 's/\\n/$/g' | tr '$' '\n'
}

upd_param () {
PARAM="${1}"
VALUE="${2}"

HAS_PARAM=$(cat ${TORRC_CONF} | sed 's/#//g' | grep -n "^${PARAM} "  | cut -d':' -f1)
[ ${HAS_PARAM:-0} -gt 0 ] && {
NB_LINES=$(cat ${TORRC_CONF} | wc -l)
FIRST_PART=$(expr ${HAS_PARAM} - 1)
[ ${HAS_PARAM} -eq ${NB_LINES:-0} ] && LAST_PART=0
[ ${HAS_PARAM} -lt ${NB_LINES:-0} ] && LAST_PART=$(expr ${NB_LINES} - ${HAS_PARAM})
cat ${TORRC_CONF} | head -${FIRST_PART} > ${TORRC_CONF}.$$
echo "${PARAM} ${VALUE}" >> ${TORRC_CONF}.$$
[ ${LAST_PART} -gt 0 ] && cat ${TORRC_CONF} | tail -${LAST_PART} >> ${TORRC_CONF}.$$
}

[ ${HAS_PARAM:-0} -eq 0 ] && {
cat ${TORRC_CONF} > ${TORRC_CONF}.$$
echo "${PARAM} ${VALUE}" >> ${TORRC_CONF}.$$
}

# Application de la configuration
mv ${TORRC_CONF}.$$ ${TORRC_CONF}

}

is_integer () {
# teste si le paramètre est un entier
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
P_UID=$(id -u)
GUI_LANG=$(echo ${LANG} | head -1 | cut -c1-2)
STRICT_EXIT_NODES="0"
ISO_3611="${SCRIPT_DIR}/ISO-3611.txt"
CONFIG_DIR="${HOME}/.config/tor-location-switcher"
NEW_CONFIGURATION="true"
TORRC_CONF="/etc/tor/torrc"
ENODE_CONF="${CONFIG_DIR}/enode.conf"
APP_CONF="${CONFIG_DIR}/app.conf"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Chargement de la langue
[ ! -f ${SCRIPT_DIR}/${GUI_LANG:-en}.lang ] && GUI_LANG="en"
. ${SCRIPT_DIR}/${GUI_LANG:-en}.lang

#------------------------------------------------------------------
mkdir -p "${CONFIG_DIR}"
[ -f ${APP_CONF} ] && . ${APP_CONF}

touch ${ENODE_CONF}

# Vérifier si Tor est en cours de fonctionnement
IS_RUNNING=$(ps -aux | grep torrc | grep -v "grep" | wc -l)
[ ${IS_RUNNING:-0} -gt 0 -a ${P_UID:--1} -ne 0 ] && print_error "${ERROR_ALREADY_RUNNING}" && exit 1

[ ${IS_RUNNING:-0} -gt 0 -a ${P_UID:--1} -eq 0 ] && {

echo "${Q_STOP_SERVICE} ${Q_CMD_YES_NO}"; read A_STOP_SERVICE

[ "${A_STOP_SERVICE:-none}" = "${A_CMD_YES_NO}" ] && service tor stop
[ "${A_STOP_SERVICE:-none}" != "${A_CMD_YES_NO}" ] && print_error "${ERROR_ALREADY_RUNNING}" && exit 0

}

#------------------------------------------------------------------
# Vérifier si le répertoire de torrc est trouvé

while [ ! -f "${TORRC_CONF}" ]
do

echo "${TITLE_CONFIG_FILE} ?"; read TORRC_CONF

#Enregistrement du chemin de configuration
echo "TORRC_CONF=${TORRC_CONF}" > "${APP_CONF}"

if [ ! -f "${TORRC_CONF}" ] 
then
print_error "${ERROR_NO_FILE_FOUND}"
echo "${Q_QUIT_CONF} ${Q_CMD_YES_NO}"; read A_QUIT_CONF
[ "${A_QUIT_CONF:-none}" = "${A_CMD_YES_NO}" ] && END_OF_CONF="true"
[ "${A_QUIT_CONF:-none}" != "${A_CMD_YES_NO}" ] && END_OF_CONF="false"

[ "${END_OF_CONF:-false}" = "true" ] && exit 0
fi
	
done

[ ! -f "${TORRC_CONF}" ] && print_error "${ERROR_NO_TORRC_DEFINED}" && sleep 3 && exit 1

TORRC_DIR="$(dirname ${TORRC_CONF})"
#------------------------------------------------------------------
# Récupération des configurations
NB_CONF=$(cat ${ENODE_CONF} | wc -l)
CURRENT_CONF=$( expr ${NB_CONF:-0} + 1 )
if [ ${NB_CONF:-0} -gt 0 ]
then
NEW_CONFIGURATION="false"

echo
echo "${TITLE_CONFIG_CHOOSE}"
echo "0 : ${CHOICE_CANCEL}"
cat ${ENODE_CONF} | tr ';' ' ' | sort -n -k 1 | awk '{print $1 " : " $3}'
echo "${CURRENT_CONF} : ${CHOICE_NEW_CONF}"
read CHOOSE_CONFIGURATION

[ ${CHOOSE_CONFIGURATION:-0} -lt 0 -o ${CHOOSE_CONFIGURATION:-0} -gt ${CURRENT_CONF} ] && print_error "${ERROR_INVALID_CHOICE}" && sleep 3 && exit 1

[ ${CHOOSE_CONFIGURATION:-0} -eq 0 ] && print_error "${WARNING_CANCEL}" && sleep 3 && exit 0
[ ${CHOOSE_CONFIGURATION:-0} -eq ${CURRENT_CONF} ] && NEW_CONFIGURATION="true"
[ ${CHOOSE_CONFIGURATION:-0} -gt 0 -a  ${CHOOSE_CONFIGURATION:-0} -le ${NB_CONF} ] && COUNTRIES_VALID="$(cat ${ENODE_CONF} | awk -F';' -v configuration=${CHOOSE_CONFIGURATION} '($1 ==configuration) {print $3}')"
CURRENT_CONF=${CHOOSE_CONFIGURATION}
fi

#------------------------------------------------------------------
# Nouvelle configuration
if [ "${NEW_CONFIGURATION:-false}" = "true" ]
then
# Choix du pays

COUNTRIES_VALID=""
END_OF_ENTRIES="false"
SEP=""
NB_COUNTRIES=0

while [ "${END_OF_ENTRIES}" = "false" ]
do

# Liste des pays ?
echo "${Q_COUNTRY_CODE}"; read A_COUNTRY_CODE

# En majuscules pour interroger la DB
COUNTRY_CODE_MAJ="$(echo ${A_COUNTRY_CODE} | tr '[a-z]' '[A-Z]')"

COUNTRY="$(cat "${ISO_3611}" | awk -F';' -v country=${COUNTRY_CODE_MAJ} '($2 == country) {print $1}')"

echo "==> ${COUNTRY}"
echo "${Q_COUNTRY_CONFIRM} ${Q_CMD_YES_NO}"; read A_COUNTRY_CONFIRM
if [ "${A_COUNTRY_CONFIRM:-none}" = "${A_CMD_YES_NO}" ]
then
COUNTRIES_VALID="${COUNTRIES_VALID}${SEP}${COUNTRY_CODE_MAJ}"
NB_COUNTRIES=$(expr ${NB_COUNTRIES} + 1)
[ ${NB_COUNTRIES:-0} -gt 0 ] && SEP=","
fi

echo "${Q_ONE_COUNTRY_MORE} ${Q_CMD_YES_NO}"; read A_ONE_COUNTRY_MORE
[ "${A_ONE_COUNTRY_MORE:-none}" = "${A_CMD_YES_NO}" ] && END_OF_ENTRIES="false"
[ "${A_ONE_COUNTRY_MORE:-none}" != "${A_CMD_YES_NO}" ] && END_OF_ENTRIES="true"

done

fi

# Pas de pays défini --> erreur
[ "${COUNTRIES_VALID:-none}" = "none" ] && print_error "${ERROR_NO_COUNTRY_DEFINED}" && sleep 3 && exit 1

# Enregistrement de la configuration
# Sauvegarde de la configuration --> on désactive les autres config
cat "${ENODE_CONF}" | awk -F';' -v configuration="${CURRENT_CONF}" '($1 != configuration) {print $1 ";FALSE;" $3}' > ${ENODE_CONF}.$$
echo "${CURRENT_CONF};TRUE;${COUNTRIES_VALID}" >> ${ENODE_CONF}.$$
mv ${ENODE_CONF}.$$ ${ENODE_CONF}

#------------------------------------------------------------------
# Application de la configuration dans torrc

# En minuscules pour le torrc
COUNTRIES_LIST="$(echo ${COUNTRIES_VALID} | tr '[A-Z]' '[a-z]' | sed 's/,/},{/g' | sed 's/{},//g')"
COUNTRIES_CMD="ExitNodes {${COUNTRIES_LIST}} StrictNodes ${STRICT_EXIT_NODES}"

# Enregistrement de l'ancienne configuration
[ -f ${TORRC_CONF} ] && cp ${TORRC_CONF} ${TORRC_CONF}.previous
[ -f ${TORRC_CONF} ] && cp ${TORRC_CONF} ${TORRC_CONF}.$(date -u +'%Y%m%d_%H%M%S')

# Remplacement de la configuration dans le fichier
HAS_ENODE=$(grep -n 'ExitNodes ' ${TORRC_CONF} | cut -d':' -f1)
[ ${HAS_ENODE:-0} -gt 0 ] && {
NB_LINES=$(cat ${TORRC_CONF} | wc -l)
FIRST_PART=$(expr ${HAS_ENODE} - 1)
[ ${HAS_ENODE} -eq ${NB_LINES:-0} ] && LAST_PART=0
[ ${HAS_ENODE} -lt ${NB_LINES:-0} ] && LAST_PART=$(expr ${NB_LINES} - ${HAS_ENODE} - 1)
cat ${TORRC_CONF} | head -${FIRST_PART} > ${TORRC_CONF}.$$
echo ${COUNTRIES_CMD} >> ${TORRC_CONF}.$$
[ ${LAST_PART} -gt 0 ] && cat ${TORRC_CONF} | tail -${LAST_PART} > ${TORRC_CONF}.$$
}

[ ${HAS_ENODE:-0} -eq 0 ] && {
cat ${TORRC_CONF} > ${TORRC_CONF}.$$
echo "${COUNTRIES_CMD}" >> ${TORRC_CONF}.$$
}

# Application de la configuration
mv ${TORRC_CONF}.$$ ${TORRC_CONF}
#------------------------------------------------------------------
# Configuration avancée

print_text "${SUCCESS_CONFIG_DONE} ${Q_CMD_YES_NO}/${Q_CMD_ADVANCED}"; read CONFIG_COUNTRY_OK

[ "${CONFIG_COUNTRY_OK:-none}" = "${A_CMD_ADVANCED}" ] && {

BWRATE_VALUE_CURRENT=$( cat ${TORRC_CONF} | sed 's/#//g' | grep '^RelayBandwidthRate ' | awk '{print $2}' )
BWRATE_UNIT_CURRENT=$( cat ${TORRC_CONF} | sed 's/#//g' | grep '^RelayBandwidthRate ' | awk '{print $3}' )
# Conversion in KB
[ "${BWRATE_UNIT_CURRENT:-KB}" = "MB" ] && BWRATE_VALUE_CURRENT=$( expr ${BWRATE_VALUE_CURRENT:-200} \* 1000 )
BWRATE_UNIT_CURRENT="KB"

print_text "${TITLE_CONFIG_BWRATE} (${BWRATE_VALUE_CURRENT})"; read BWRATE_VALUE
[ "x${BWRATE_VALUE}x" = "xx" ] && BWRATE_VALUE=${BWRATE_VALUE_CURRENT}
IS_INT=$( is_integer "${BWRATE_VALUE}" )
[ ${IS_INT:-1} -ne 0 ] && print_error "${ERROR_NOT_INTEGER}" && sleep 3 && exit 1
BWBURST_VALUE=$( expr ${BWRATE_VALUE} \* 15  \/  10 )

#RelayBandwidthRate 100 KB  # Throttle traffic to 100KB/s (800Kbps)
upd_param "RelayBandwidthRate" "${BWRATE_VALUE:-200} KB"
#RelayBandwidthBurst 200 KB # But allow bursts up to 200KB/s (1600Kbps)
upd_param "RelayBandwidthBurst" "${BWBURST_VALUE:-300} KB"

}
#------------------------------------------------------------------
# Nettoyage de l'historique
NB_HISTO=$(ls -1 ${TORRC_DIR}/torrc.*_* | wc -l)
[ ${NB_HISTO:-0} -gt 10 ] && {
NB_DELETE=$(expr ${NB_HISTO} - 10)
ls -1a ${TORRC_DIR}/torrc.*_* | head -${NB_DELETE} | xargs rm -f
}


[ ${P_UID:--1} -eq 0 ] && {

echo "${Q_START_SERVICE} ${Q_CMD_YES_NO}"; read A_START_SERVICE
[ "${A_START_SERVICE:-none}" = "${A_CMD_YES_NO}" ] && service tor start

}


print_text "${SUCCESS_CONFIG_DONE}"
# Wait 3 seconds for printing the validation
sleep 3
#------------------------------------------------------------------
exit 0