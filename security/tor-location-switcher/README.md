# Tor Location Switcher : Use Tor as VPN

Tool for GNU+Linux to manage the location of Tor as you could do with a VPN service provider.
**tor-location-switcher** manages the location of Tor. It changes automatically the *torrc* (configuration) file through a simple interface, without open and change it manually.

It provides 2 tools :

- graphical tool (Zenity) for those who use a computer with Zenity already installed: *tor-location-switcher-gui.sh*;
- command line for those who use a computer without Zenity already installed: *tor-location-switcher-cmd.sh*.


## Installation

1- Download the application (scripts + languages + icons).

2- Change the permission to allow executing *tor-location-switcher-gui.sh*/*tor-location-switcher-cmd.sh* (if not set already). 


### With *Tor Browser Bundle*

1- Download the **Tor Browser Bundle** from : https://www.torproject.org/download/

2- Uncompress the package where you want to.

3- Return to **tor-location-switcher** and launch *tor-location-switcher-gui.sh*/*tor-location-switcher-cmd.sh* from its installation directory.

4- Set the *torrc* configuration to use it. It should be found in the **Tor Browser Bundle** at {*Tor Browser Bundle* directory}/Browser/TorBrowser/Data/Tor/torrc.

5- Follow the configuration's GUI. Set the countries you want and apply.

6- Go to the **Tor Browser Bundle** directory (where you uncompressed it) and right click on *start-tor-browser.desktop*, open *Properties* or *Preferences* and change the permission to allow executing file as program. Double-click the icon to start up *Tor Browser* for the first time.
**Note** : The installation guide for **Tor Browser Bundle** is available here : https://tb-manual.torproject.org/installation/

7- Open a search tab, find a *DNS lookup* on the Internet and check your location.


## Usage

1- Stop *Tor* (i.e **Tor Browser**) if it runs already.

2- Execute **tor-location-switcher** (i.e. *tor-location-switcher-gui.sh* or *tor-location-switcher-cmd.sh*).

The program :

- checks if the *torrc* is found at the standard location. If not, it asks where to find it.

- asks which countries you want to use. You can select multiple locations.

**Note** : If you defined configurations already, **tor-location-switcher** proposes to select a previous one. If you do want a new selection, cancel this step to reach the manual selection of countries.

3- Once you have selected the countries you want, it writes down the configuration. After the confirmation of success, you can relaunch *Tor* (i.e **Tor Browser**).


## Multi-language management

The *xx.lang* file provides a translation for GUI.

If the file does not exist, feel free to copy+modify an existing one.

Copy and rename the *en.lang* file as *xx.lang* to support your language *xx* (named accordingly ISO 3611).


## Notes

**tor-location-switcher-gui.sh** tested on :
- **Ubuntu** 21.04, 21.10
- **Pop!_OS** 21.04

**tor-location-switcher-cmd.sh** tested on :
- **Ubuntu** 21.04, 21.10


## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")