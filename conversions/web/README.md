# Conversions

## Descriptions

### CleanContent.sed

First level of HTML cleansing. Converts the punctuation encoded to standard punctuation.

For example, "%3B" becomes ";".

### ent2html.sed

When the text is sent by the HTML form, the HTML tags are encoded into URL.

Once the first level (punctuation) is done, and HTML encoding is converted to XML entities, this level restores the standard notation of HTML/XML format.

For example, the XML entity "&amp;#62;" becomes ">".

### text2url.sed

Converts a string into HTML/URL notation.

For example, "=" becomes "%3D".

### w3centities2xmlentities.sed

Converts HTML entities to XML entities.

For example, "&amp;Aacute;" becomes "&amp;#x000C1;".

## Usage

## w3centities2xmlentities.sed

For conversion, you have to deactivate the  "&amp;#" pattern before, and reactivate it after with this trick:

    entities2xml () {
    cat - | sed 's/@/\&#64;/g' | sed 's/\&#/@/g' | sed -f w3centities2xmlentities.sed | sed 's/@/\&#/g' | sed 's/\&#64;/@/g'
    }

## Minify CSS

Script to minify CSS using yuicompressor.

### Configuration

    ./minify-css.sh -j "/path/to/java" -c "/path/to/yuicompressor.jar"

saves the configuration for later executions.

### Compression

    ./minify-css.sh -i "/path/to/my_file.css"

minifies the CSS file in the same directory, with the name my_file.*minify*.css.

    ./minify-css.sh -i "path/to/my_file.css" -o "/path/to/directory"

minifies the CSS file in the directory given by -o, with the name my_file.*minify*.css.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
