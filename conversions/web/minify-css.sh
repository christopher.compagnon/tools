#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) minify-css : Compress a css file
#@(#)--------------------------------------------------------------
#@(#) Minify a CSS file with yuicompressor
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -j filename ] [ -c filename ] [ -i filename ] [ -o directory ] [ -h ]"
echo "-i (input) : path to file to be minified. Required if output."
echo "-o (output) : directory of the css minified. If no directory, the same as input."
echo "-j (java) : path to java exec."
echo "-c (compressor) : path to jar to be executed (yuicompressor)."
echo "-h (help) : display help."
}

WriteConfig () {
# Write configuration file
echo "# $(date '+%c')" > ${CONFIG_FILE}
echo "JAVA_EXEC=\"${JAVA_EXEC}\"" >> ${CONFIG_FILE}
echo "CSS_PROC=\"${CSS_PROC}\"" >> ${CONFIG_FILE}
echo "Configuration saved." >&2
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
JAVA_EXEC="$(whereis java | awk '{print $2}')"
CONFIG_FILE="${HOME}/.tools/minify-css.conf"
mkdir -p "$(dirname ${CONFIG_FILE})"
[ ! -f "${CONFIG_FILE}" ] && WriteConfig
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":i:o:j:c:h" option 
do 
case ${option} in 

i)
# input
FILE="${OPTARG}"
HAS_INPUT="true"
FILE_NAME="$(basename ${FILE})"
ROOT_FILE="${FILE_NAME%.*}"
EXT_FILE="${FILE_NAME##*.}"
;;

o)
# output
DIRECTORY="${OPTARG}"
HAS_OUTPUT="true"
;;

j)
# java
JAVA_EXEC="${OPTARG}"
HAS_JAVA="true"
;;

c)
# compressor
CSS_PROC="${OPTARG}"
HAS_CSS_PROCESSOR="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
echo "Invalid option ${OPTARG}!"
usage
exit 1
;;

esac
done
#------------------------------------------------------------------
# Contrôles
[ "${HAS_INPUT:-false}" = "true" -a "${HAS_OUTPUT:-false}" != "true" ] && DIRECTORY="$(dirname ${FILE})"
[ "${HAS_INPUT:-false}" != "true" -a "${HAS_OUTPUT:-false}" = "true" ] && print_error "Option -i is missing !" && exit 1
[ "${HAS_JAVA:-false}" = "true" -a ! -f "${JAVA_EXEC}" ] && print_error "${JAVA_EXEC} not found !" && exit 1
[ "${HAS_CSS_PROCESSOR:-false}" = "true" -a ! -f "${CSS_PROC}" ] && print_error "${CSS_PROC} not found !" && exit 1
[ "${HAS_JAVA:-false}" = "true" ] && {
# Test of java
TEST_FILE="/tmp/$$.test"
${JAVA_EXEC} -version 2> ${TEST_FILE}
#./test.java.sh 2>&1 test.java.err 1> test.java.log
TEST_CONFIG=$(cat ${TEST_FILE} | grep "version" | wc -l)
[ -f ${TEST_FILE} ] && rm -f ${TEST_FILE}
[ ${TEST_CONFIG:-0} -eq 0 ] && print_error "${JAVA_EXEC} not executable !" && exit 1
}
#------------------------------------------------------------------
# Write the configuration
[ "${HAS_JAVA:-false}" = "true" -o "${HAS_CSS_PROCESSOR:-false}" = "true" ] && WriteConfig

# Load configuration
. ${CONFIG_FILE}
#------------------------------------------------------------------
# Compress the file
[ "${HAS_INPUT:-false}" = "true" ] && {
cd "${SCRIPT_DIR}"

# Check if config is OK
[ ! -f "${JAVA_EXEC}" ] && print_error "Java not found !" && exit 1
[ ! -f "${CSS_PROC}" ] && print_error "Compressor not found !" && exit 1

mkdir -p "${DIRECTORY}"
CSS_FILE="${ROOT_FILE}.minify.${EXT_FILE}"

${JAVA_EXEC} -jar ${CSS_PROC} --type css --charset utf-8 -o "${DIRECTORY}/${CSS_FILE}" "${FILE}"
RETURN=${?}
[ ${RETURN:-0} -ne 0 ] && print_error "Error during the compression" && exit 1
print_error "File suseccessfully minified"
}
#------------------------------------------------------------------
exit 0