#!/bin/sh
# Conversion de l'encodage HTML en encodage UTF-8.
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
DATA_TMP="/tmp"
FILTER_AWK=html2utf8.awk
FILTER=html2utf8.sed
FILTER_TMP=${DATA_TMP}/html2utf8.$$.tmp
QUERY_FILE=${DATA_TMP}/QUERY_STRING.$$
file0=${DATA_TMP}/file0.$$
file1=${DATA_TMP}/file1.$$
file2=${DATA_TMP}/file2.$$
file3=${DATA_TMP}/file3.$$
file4=${DATA_TMP}/file4.$$
file5=${DATA_TMP}/file5.$$
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
mkdir -p "${DATA_TMP}"
cat - > ${QUERY_FILE}
#Si le fichier n'existe pas, on le crée
[ ! -f ${FILTER} ] && touch ${FILTER}

cat ${QUERY_FILE} | sed 's/+/ /g' | sed -f ${FILTER} > ${file0}

cat ${QUERY_FILE} |  sed 's/\%\([0-9A-F][0-9A-F]\)/\$\%\1\$/g'  | tr '$' '\n' | grep "^%" | sed 's/^\%//g' | awk '{print NR ";" $0}' > ${file1}

cat ${file1} |  awk -F";" 'BEGIN { print "ibase=16;obase=2"} {print $2}'| bc | awk '{print "00000000" $1}' | awk '{print NR ";" substr($1,length($1)-7,8)}' > ${file2}

join -t";" -o 1.2,2.2 -j 1 ${file1} ${file2} | awk -F";" -f ${FILTER_AWK} | awk '{print NR ";" $0}' > ${file3}

cat ${file3} | awk -F";" 'BEGIN { print "ibase=2"} {print $3}'| bc | awk '{print NR ";" $0}' > ${file4}

join -t";" -o 1.2,2.2 -j 1 ${file3} ${file4} | sort | uniq | awk -F";" '{print "s/" $1 "/\\&#" $2 "\\;/g"}' > ${file5}

cat ${FILTER} > ${FILTER_TMP}
cat ${file5} >> ${FILTER_TMP}
cat ${FILTER_TMP} | sort | uniq > ${FILTER}

cat ${file0} | sed -f ${FILTER_TMP}
#------------------------------------------------------------------
rm -f ${file0}
rm -f ${file1}
rm -f ${file2}
rm -f ${file3}
rm -f ${file4}
rm -f ${file5}
rm -f ${QUERY_FILE}
rm -f ${FILTER_TMP}
#------------------------------------------------------------------
exit 0
