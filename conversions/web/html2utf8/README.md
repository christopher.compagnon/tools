# html2utf8

Script to convert HTML data from form to XML/UTF8 entities.

## Dependencies

- bc

## Installation

Copy the files into the directory you want.

## Usage

Pipe the script for conversion:

    echo "My%20string" | ./html2utf8.sh

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

