# Conversion des caract�res HTML en utf-8
# entr�es sur 2 colonnes, la premi�re le motif d'origine, le second sa conversion en binaire
BEGIN {}

(substr($2,1,4)==1110) {
# cas o� le caract�re est cod� sur 3
n=3
m=substr($2,5,4) 
p="\\%" $1 }

(substr($2,1,3)==110) { 
# cas o� le caract�re est cod� sur 2
n=2
m=m substr($2,4,5)
p=p "\\%" $1}

(substr($2,1,2)==10) { 
# cas d'une suite 
n=n-1
m=m substr($2,3,6)
p=p "\\%" $1
if (n==1) {
n=0
print p ";" m
p=""
m=""
}}

(substr($2,1,1)==0) { 
# cas o� le caract�re est cod� sur 1 
print "\\%" $1 ";" $2
}

END {}