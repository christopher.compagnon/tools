#!/bin/sh
# Gestion d'un diaporama
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
cd "${SCRIPT_DIR}"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Pipote-a-lot : Pipotron
#@(#)--------------------------------------------------------------
#@(#) Choisit aléatoirement une phrase sans aucun sens mais qui fait intelligent (ou presque) !
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#@(#) Version : 1.0 - original
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage: ${SCRIPT_NAME} [-h]"
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
i=1
NB_FILES=$(ls -l ./sentences_*.txt | wc -l)
RESULT=""
#------------------------------------------------------------------
# Paramètres
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

while [ ${i} -le ${NB_FILES} ]
do
RESULT="${RESULT} $(cat sentences_${i}.txt | shuf -n1)"
i=$(expr ${i} + 1)
done

# Conversion des apostrophes
RESULT="$(echo ${RESULT} | sed "s/% %/'/g" | sed "s/% /e /g")"
# Formattage du résultat
RESULT="$(echo ${RESULT} | xargs -0)"

echo "${RESULT}."
#------------------------------------------------------------------
exit 0