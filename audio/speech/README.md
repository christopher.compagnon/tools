# speech

Read a text more naturally from a speech file.

## Dependencies

- tts.sh (available in *tools*)

Download/copy *tts.sh* in the same directory.


## Examples

    ./speech.sh my_file.speech

Read the file *my_file.speech*.

Some speech examples are available in *tests* directory.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")