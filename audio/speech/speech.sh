#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) speech : Read a speech formatted text
#@(#)--------------------------------------------------------------
#@(#) Read a speech formatted text using the tts of Google.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} file"
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
SPEECH_FILE="${1}"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Contrôle
[ ! -f ${SPEECH_FILE} ] && print_error "${SPEECH_FILE} does not exists" && exit 1

# Récupération de la langue
LANG=$(cat ${SPEECH_FILE} | grep !LANG= | cut -d"=" -f2)
[ "${LANG:-none}" = "none" ] && print_error "Language not found. The speech file is malformed." && exit 1 

clear

cat ${SPEECH_FILE} | grep -v '#' | grep '|' | while read line
do
PAUSE=$(echo ${line} | awk -F'|' '{print $1}')
TEXT=$(echo ${line} | awk -F'|' '{print $2}')
SPEECH=$(echo ${TEXT} | sed 's/ /+/g')

echo ${TEXT}
[ "${TEXT}" != "" ] && ./tts.sh -l ${LANG} -t "${SPEECH}"

sleep ${PAUSE:-0.200}

done

#------------------------------------------------------------------
exit 0
