#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) srt2mp3 : Convert .srt file to mp3
#@(#)------------------------------------------------------------------
#@(#) Convert a subtitle from a .srt file to mp3 using the tts of Google.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} [ -l language ] [ -f file ] [ -h ]"
echo "-l (language) : language of the subtitles (en, fr, de, …)"
echo "-f (file) : .srt file to convert"
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Gestion des options
while getopts ":l:f:h" option 
do 
case ${option} in 

l)
LANG=${OPTARG}
HAS_LANG="true"
;; 

f)
SRT_FILE=${OPTARG}
HAS_FILE="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
usage
;;

esac
done 
#------------------------------------------------------------------
[ "${HAS_LANG:-false}" != "true" ] && print_error "Option -l is missing !" && exit 1
[ "${HAS_FILE:-false}" != "true" ] && print_error "Option -f is missing !" && exit 1

[ ! -f "${SRT_FILE}" ] && print_error "${SRT_FILE} not found !" && exit 1

# Préparation du fichier
TGT_DIR="$(dirname ${SRT_FILE})"
TEMP_FILE1="${SRT_FILE}.$$"
cat "${SRT_FILE}" | sed "s/^\r/\$\$/g" | tr '\r' '#' | tr -d '\n' | tr '$$' '\n' | cut -d '#' -f3- | grep '#' | sed 's/#//g' > "${TEMP_FILE1}"

# Conversion en MP3
cat "${TEMP_FILE1}" | while read line
do
echo "Converting $(echo ${line})…"
TEXT="$(echo ${line} | sed 's/ /+/g')"
AUDIO_FILE="${TGT_DIR}/$(echo ${line} | tr -d '.' | tr '[:punct:]' '_' | tr '[:blank:]' '_').mp3"

wget -q -U Mozilla -O "${AUDIO_FILE}" "http://translate.google.com/translate_tts?ie=UTF-8&total=1&client=tw-ob&tl=${LANG:-fr}&q=${TEXT}"
[ ! -e "${AUDIO_FILE}" ] && print_error "The extraction of the mp3 has failed !" && exit 1

done

#------------------------------------------------------------------
# Nettoyage
[ -f "${TEMP_FILE1}" ] && rm -f "${TEMP_FILE1}"
#------------------------------------------------------------------
exit 0
