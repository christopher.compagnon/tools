# srt2mp3

Conversion of .srt files to .mp3 with Google tts.

Very useful in video production workflow.

## Examples

    ./srt2mp3.sh -l en -f /path/to/my_file.srt

Produces one mp3 file by line of subtitle, named according to the text.

See/use *tests/test.fr.srt* as example.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

