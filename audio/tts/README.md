# tts

Generate a mp3 from text using Google tts and play the mp3.

## Dependencies

- mpg321

## Installation

### As webservice

Install your favorite web server. For example Apache:

    apt install apache2

Activate the module cgid

    a2enmod cgid

Create or modify the Apache configuration file. Use *tts.conf* as example.

Activate the new site:

    a2ensite tts

Restart the web server:

    systemctl restart apache2

Copy *ws.tts.sh* and *tts.sh* in the directory defined in the Apache configuration file.

Set the execution rights to the scripts:

    chmod +x *.sh

### As local script

Simply use *tts.sh* as command line.

## Usage

### As webservice : from the web browser

You can connect a server (i.e. Raspberry Pi) to speakers and install **tts** as webservice to play messages from network.

![tts as webservice](ws.tts.png "tts as webservice")

From your browser, call the webservice:

http://your_webserver/path/ws.tts.sh?lang=en&text=Hello world!

### As webservice : from the client

You can use a client to call the webservice from command line.

You have an example in the *client* directory.

- tts_client.sh : client itself.
- call_client.sh : example of usage.

This client can be used for warnings about something wrong on one device on network.

### As local script

    ./tts.sh -l en -t "Hello world!"


## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

