#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) tts_client : client for ws.tts
#@(#)--------------------------------------------------------------
#@(#) Send some text to tts webservice
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} [ -l language ] [ -t string ] [ -h ]"
echo "-l (language) : language."
echo "-t (text) : text to read."
echo "-u (URL) : URL of the web server where the webservice ws.tts.sh is installed."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
#REQUEST=$(echo ${1} | sed 's/ /+/g')
#CONF_FILE="client.conf"
REQUEST_ANSWER="client.txt"
LOG_FILE="client.log"
APP_LOG_DIR="."
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Load config
#. "${CONF_FILE}"

#------------------------------------------------------------------
# Gestion des options
while getopts ":l:t:u:h" option 
do 
case ${option} in 

l)
LANG=${OPTARG}
HAS_LANG="true"
;; 

t)
TEXT=$(echo ${OPTARG} | sed 's/ /+/g')
HAS_TEXT="true"
;;

u)
# URL of the webserver
SERVER_URL=${OPTARG}
HAS_URL="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
usage
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_LANG:-false}" != "true" ] && print_error "Option -l is missing !" && exit 1
[ "${HAS_TEXT:-false}" != "true" ] && print_error "Option -t is missing !" && exit 1
[ "${HAS_URL:-false}" != "true" ] && print_error "Option -u is missing !" && exit 1
#------------------------------------------------------------------
# Call webservice
wget -a ${LOG_FILE}  -P "${APP_LOG_DIR}/" -O ${REQUEST_ANSWER} --no-check-certificate "${SERVER_URL}/ws.tts.sh?lang=${LANG:-fr}&text=${TEXT}"

[ -f ${LOG_FILE} ] && cat ${LOG_FILE}
#------------------------------------------------------------------
# Nettoyage
[ -f ${REQUEST_ANSWER} ] && rm -f ${REQUEST_ANSWER}
[ -f ${LOG_FILE} ] && rm -f ${LOG_FILE}
#------------------------------------------------------------------
exit 0