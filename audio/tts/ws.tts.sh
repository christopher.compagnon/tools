#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) ws.tts : webservices for tts
#@(#)--------------------------------------------------------------
#@(#) Webservice calling tts.
#@(#) Call webservice with GET method, like ws.tts.sh?lang=en&text=Hello+world!
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
[ "${REQUEST_METHOD}" = "POST" ] && read QUERY_STRING

# Retrieve parameters from request
LANG=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^lang= | cut -d"=" -f2)
TEXT=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^text= | cut -d"=" -f2)
# [TODO] : retrieve lang from browser if LANG not defined

[ "${LANG:-none}" != "none" -a "${TEXT:-none}" != "none" ] && {
# Call tts
./tts.sh -t "${TEXT}" -l ${LANG}
}

#------------------------------------------------------------------
# En-tête fichier
echo "content-type: text/html; charset=utf-8"
echo
echo '<?xml version="1.0" encoding="utf-8"?>'
echo '<!DOCTYPE html>'
echo '<html xmlns="http://www.w3.org/1999/xhtml">'

echo '<head>'
echo "<title>tts</title>"
echo '<meta name="generator" content="tts"/>'
echo '<meta charset="utf-8"/>'
echo '</head>'

echo '<body>'

[ "${LANG:-none}" = "none" ] && {
echo "<h2>Parameter lang not defined</h2>"
echo "<p>tts cannot speak if <strong>lang</strong> is not defined.</p>"
echo "<p>Please, call ws.tts.sh as : <i>ws.tts.sh?<strong>lang=en</strong>&text=Hello+world!</i></p>"
}

[ "${TEXT:-none}" = "none" ] && {
echo "<h2>Parameter text not defined</h2>"
echo "<p>tts cannot speak if <strong>text</strong> is not defined.</p>"
echo "<p>Please, call ws.tts.sh as : <i>ws.tts.sh?lang=en&<strong>text=Hello+world!</strong></i></p>"
}

[ "${LANG:-none}" != "none" -a "${TEXT:-none}" != "none" ] && {
echo "<p>lang : ${LANG}</p>"
echo "<p>text : ${TEXT}</p>"
echo "<p><strong>OK<strong></p>"
}

echo '</body>'
echo '</html>'
#------------------------------------------------------------------
exit 0