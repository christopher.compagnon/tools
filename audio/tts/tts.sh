#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) tts : read some text
#@(#)--------------------------------------------------------------
#@(#) Read some text using the tts of Google.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} [ -l language ] [ -t string ] [ -s ] [ -h ]"
echo "-l (language) : language."
echo "-t (text) : text to read."
echo "-s (silent) : do not play the mp3, just create the voice file."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
#REQUEST=$(echo ${1} | sed 's/ /+/g')
AUDIO_DIR="./audio"
GENDER=male
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------

# Gestion des options
while getopts ":l:t:sh" option 
do 
case ${option} in 

l)
LANG=${OPTARG}
HAS_LANG="true"
;; 

t)
TEXT=$(echo ${OPTARG} | sed 's/ /+/g')
HAS_TEXT="true"
;;

s)
# silent
IS_SILENT="true"
;; 

h)
# usage
usage
exit 0
;;

\?) 
usage
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_LANG:-false}" != "true" ] && print_error "Option -l is missing !" && exit 1
[ "${HAS_TEXT:-false}" != "true" ] && print_error "Option -t is missing !" && exit 1

#------------------------------------------------------------------
# Création du répertoire
mkdir -p ${AUDIO_DIR}

# Récupération du texte
# Si le fichier n'existe pas, alors on utilise tts pour en créer un, sinon on utilise celui qu'on a déjà
TEXT_HASH=$(echo ${TEXT} | md5sum | awk '{print $1}')
AUDIO_FILE="${AUDIO_DIR}/${TEXT_HASH}.mp3"
[ ! -f "${AUDIO_FILE}" ] && wget -q -U Mozilla -O "${AUDIO_FILE}" "http://translate.google.com/translate_tts?ie=UTF-8&total=1&client=tw-ob&tl=${LANG:-fr}&q=${TEXT}"
[ "${IS_SILENT:-false}" != "true" ] && mpg321 -q "${AUDIO_FILE}" 2>/dev/null

#------------------------------------------------------------------
exit 0
