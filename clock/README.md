# Clocks and Timers


## term_clock

Display a digital clock in a terminal.

![Terminal clock](term_clock_example.png "Terminal clock")

## term_timer

A terminal timer.

### Usage

    term_timer -d 00:10:00 -c "echo done"

Set a timer at 10 minutes and display in terminal and "done" once at 0.

![Terminal timer](term_timer_example.png "Terminal timer")

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

