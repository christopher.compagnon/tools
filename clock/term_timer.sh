#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) timer : a simple timer
#@(#)--------------------------------------------------------------
#@(#) 
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${0} [ -d hh:mi:ss ] [ -c command ] [ -h ]"
echo "-d (duration) : duration of timer, format HH:MI:SS (max 99:59:59). Required."
echo "-c (command) : optional command to be executed once timer at 0."
echo "-h (help) : display help."
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
separator_1="    "
separator_2="  XX"
separator_3="    "
separator_4="  XX"
separator_5="    "
#------------------------------------------------------------------
unite_0_1="  XXXXXX"
unite_0_2="  XX  XX"
unite_0_3="  XX  XX"
unite_0_4="  XX  XX"
unite_0_5="  XXXXXX"
#------------------------------------------------------------------
unite_1_1="      XX"
unite_1_2="      XX"
unite_1_3="      XX"
unite_1_4="      XX"
unite_1_5="      XX"
#------------------------------------------------------------------
unite_2_1="  XXXXXX"
unite_2_2="      XX"
unite_2_3="  XXXXXX"
unite_2_4="  XX    "
unite_2_5="  XXXXXX"
#------------------------------------------------------------------
unite_3_1="  XXXXXX"
unite_3_2="      XX"
unite_3_3="  XXXXXX"
unite_3_4="      XX"
unite_3_5="  XXXXXX"
#------------------------------------------------------------------
unite_4_1="  XX  XX"
unite_4_2="  XX  XX"
unite_4_3="  XXXXXX"
unite_4_4="      XX"
unite_4_5="      XX"
#------------------------------------------------------------------
unite_5_1="  XXXXXX"
unite_5_2="  XX    "
unite_5_3="  XXXXXX"
unite_5_4="      XX"
unite_5_5="  XXXXXX"
#------------------------------------------------------------------
unite_6_1="  XXXXXX"
unite_6_2="  XX    "
unite_6_3="  XXXXXX"
unite_6_4="  XX  XX"
unite_6_5="  XXXXXX"
#------------------------------------------------------------------
unite_7_1="  XXXXXX"
unite_7_2="      XX"
unite_7_3="      XX"
unite_7_4="      XX"
unite_7_5="      XX"
#------------------------------------------------------------------
unite_8_1="  XXXXXX"
unite_8_2="  XX  XX"
unite_8_3="  XXXXXX"
unite_8_4="  XX  XX"
unite_8_5="  XXXXXX"
#------------------------------------------------------------------
unite_9_1="  XXXXXX"
unite_9_2="  XX  XX"
unite_9_3="  XXXXXX"
unite_9_4="      XX"
unite_9_5="  XXXXXX"

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Gestion des options
while getopts ":d:c:h" option 
do 
case ${option} in 

d)
# Duration
PARAM_DURATION="${OPTARG}"
HAS_DURATION="true"
;; 

c)
# Command
PARAM_COMMAND="${OPTARG}"
HAS_COMMAND="true"
;;

h)
# usage
usage
exit 0
;;

\?) 
usage
;;

esac
done 
#------------------------------------------------------------------
# Contrôles
[ "${HAS_DURATION:-false}" != "true" ] && print_error "Option -d is missing !" && exit 1

# Format du paramètre
[ $(echo ${PARAM_DURATION} | grep -e "[0-9][0-9]:[0-9][0-9]:[0-9][0-9]" | wc -l) -eq 0 ] && print_error "Wrong format of -d. Must be HH:MI:SS" && exit 1


# Conversion du paramètre en secondes
PARAM_HOUR=$(echo ${PARAM_DURATION} | cut -d ':' -f1)
PARAM_MIN=$(echo ${PARAM_DURATION} | cut -d ':' -f2)
PARAM_SEC=$(echo ${PARAM_DURATION} | cut -d ':' -f3)

DURATION_HOUR_S=$(expr ${PARAM_HOUR} \* 3600)
DURATION_MIN_S=$(expr ${PARAM_MIN} \* 60)
DURATION_SEC_S=$(echo ${PARAM_SEC} | cut -d ':' -f3)

DURATION=$(expr ${DURATION_HOUR_S:-0} + ${DURATION_MIN_S:-0} + ${DURATION_SEC_S:-0})
# Date en secondes
NOW=$(date '+%s')
END_TIME=$(expr ${NOW} + ${DURATION})

#------------------------------------------------------------------
# Compte à rebours
while [ ${NOW} -lt ${END_TIME} ]
do

DURATION=$(expr ${END_TIME} - ${NOW})
# Recalcul de la durée en HH:MI:SS
NB_HOUR=$(expr ${DURATION} / 3600)
RESTE_HOUR=$(expr ${DURATION} % 3600)
NB_MIN=$(expr ${RESTE_HOUR} / 60)
NB_SEC=$(expr ${RESTE_HOUR} % 60)
DISPLAY_HOUR=$(printf %02d ${NB_HOUR})
DISPLAY_MIN=$(printf %02d ${NB_MIN})
DISPLAY_SEC=$(printf %02d ${NB_SEC})

#------------------------------------------------------------------
hnow_d=$(echo ${DISPLAY_HOUR} | cut -c 1)
hnow_u=$(echo ${DISPLAY_HOUR} | cut -c 2)

mnow_d=$(echo ${DISPLAY_MIN} | cut -c 1)
mnow_u=$(echo ${DISPLAY_MIN} | cut -c 2)

snow_d=$(echo ${DISPLAY_SEC} | cut -c 1)
snow_u=$(echo ${DISPLAY_SEC} | cut -c 2)

#------------------------------------------------------------------
# Hour : 0x-9x

if [ ${hnow_d} -eq 0 ]
then
H1_1="${unite_0_1}"
H1_2="${unite_0_2}"
H1_3="${unite_0_3}"
H1_4="${unite_0_4}"
H1_5="${unite_0_5}"
fi

if [ ${hnow_d} -eq 1 ]
then
H1_1="${unite_1_1}"
H1_2="${unite_1_2}"
H1_3="${unite_1_3}"
H1_4="${unite_1_4}"
H1_5="${unite_1_5}"
fi

if [ ${hnow_d} -eq 2 ]
then
H1_1="${unite_2_1}"
H1_2="${unite_2_2}"
H1_3="${unite_2_3}"
H1_4="${unite_2_4}"
H1_5="${unite_2_5}"
fi

if [ ${hnow_d} -eq 3 ]
then
H1_1="${unite_3_1}"
H1_2="${unite_3_2}"
H1_3="${unite_3_3}"
H1_4="${unite_3_4}"
H1_5="${unite_3_5}"
fi

if [ ${hnow_d} -eq 4 ]
then
H1_1="${unite_4_1}"
H1_2="${unite_4_2}"
H1_3="${unite_4_3}"
H1_4="${unite_4_4}"
H1_5="${unite_4_5}"
fi

if [ ${hnow_d} -eq 5 ]
then
H1_1="${unite_5_1}"
H1_2="${unite_5_2}"
H1_3="${unite_5_3}"
H1_4="${unite_5_4}"
H1_5="${unite_5_5}"
fi

if [ ${hnow_d} -eq 6 ]
then
H1_1="${unite_6_1}"
H1_2="${unite_6_2}"
H1_3="${unite_6_3}"
H1_4="${unite_6_4}"
H1_5="${unite_6_5}"
fi

if [ ${hnow_d} -eq 7 ]
then
H1_1="${unite_7_1}"
H1_2="${unite_7_2}"
H1_3="${unite_7_3}"
H1_4="${unite_7_4}"
H1_5="${unite_7_5}"
fi

if [ ${hnow_d} -eq 8 ]
then
H1_1="${unite_8_1}"
H1_2="${unite_8_2}"
H1_3="${unite_8_3}"
H1_4="${unite_8_4}"
H1_5="${unite_8_5}"
fi

if [ ${hnow_d} -eq 9 ]
then
H1_1="${unite_9_1}"
H1_2="${unite_9_2}"
H1_3="${unite_9_3}"
H1_4="${unite_9_4}"
H1_5="${unite_9_5}"
fi

#------------------------------------------------------------------
# Hour : x0-x9

if [ ${hnow_u} -eq 0 ]
then
H2_1="${unite_0_1}"
H2_2="${unite_0_2}"
H2_3="${unite_0_3}"
H2_4="${unite_0_4}"
H2_5="${unite_0_5}"
fi

if [ ${hnow_u} -eq 1 ]
then
H2_1="${unite_1_1}"
H2_2="${unite_1_2}"
H2_3="${unite_1_3}"
H2_4="${unite_1_4}"
H2_5="${unite_1_5}"
fi

if [ ${hnow_u} -eq 2 ]
then
H2_1="${unite_2_1}"
H2_2="${unite_2_2}"
H2_3="${unite_2_3}"
H2_4="${unite_2_4}"
H2_5="${unite_2_5}"
fi

if [ ${hnow_u} -eq 3 ]
then
H2_1="${unite_3_1}"
H2_2="${unite_3_2}"
H2_3="${unite_3_3}"
H2_4="${unite_3_4}"
H2_5="${unite_3_5}"
fi

if [ ${hnow_u} -eq 4 ]
then
H2_1="${unite_4_1}"
H2_2="${unite_4_2}"
H2_3="${unite_4_3}"
H2_4="${unite_4_4}"
H2_5="${unite_4_5}"
fi

if [ ${hnow_u} -eq 5 ]
then
H2_1="${unite_5_1}"
H2_2="${unite_5_2}"
H2_3="${unite_5_3}"
H2_4="${unite_5_4}"
H2_5="${unite_5_5}"
fi

if [ ${hnow_u} -eq 6 ]
then
H2_1="${unite_6_1}"
H2_2="${unite_6_2}"
H2_3="${unite_6_3}"
H2_4="${unite_6_4}"
H2_5="${unite_6_5}"
fi

if [ ${hnow_u} -eq 7 ]
then
H2_1="${unite_7_1}"
H2_2="${unite_7_2}"
H2_3="${unite_7_3}"
H2_4="${unite_7_4}"
H2_5="${unite_7_5}"
fi

if [ ${hnow_u} -eq 8 ]
then
H2_1="${unite_8_1}"
H2_2="${unite_8_2}"
H2_3="${unite_8_3}"
H2_4="${unite_8_4}"
H2_5="${unite_8_5}"
fi

if [ ${hnow_u} -eq 9 ]
then
H2_1="${unite_9_1}"
H2_2="${unite_9_2}"
H2_3="${unite_9_3}"
H2_4="${unite_9_4}"
H2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
# Minute : 0x-5x

if [ ${hnow_d} -eq 0 ]
then
M1_1="${unite_0_1}"
M1_2="${unite_0_2}"
M1_3="${unite_0_3}"
M1_4="${unite_0_4}"
M1_5="${unite_0_5}"
fi

if [ ${hnow_d} -eq 1 ]
then
M1_1="${unite_1_1}"
M1_2="${unite_1_2}"
M1_3="${unite_1_3}"
M1_4="${unite_1_4}"
M1_5="${unite_1_5}"
fi

if [ ${hnow_d} -eq 2 ]
then
M1_1="${unite_2_1}"
M1_2="${unite_2_2}"
M1_3="${unite_2_3}"
M1_4="${unite_2_4}"
M1_5="${unite_2_5}"
fi

if [ ${hnow_d} -eq 3 ]
then
M1_1="${unite_3_1}"
M1_2="${unite_3_2}"
M1_3="${unite_3_3}"
M1_4="${unite_3_4}"
M1_5="${unite_3_5}"
fi

if [ ${hnow_d} -eq 4 ]
then
M1_1="${unite_4_1}"
M1_2="${unite_4_2}"
M1_3="${unite_4_3}"
M1_4="${unite_4_4}"
M1_5="${unite_4_5}"
fi

if [ ${hnow_d} -eq 5 ]
then
M1_1="${unite_5_1}"
M1_2="${unite_5_2}"
M1_3="${unite_5_3}"
M1_4="${unite_5_4}"
M1_5="${unite_5_5}"
fi
#------------------------------------------------------------------
# Minute : x0-x9

if [ ${mnow_u} -eq 0 ]
then
M2_1="${unite_0_1}"
M2_2="${unite_0_2}"
M2_3="${unite_0_3}"
M2_4="${unite_0_4}"
M2_5="${unite_0_5}"
fi

if [ ${mnow_u} -eq 1 ]
then
M2_1="${unite_1_1}"
M2_2="${unite_1_2}"
M2_3="${unite_1_3}"
M2_4="${unite_1_4}"
M2_5="${unite_1_5}"
fi

if [ ${mnow_u} -eq 2 ]
then
M2_1="${unite_2_1}"
M2_2="${unite_2_2}"
M2_3="${unite_2_3}"
M2_4="${unite_2_4}"
M2_5="${unite_2_5}"
fi

if [ ${mnow_u} -eq 3 ]
then
M2_1="${unite_3_1}"
M2_2="${unite_3_2}"
M2_3="${unite_3_3}"
M2_4="${unite_3_4}"
M2_5="${unite_3_5}"
fi

if [ ${mnow_u} -eq 4 ]
then
M2_1="${unite_4_1}"
M2_2="${unite_4_2}"
M2_3="${unite_4_3}"
M2_4="${unite_4_4}"
M2_5="${unite_4_5}"
fi

if [ ${mnow_u} -eq 5 ]
then
M2_1="${unite_5_1}"
M2_2="${unite_5_2}"
M2_3="${unite_5_3}"
M2_4="${unite_5_4}"
M2_5="${unite_5_5}"
fi

if [ ${mnow_u} -eq 6 ]
then
M2_1="${unite_6_1}"
M2_2="${unite_6_2}"
M2_3="${unite_6_3}"
M2_4="${unite_6_4}"
M2_5="${unite_6_5}"
fi

if [ ${mnow_u} -eq 7 ]
then
M2_1="${unite_7_1}"
M2_2="${unite_7_2}"
M2_3="${unite_7_3}"
M2_4="${unite_7_4}"
M2_5="${unite_7_5}"
fi

if [ ${mnow_u} -eq 8 ]
then
M2_1="${unite_8_1}"
M2_2="${unite_8_2}"
M2_3="${unite_8_3}"
M2_4="${unite_8_4}"
M2_5="${unite_8_5}"
fi

if [ ${mnow_u} -eq 9 ]
then
M2_1="${unite_9_1}"
M2_2="${unite_9_2}"
M2_3="${unite_9_3}"
M2_4="${unite_9_4}"
M2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
# Second : 0x-5x

if [ ${snow_d} -eq 0 ]
then
S1_1="${unite_0_1}"
S1_2="${unite_0_2}"
S1_3="${unite_0_3}"
S1_4="${unite_0_4}"
S1_5="${unite_0_5}"
fi

if [ ${snow_d} -eq 1 ]
then
S1_1="${unite_1_1}"
S1_2="${unite_1_2}"
S1_3="${unite_1_3}"
S1_4="${unite_1_4}"
S1_5="${unite_1_5}"
fi

if [ ${snow_d} -eq 2 ]
then
S1_1="${unite_2_1}"
S1_2="${unite_2_2}"
S1_3="${unite_2_3}"
S1_4="${unite_2_4}"
S1_5="${unite_2_5}"
fi

if [ ${snow_d} -eq 3 ]
then
S1_1="${unite_3_1}"
S1_2="${unite_3_2}"
S1_3="${unite_3_3}"
S1_4="${unite_3_4}"
S1_5="${unite_3_5}"
fi

if [ ${snow_d} -eq 4 ]
then
S1_1="${unite_4_1}"
S1_2="${unite_4_2}"
S1_3="${unite_4_3}"
S1_4="${unite_4_4}"
S1_5="${unite_4_5}"
fi

if [ ${snow_d} -eq 5 ]
then
S1_1="${unite_5_1}"
S1_2="${unite_5_2}"
S1_3="${unite_5_3}"
S1_4="${unite_5_4}"
S1_5="${unite_5_5}"
fi

#------------------------------------------------------------------
# Second : x0-x9

if [ ${snow_u} -eq 0 ]
then
S2_1="${unite_0_1}"
S2_2="${unite_0_2}"
S2_3="${unite_0_3}"
S2_4="${unite_0_4}"
S2_5="${unite_0_5}"
fi

if [ ${snow_u} -eq 1 ]
then
S2_1="${unite_1_1}"
S2_2="${unite_1_2}"
S2_3="${unite_1_3}"
S2_4="${unite_1_4}"
S2_5="${unite_1_5}"
fi

if [ ${snow_u} -eq 2 ]
then
S2_1="${unite_2_1}"
S2_2="${unite_2_2}"
S2_3="${unite_2_3}"
S2_4="${unite_2_4}"
S2_5="${unite_2_5}"
fi

if [ ${snow_u} -eq 3 ]
then
S2_1="${unite_3_1}"
S2_2="${unite_3_2}"
S2_3="${unite_3_3}"
S2_4="${unite_3_4}"
S2_5="${unite_3_5}"
fi

if [ ${snow_u} -eq 4 ]
then
S2_1="${unite_4_1}"
S2_2="${unite_4_2}"
S2_3="${unite_4_3}"
S2_4="${unite_4_4}"
S2_5="${unite_4_5}"
fi

if [ ${snow_u} -eq 5 ]
then
S2_1="${unite_5_1}"
S2_2="${unite_5_2}"
S2_3="${unite_5_3}"
S2_4="${unite_5_4}"
S2_5="${unite_5_5}"
fi

if [ ${snow_u} -eq 6 ]
then
S2_1="${unite_6_1}"
S2_2="${unite_6_2}"
S2_3="${unite_6_3}"
S2_4="${unite_6_4}"
S2_5="${unite_6_5}"
fi

if [ ${snow_u} -eq 7 ]
then
S2_1="${unite_7_1}"
S2_2="${unite_7_2}"
S2_3="${unite_7_3}"
S2_4="${unite_7_4}"
S2_5="${unite_7_5}"
fi

if [ ${snow_u} -eq 8 ]
then
S2_1="${unite_8_1}"
S2_2="${unite_8_2}"
S2_3="${unite_8_3}"
S2_4="${unite_8_4}"
S2_5="${unite_8_5}"
fi

if [ ${snow_u} -eq 9 ]
then
S2_1="${unite_9_1}"
S2_2="${unite_9_2}"
S2_3="${unite_9_3}"
S2_4="${unite_9_4}"
S2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
VAR_TIME_1="${H1_1:-0}${H2_1:-0}${separator_1}${M1_1:-0}${M2_1:-0}${separator_1}${S1_1:-0}${S2_1:-0}"
VAR_TIME_2="${H1_2:-0}${H2_2:-0}${separator_2}${M1_2:-0}${M2_2:-0}${separator_2}${S1_2:-0}${S2_2:-0}"
VAR_TIME_3="${H1_3:-0}${H2_3:-0}${separator_3}${M1_3:-0}${M2_3:-0}${separator_3}${S1_3:-0}${S2_3:-0}"
VAR_TIME_4="${H1_4:-0}${H2_4:-0}${separator_4}${M1_4:-0}${M2_4:-0}${separator_4}${S1_4:-0}${S2_4:-0}"
VAR_TIME_5="${H1_5:-0}${H2_5:-0}${separator_5}${M1_5:-0}${M2_5:-0}${separator_5}${S1_5:-0}${S2_5:-0}"
VAR_TIME="${VAR_TIME_1}\n${VAR_TIME_2}\n${VAR_TIME_3}\n${VAR_TIME_4}\n${VAR_TIME_5}"

#------------------------------------------------------------------
clear
#echo "${DISPLAY_HOUR}:${DISPLAY_MIN}:${DISPLAY_SEC}"
echo "${VAR_TIME}" | sed 's/X/█/g'

sleep 1
NOW=$(date '+%s')
done

clear

[ "${HAS_COMMAND:-false}" = "true" ] && {
# Execute command
${PARAM_COMMAND}
}
#------------------------------------------------------------------
# Déclenchement d'une action

#------------------------------------------------------------------
exit 0