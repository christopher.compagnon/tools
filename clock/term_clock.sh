#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) term_clock : Clock in terminal
#@(#)--------------------------------------------------------------
#@(#) Display a clock in the terminal
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

# Une unité fait 2x2
# Chaque chiffre fait 3x5 de surface
# L'espace entre chaque chiffre est de 1 unité
#------------------------------------------------------------------
separator_1="    "
separator_2="  XX"
separator_3="    "
separator_4="  XX"
separator_5="    "
#------------------------------------------------------------------
unite_0_1="  XXXXXX"
unite_0_2="  XX  XX"
unite_0_3="  XX  XX"
unite_0_4="  XX  XX"
unite_0_5="  XXXXXX"
#------------------------------------------------------------------
unite_1_1="      XX"
unite_1_2="      XX"
unite_1_3="      XX"
unite_1_4="      XX"
unite_1_5="      XX"
#------------------------------------------------------------------
unite_2_1="  XXXXXX"
unite_2_2="      XX"
unite_2_3="  XXXXXX"
unite_2_4="  XX    "
unite_2_5="  XXXXXX"
#------------------------------------------------------------------
unite_3_1="  XXXXXX"
unite_3_2="      XX"
unite_3_3="  XXXXXX"
unite_3_4="      XX"
unite_3_5="  XXXXXX"
#------------------------------------------------------------------
unite_4_1="  XX  XX"
unite_4_2="  XX  XX"
unite_4_3="  XXXXXX"
unite_4_4="      XX"
unite_4_5="      XX"
#------------------------------------------------------------------
unite_5_1="  XXXXXX"
unite_5_2="  XX    "
unite_5_3="  XXXXXX"
unite_5_4="      XX"
unite_5_5="  XXXXXX"
#------------------------------------------------------------------
unite_6_1="  XXXXXX"
unite_6_2="  XX    "
unite_6_3="  XXXXXX"
unite_6_4="  XX  XX"
unite_6_5="  XXXXXX"
#------------------------------------------------------------------
unite_7_1="  XXXXXX"
unite_7_2="      XX"
unite_7_3="      XX"
unite_7_4="      XX"
unite_7_5="      XX"
#------------------------------------------------------------------
unite_8_1="  XXXXXX"
unite_8_2="  XX  XX"
unite_8_3="  XXXXXX"
unite_8_4="  XX  XX"
unite_8_5="  XXXXXX"
#------------------------------------------------------------------
unite_9_1="  XXXXXX"
unite_9_2="  XX  XX"
unite_9_3="  XXXXXX"
unite_9_4="      XX"
unite_9_5="  XXXXXX"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------


# Boucle infinie
while :
do
hnow=$(date +%H)
hnow_d=$(echo ${hnow} | cut -c 1)
hnow_u=$(echo ${hnow} | cut -c 2)

mnow=$(date +%M)
mnow_d=$(echo ${mnow} | cut -c 1)
mnow_u=$(echo ${mnow} | cut -c 2)

snow=$(date +%S)
snow_d=$(echo ${snow} | cut -c 1)
snow_u=$(echo ${snow} | cut -c 2)

#------------------------------------------------------------------
# Hour : 0x-2x
if [ ${hnow_d} -eq 0 ]
then
H1_1="${unite_0_1}"
H1_2="${unite_0_2}"
H1_3="${unite_0_3}"
H1_4="${unite_0_4}"
H1_5="${unite_0_5}"
fi

if [ ${hnow_d} -eq 1 ]
then
H1_1="${unite_1_1}"
H1_2="${unite_1_2}"
H1_3="${unite_1_3}"
H1_4="${unite_1_4}"
H1_5="${unite_1_5}"
fi

if [ ${hnow_d} -eq 2 ]
then
H1_1="${unite_2_1}"
H1_2="${unite_2_2}"
H1_3="${unite_2_3}"
H1_4="${unite_2_4}"
H1_5="${unite_2_5}"
fi

#------------------------------------------------------------------
# Hour : x0-x9

if [ ${hnow_u} -eq 0 ]
then
H2_1="${unite_0_1}"
H2_2="${unite_0_2}"
H2_3="${unite_0_3}"
H2_4="${unite_0_4}"
H2_5="${unite_0_5}"
fi

if [ ${hnow_u} -eq 1 ]
then
H2_1="${unite_1_1}"
H2_2="${unite_1_2}"
H2_3="${unite_1_3}"
H2_4="${unite_1_4}"
H2_5="${unite_1_5}"
fi

if [ ${hnow_u} -eq 2 ]
then
H2_1="${unite_2_1}"
H2_2="${unite_2_2}"
H2_3="${unite_2_3}"
H2_4="${unite_2_4}"
H2_5="${unite_2_5}"
fi

if [ ${hnow_u} -eq 3 ]
then
H2_1="${unite_3_1}"
H2_2="${unite_3_2}"
H2_3="${unite_3_3}"
H2_4="${unite_3_4}"
H2_5="${unite_3_5}"
fi

if [ ${hnow_u} -eq 4 ]
then
H2_1="${unite_4_1}"
H2_2="${unite_4_2}"
H2_3="${unite_4_3}"
H2_4="${unite_4_4}"
H2_5="${unite_4_5}"
fi

if [ ${hnow_u} -eq 5 ]
then
H2_1="${unite_5_1}"
H2_2="${unite_5_2}"
H2_3="${unite_5_3}"
H2_4="${unite_5_4}"
H2_5="${unite_5_5}"
fi

if [ ${hnow_u} -eq 6 ]
then
H2_1="${unite_6_1}"
H2_2="${unite_6_2}"
H2_3="${unite_6_3}"
H2_4="${unite_6_4}"
H2_5="${unite_6_5}"
fi

if [ ${hnow_u} -eq 7 ]
then
H2_1="${unite_7_1}"
H2_2="${unite_7_2}"
H2_3="${unite_7_3}"
H2_4="${unite_7_4}"
H2_5="${unite_7_5}"
fi

if [ ${hnow_u} -eq 8 ]
then
H2_1="${unite_8_1}"
H2_2="${unite_8_2}"
H2_3="${unite_8_3}"
H2_4="${unite_8_4}"
H2_5="${unite_8_5}"
fi

if [ ${hnow_u} -eq 9 ]
then
H2_1="${unite_9_1}"
H2_2="${unite_9_2}"
H2_3="${unite_9_3}"
H2_4="${unite_9_4}"
H2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
# Minute : 0x-5x

if [ ${hnow_d} -eq 0 ]
then
M1_1="${unite_0_1}"
M1_2="${unite_0_2}"
M1_3="${unite_0_3}"
M1_4="${unite_0_4}"
M1_5="${unite_0_5}"
fi

if [ ${hnow_d} -eq 1 ]
then
M1_1="${unite_1_1}"
M1_2="${unite_1_2}"
M1_3="${unite_1_3}"
M1_4="${unite_1_4}"
M1_5="${unite_1_5}"
fi

if [ ${hnow_d} -eq 2 ]
then
M1_1="${unite_2_1}"
M1_2="${unite_2_2}"
M1_3="${unite_2_3}"
M1_4="${unite_2_4}"
M1_5="${unite_2_5}"
fi

if [ ${hnow_d} -eq 3 ]
then
M1_1="${unite_3_1}"
M1_2="${unite_3_2}"
M1_3="${unite_3_3}"
M1_4="${unite_3_4}"
M1_5="${unite_3_5}"
fi

if [ ${hnow_d} -eq 4 ]
then
M1_1="${unite_4_1}"
M1_2="${unite_4_2}"
M1_3="${unite_4_3}"
M1_4="${unite_4_4}"
M1_5="${unite_4_5}"
fi

if [ ${hnow_d} -eq 5 ]
then
M1_1="${unite_5_1}"
M1_2="${unite_5_2}"
M1_3="${unite_5_3}"
M1_4="${unite_5_4}"
M1_5="${unite_5_5}"
fi
#------------------------------------------------------------------
# Minute : x0-x9

if [ ${mnow_u} -eq 0 ]
then
M2_1="${unite_0_1}"
M2_2="${unite_0_2}"
M2_3="${unite_0_3}"
M2_4="${unite_0_4}"
M2_5="${unite_0_5}"
fi

if [ ${mnow_u} -eq 1 ]
then
M2_1="${unite_1_1}"
M2_2="${unite_1_2}"
M2_3="${unite_1_3}"
M2_4="${unite_1_4}"
M2_5="${unite_1_5}"
fi

if [ ${mnow_u} -eq 2 ]
then
M2_1="${unite_2_1}"
M2_2="${unite_2_2}"
M2_3="${unite_2_3}"
M2_4="${unite_2_4}"
M2_5="${unite_2_5}"
fi

if [ ${mnow_u} -eq 3 ]
then
M2_1="${unite_3_1}"
M2_2="${unite_3_2}"
M2_3="${unite_3_3}"
M2_4="${unite_3_4}"
M2_5="${unite_3_5}"
fi

if [ ${mnow_u} -eq 4 ]
then
M2_1="${unite_4_1}"
M2_2="${unite_4_2}"
M2_3="${unite_4_3}"
M2_4="${unite_4_4}"
M2_5="${unite_4_5}"
fi

if [ ${mnow_u} -eq 5 ]
then
M2_1="${unite_5_1}"
M2_2="${unite_5_2}"
M2_3="${unite_5_3}"
M2_4="${unite_5_4}"
M2_5="${unite_5_5}"
fi

if [ ${mnow_u} -eq 6 ]
then
M2_1="${unite_6_1}"
M2_2="${unite_6_2}"
M2_3="${unite_6_3}"
M2_4="${unite_6_4}"
M2_5="${unite_6_5}"
fi

if [ ${mnow_u} -eq 7 ]
then
M2_1="${unite_7_1}"
M2_2="${unite_7_2}"
M2_3="${unite_7_3}"
M2_4="${unite_7_4}"
M2_5="${unite_7_5}"
fi

if [ ${mnow_u} -eq 8 ]
then
M2_1="${unite_8_1}"
M2_2="${unite_8_2}"
M2_3="${unite_8_3}"
M2_4="${unite_8_4}"
M2_5="${unite_8_5}"
fi

if [ ${mnow_u} -eq 9 ]
then
M2_1="${unite_9_1}"
M2_2="${unite_9_2}"
M2_3="${unite_9_3}"
M2_4="${unite_9_4}"
M2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
# Second : 0x-5x

if [ ${snow_d} -eq 0 ]
then
S1_1="${unite_0_1}"
S1_2="${unite_0_2}"
S1_3="${unite_0_3}"
S1_4="${unite_0_4}"
S1_5="${unite_0_5}"
fi

if [ ${snow_d} -eq 1 ]
then
S1_1="${unite_1_1}"
S1_2="${unite_1_2}"
S1_3="${unite_1_3}"
S1_4="${unite_1_4}"
S1_5="${unite_1_5}"
fi

if [ ${snow_d} -eq 2 ]
then
S1_1="${unite_2_1}"
S1_2="${unite_2_2}"
S1_3="${unite_2_3}"
S1_4="${unite_2_4}"
S1_5="${unite_2_5}"
fi

if [ ${snow_d} -eq 3 ]
then
S1_1="${unite_3_1}"
S1_2="${unite_3_2}"
S1_3="${unite_3_3}"
S1_4="${unite_3_4}"
S1_5="${unite_3_5}"
fi

if [ ${snow_d} -eq 4 ]
then
S1_1="${unite_4_1}"
S1_2="${unite_4_2}"
S1_3="${unite_4_3}"
S1_4="${unite_4_4}"
S1_5="${unite_4_5}"
fi

if [ ${snow_d} -eq 5 ]
then
S1_1="${unite_5_1}"
S1_2="${unite_5_2}"
S1_3="${unite_5_3}"
S1_4="${unite_5_4}"
S1_5="${unite_5_5}"
fi

#------------------------------------------------------------------
# Second : x0-x9

if [ ${snow_u} -eq 0 ]
then
S2_1="${unite_0_1}"
S2_2="${unite_0_2}"
S2_3="${unite_0_3}"
S2_4="${unite_0_4}"
S2_5="${unite_0_5}"
fi

if [ ${snow_u} -eq 1 ]
then
S2_1="${unite_1_1}"
S2_2="${unite_1_2}"
S2_3="${unite_1_3}"
S2_4="${unite_1_4}"
S2_5="${unite_1_5}"
fi

if [ ${snow_u} -eq 2 ]
then
S2_1="${unite_2_1}"
S2_2="${unite_2_2}"
S2_3="${unite_2_3}"
S2_4="${unite_2_4}"
S2_5="${unite_2_5}"
fi

if [ ${snow_u} -eq 3 ]
then
S2_1="${unite_3_1}"
S2_2="${unite_3_2}"
S2_3="${unite_3_3}"
S2_4="${unite_3_4}"
S2_5="${unite_3_5}"
fi

if [ ${snow_u} -eq 4 ]
then
S2_1="${unite_4_1}"
S2_2="${unite_4_2}"
S2_3="${unite_4_3}"
S2_4="${unite_4_4}"
S2_5="${unite_4_5}"
fi

if [ ${snow_u} -eq 5 ]
then
S2_1="${unite_5_1}"
S2_2="${unite_5_2}"
S2_3="${unite_5_3}"
S2_4="${unite_5_4}"
S2_5="${unite_5_5}"
fi

if [ ${snow_u} -eq 6 ]
then
S2_1="${unite_6_1}"
S2_2="${unite_6_2}"
S2_3="${unite_6_3}"
S2_4="${unite_6_4}"
S2_5="${unite_6_5}"
fi

if [ ${snow_u} -eq 7 ]
then
S2_1="${unite_7_1}"
S2_2="${unite_7_2}"
S2_3="${unite_7_3}"
S2_4="${unite_7_4}"
S2_5="${unite_7_5}"
fi

if [ ${snow_u} -eq 8 ]
then
S2_1="${unite_8_1}"
S2_2="${unite_8_2}"
S2_3="${unite_8_3}"
S2_4="${unite_8_4}"
S2_5="${unite_8_5}"
fi

if [ ${snow_u} -eq 9 ]
then
S2_1="${unite_9_1}"
S2_2="${unite_9_2}"
S2_3="${unite_9_3}"
S2_4="${unite_9_4}"
S2_5="${unite_9_5}"
fi

#------------------------------------------------------------------
VAR_TIME_1="${H1_1:-0}${H2_1:-0}${separator_1}${M1_1:-0}${M2_1:-0}${separator_1}${S1_1:-0}${S2_1:-0}"
VAR_TIME_2="${H1_2:-0}${H2_2:-0}${separator_2}${M1_2:-0}${M2_2:-0}${separator_2}${S1_2:-0}${S2_2:-0}"
VAR_TIME_3="${H1_3:-0}${H2_3:-0}${separator_3}${M1_3:-0}${M2_3:-0}${separator_3}${S1_3:-0}${S2_3:-0}"
VAR_TIME_4="${H1_4:-0}${H2_4:-0}${separator_4}${M1_4:-0}${M2_4:-0}${separator_4}${S1_4:-0}${S2_4:-0}"
VAR_TIME_5="${H1_5:-0}${H2_5:-0}${separator_5}${M1_5:-0}${M2_5:-0}${separator_5}${S1_5:-0}${S2_5:-0}"
VAR_TIME="${VAR_TIME_1}\n${VAR_TIME_2}\n${VAR_TIME_3}\n${VAR_TIME_4}\n${VAR_TIME_5}"
# Affichage
clear
echo "${VAR_TIME}" | sed 's/X/█/g'
sleep 1
done

#------------------------------------------------------------------
exit 0